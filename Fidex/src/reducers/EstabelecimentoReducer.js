import {
    CHANGE_NOME_ESTABELECIMENTO,
    CHANGE_EMAIL_ESTABELECIMENTO,
    CHANGE_PHONE_ESTABELECIMENTO,
    CHANGE_WHATSAPP_ESTABELECIMENTO,
    CHANGE_CNPJ_ESTABELECIMENTO,
    CHANGE_ENDERECO_ESTABELECIMENTO,
    CHANGE_BAIRRO_ESTABELECIMENTO,
    CHANGE_CIDADE_ESTABELECIMENTO,
    CHANGE_UF_ESTABELECIMENTO,
    CHANGE_CEP_ESTABELECIMENTO,
    CHANGE_DESCRIPTION_ESTABELECIMENTO,
    CHANGE_KEYWORDS_ESTABELECIMENTO,
    CHANGE_FACEBOOK_ESTABELECIMENTO,
    CHANGE_INSTAGRAM_ESTABELECIMENTO,
    CHANGE_TWITTER_ESTABELECIMENTO,
    CHANGE_CATEGORIA_ESTABELECIMENTO,
    CHANGE_AVATAR_ESTABELECIMENTO,
    SAVE_ESTABELECIMENTOS,
    CHANGE_ID_ESTABELECIMENTO,
    CHANGE_TYPE_OF_SEARCH,
    CHANGE_STATUS_SEARCH_PLACES,
    CHANGE_SEARCH_ESTABELECIMENTO,
    CHANGE_FOLLOW_ESTABELECIMENTO,
    CHANGE_PROMOCOES_ESTABELECIMENTO,
    CHANGE_PROMOCOES_RESGATADAS,
    CHANGE_PONTOS_USUARIO,
    CHANGE_LATITUDE_ESTABELECIMENTO,
    CHANGE_LONGITUDE_ESTABELECIMENTO,
    CHANGE_REGION_ESTABELECIMENTO,
    CHANGE_REGION_ESTABELECIMENTO2,
    RESET_REGION_ESTABELECIMENTO,
    CHANGE_CREDITAR_PONTOS_CPF,
    CHANGE_CREDITAR_PONTOS_VALOR,
    CHANGE_RESGATAR_PROMOCAO_CPF,
    CHANGE_RESGATAR_PROMOCAO_VOUCHER,
    CHANGE_RESGATAR_PROMOCAO_ERROR,
    CHANGE_CLIENTES,
    CHANGE_PONTOS_CREDITADOS,
    CHANGE_RESGATE_PROMOCOES,
    CHANGE_MOVIMENTACOES,
    CHANGE_CLIENTE_ATUAL
} from '../actions/types';

const INITIAL_STATE = {
    // informacoes básicas de um estabelecimento
    name: '',
    email: '',
    phone: '',
    whatsapp: '',
    cnpj: '',
    description: '',
    category: 'Categoria',
    avatar: '',
    keywords: '',
    facebook: '',
    instagram: '',
    twitter: '',
    latitude: null,
    longitude: null,

    // campos de endereco - adicionados posteriormente
    endereco: '',
    bairro: '',
    cidade: '',
    uf: 'UF',
    cep: '',

    defaultRegion: {
        latitude: -12.809036,
        longitude: -50.618271,
        latitudeDelta: 35,
        longitudeDelta: 35,
    },
    id: 0,
    segue: 'nao',
    promocoes: null,
    pontosUsuario: 0,
    promocoesResgatadas: 0,
    estabelecimentos: null,

    statusBuscaPorLocais: 'normal',
    tipoBuscaPorLocais: 'Todos os lugares',
    tiposBuscaPorLocais: [
        {
            nome: 'Todos os lugares'
        },
        {
            nome: 'Lugares próximos'
        }
    ],
    todosEstabelecimentos: [],
    pesquisaEstabelecimento: '',
    creditarPontosCpf: '',
    creditarPontosValor: '',
    resgatarPromocaoCpf: '',
    resgatarPromocaoCodigoVoucher: '',
    resgatarPromocaoError: null,
    clientes: null,
    clienteAtual: null,
    pontosCreditados: null,
    resgatePromocoes: null,
    listaMovimentacoes: null,
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case CHANGE_MOVIMENTACOES:
            return { ...state, listaMovimentacoes: action.payload }
        case CHANGE_RESGATAR_PROMOCAO_CPF:
            return { ...state, resgatarPromocaoCpf: action.payload }
        case CHANGE_RESGATAR_PROMOCAO_VOUCHER:
            return { ...state, resgatarPromocaoCodigoVoucher: action.payload }
        case CHANGE_RESGATAR_PROMOCAO_ERROR:
            return { ...state, resgatarPromocaoError: action.payload }
        case CHANGE_CREDITAR_PONTOS_CPF:
            return { ...state, creditarPontosCpf: action.payload }
        case CHANGE_CREDITAR_PONTOS_VALOR:
            return { ...state, creditarPontosValor: action.payload }
        case CHANGE_NOME_ESTABELECIMENTO:
            return { ...state, name: action.payload }
        case CHANGE_EMAIL_ESTABELECIMENTO:
            return { ...state, email: action.payload }
        case CHANGE_PHONE_ESTABELECIMENTO:
            return { ...state, phone: action.payload }
        case CHANGE_WHATSAPP_ESTABELECIMENTO:
            return { ...state, whatsapp: action.payload }
        case CHANGE_CNPJ_ESTABELECIMENTO:
            return { ...state, cnpj: action.payload }
        case CHANGE_ENDERECO_ESTABELECIMENTO:
            return { ...state, endereco: action.payload }
        case CHANGE_BAIRRO_ESTABELECIMENTO:
            return { ...state, bairro: action.payload }
        case CHANGE_CIDADE_ESTABELECIMENTO:
            return { ...state, cidade: action.payload }
        case CHANGE_UF_ESTABELECIMENTO:
            return { ...state, uf: action.payload }
        case CHANGE_CEP_ESTABELECIMENTO:
            return { ...state, cep: action.payload }
        case CHANGE_DESCRIPTION_ESTABELECIMENTO:
            return { ...state, description: action.payload }
        case CHANGE_KEYWORDS_ESTABELECIMENTO:
            return { ...state, keywords: action.payload }
        case CHANGE_FACEBOOK_ESTABELECIMENTO:
            return { ...state, facebook: action.payload }
        case CHANGE_INSTAGRAM_ESTABELECIMENTO:
            return { ...state, instagram: action.payload }
        case CHANGE_TWITTER_ESTABELECIMENTO:
            return { ...state, twitter: action.payload }
        case CHANGE_CATEGORIA_ESTABELECIMENTO:
            return { ...state, category: action.payload }
        case CHANGE_AVATAR_ESTABELECIMENTO:
            return { ...state, avatar: action.payload }
        case CHANGE_FOLLOW_ESTABELECIMENTO:
            return { ...state, segue: action.payload }
        case SAVE_ESTABELECIMENTOS:
            return { ...state, estabelecimentos: action.payload }
        case CHANGE_REGION_ESTABELECIMENTO2:
            return { ...state, defaultRegion: action.payload }
        case CHANGE_REGION_ESTABELECIMENTO:
            let regionMarkerAux = {
                latitude: action.payload.latitude,
                longitude: action.payload.longitude,
                latitudeDelta: 0.015,
                longitudeDelta: 0.015,
            }
            return { ...state, defaultRegion: regionMarkerAux }
        case RESET_REGION_ESTABELECIMENTO:
            let regionMarkerAux2 = {
                latitude: -12.809036,
                longitude: -50.618271,
                latitudeDelta: 35,
                longitudeDelta: 35,
            }
            return { ...state, defaultRegion: regionMarkerAux2 }
        case CHANGE_ID_ESTABELECIMENTO:
            return { ...state, id: action.payload }
        case CHANGE_TYPE_OF_SEARCH:
            return { ...state, tipoBuscaPorLocais: action.payload, statusBuscaPorLocais: 'normal' }
        case CHANGE_STATUS_SEARCH_PLACES:
            return { ...state, statusBuscaPorLocais: action.payload }
        case CHANGE_SEARCH_ESTABELECIMENTO:
            return { ...state, pesquisaEstabelecimento: action.payload }
        case CHANGE_PROMOCOES_ESTABELECIMENTO:
            return { ...state, promocoes: action.payload }
        case CHANGE_PROMOCOES_RESGATADAS:
            return { ...state, promocoesResgatadas: action.payload }
        case CHANGE_PONTOS_USUARIO:
            return { ...state, pontosUsuario: action.payload }
        case CHANGE_LATITUDE_ESTABELECIMENTO:
            return { ...state, latitude: action.payload }
        case CHANGE_LONGITUDE_ESTABELECIMENTO:
            return { ...state, longitude: action.payload }
        case CHANGE_CLIENTES:
            return { ...state, clientes: action.payload }
        case CHANGE_PONTOS_CREDITADOS:
            return { ...state, pontosCreditados: action.payload }
        case CHANGE_RESGATE_PROMOCOES:
            return { ...state, resgatePromocoes: action.payload }
        case CHANGE_CLIENTE_ATUAL:
            return { ...state, clienteAtual: action.payload }
        default:
            return state;
    }
}