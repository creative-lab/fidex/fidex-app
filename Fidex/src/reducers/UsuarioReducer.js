import {
    CHANGE_DASHBOARD,
    CHANGE_MY_PLACES,
    CHANGE_LIST_VOUCHERS,
    CHANGE_DETAIL_VOUCHER,
    CHANGE_USUARIO,
    CHANGE_NOME_USUARIO,
    CHANGE_CPF_USUARIO,
    CHANGE_EMAIL_USUARIO,
    CHANGE_TELEFONE_USUARIO,
    CHANGE_AVATAR,
    CHANGE_UF,
    CHANGE_CIDADE
} from '../actions/types';

const INITIAL_STATE = {
    dashboard: null,
    meusLugares: null,
    listaVouchers: null,
    detalheVoucher: null,
    usuario: null,
    nome: null,
    cpf: null,
    email: null,
    telefone: null,
    avatar: '',
    uf: 'UF',
    cidade: null
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case CHANGE_DASHBOARD:
            return { ...state, dashboard: action.payload }
        case CHANGE_MY_PLACES:
            return { ...state, meusLugares: action.payload }
        case CHANGE_LIST_VOUCHERS:
            return { ...state, listaVouchers: action.payload }
        case CHANGE_DETAIL_VOUCHER:
            return { ...state, detalheVoucher: action.payload }
        case CHANGE_USUARIO:
            return { ...state, usuario: action.payload }
        case CHANGE_NOME_USUARIO:
            return { ...state, nome: action.payload }
        case CHANGE_CPF_USUARIO:
            return { ...state, cpf: action.payload }
        case CHANGE_EMAIL_USUARIO:
            return { ...state, email: action.payload }
        case CHANGE_TELEFONE_USUARIO:
            return { ...state, telefone: action.payload }
        case CHANGE_AVATAR:
            return { ...state, avatar: action.payload }
        case CHANGE_UF:
            return { ...state, uf: action.payload }
        case CHANGE_CIDADE:
            return { ...state, cidade: action.payload }
        default:
            return state;
    }
}