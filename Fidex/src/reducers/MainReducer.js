import { Actions } from 'react-native-router-flux';

import {
    CHANGE_TAB_SELECTED,
} from '../actions/types';

const INITIAL_STATE = {
    selectedTab: 'home',
    titleSelectedTab: 'Meu Fidex'
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case CHANGE_TAB_SELECTED:
            let titleTab;
            switch (action.payload) {
                case 'browser':
                    titleTab = "Explorar";
                    break;
                case 'myplaces':
                    titleTab = "Meus lugares";
                    break;
                case 'mywallet':
                    titleTab = "Minha carteira";
                    break;
                case 'promotion':
                    titleTab = "Promoções";
                    break;
                case 'transation':
                    titleTab = "Movimentação";
                    break;
                case 'chart':
                    titleTab = "Informações do estabelecimento";
                    break;
                default:
                    titleTab = "Meu Fidex";
            }
            Actions.refresh({ key: 'main', title: titleTab });
            return { ...state, selectedTab: action.payload, titleSelectedTab: titleTab }
        default:
            return state;
    }
}