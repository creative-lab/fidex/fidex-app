import {
    CHANGE_CPF,
    CHANGE_PASS,
    CHANGE_NAME,
    CHANGE_CONFIRM_PASS,
    CHANGE_EMAIL,
    CHANGE_PHONE,
    ACTIVITY_LOADER,
    SAVE_USER_LOGGED,
    CHANGE_LATITUDE_USUARIO,
    CHANGE_LONGITUDE_USUARIO,
    CHANGE_ERROR_GEOPOSITION,
    CHANGE_NEW_PASS,
    CHANGE_CONFIRM_NEW_PASS
} from '../actions/types';

const INITIAL_STATE = {
    cpf: '',
    pass: '',
    confirmPass: '',
    name: '',
    email: '',
    phone: '',
    showLoader: false,
    id: '',
    newPass: '',
    confirmeNewPass: ''
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case CHANGE_CPF:
            return { ...state, cpf: action.payload }
        case CHANGE_PASS:
            return { ...state, pass: action.payload }
        case CHANGE_NAME:
            return { ...state, name: action.payload }
        case CHANGE_CONFIRM_PASS:
            return { ...state, confirmPass: action.payload }
        case CHANGE_EMAIL:
            return { ...state, email: action.payload }
        case CHANGE_PHONE:
            return { ...state, phone: action.payload }
        case SAVE_USER_LOGGED:
            return { ...state, id: action.payload }
        case ACTIVITY_LOADER:
            return { ...state, showLoader: action.payload }
        case CHANGE_LATITUDE_USUARIO:
            return { ...state, latitudeUsuario: action.payload }
        case CHANGE_LONGITUDE_USUARIO:
            return { ...state, longitudeUsuario: action.payload }
        case CHANGE_ERROR_GEOPOSITION:
            return { ...state, errorGeoposition: action.payload }
        case CHANGE_NEW_PASS:
            return { ...state, newPass: action.payload }
        case CHANGE_CONFIRM_NEW_PASS:
            return { ...state, confirmeNewPass: action.payload }
        default:
            return state;
    }
}