import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import MainReducer from './MainReducer';
import EstabelecimentoReducer from './EstabelecimentoReducer';
import PromocaoReducer from './PromocaoReducer';
import UsuarioReducer from './UsuarioReducer';

export default combineReducers({
    AuthReducer, 
    MainReducer,
    EstabelecimentoReducer,
    PromocaoReducer,
    UsuarioReducer
});