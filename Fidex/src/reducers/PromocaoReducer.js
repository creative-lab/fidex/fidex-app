import {
    CHANGE_NOME_PROMOCAO,
    CHANGE_PONTOS_PROMOCAO,
    CHANGE_DESCRICAO_PROMOCAO,
    CHANGE_ID_PROMOCAO,
    SAVE_PROMOCOES

} from '../actions/types';

const INITIAL_STATE = {
    nome: '',
    pontosRestate: '',
    descricao: '',
    id: 1,
    titleEdicao: "Criar Promoção",
    promocoes: []
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case CHANGE_NOME_PROMOCAO:
            return { ...state, nome: action.payload }
        case CHANGE_PONTOS_PROMOCAO:
            return { ...state, pontosRestate: action.payload }
        case CHANGE_DESCRICAO_PROMOCAO:
            return { ...state, descricao: action.payload }
        case CHANGE_ID_PROMOCAO:
            idAux = action.payload
            if (idAux === 0) {
                return { ...state, id: action.payload, titleEdicao: "Criar Promoção" }
            }
            return { ...state, id: action.payload, titleEdicao: "Editar Promoção" }

        case SAVE_PROMOCOES:
            return { ...state, promocoes: action.payload }
        default:
            return state;
    }
}