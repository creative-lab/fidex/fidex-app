const URL_BASE =
    "http://fidexapp.com.br/api/"; // homologacao

export const endpoints = {
    CADASTRO_CONTA: `${URL_BASE}usuario/create`,
    LOGIN_USUARIO: `${URL_BASE}auth/usuario`,
    LOGIN_ESTABELECIMENTO: `${URL_BASE}auth/estabelecimento`,
    CADASTRO_ESTABELECIMENTO: `${URL_BASE}estabelecimento/create`,
    LISTA_ESTABELECIMENTO: `${URL_BASE}estabelecimento/lista-estabelecimento`,
    EDITAR_ESTABELECIMENTO: `${URL_BASE}estabelecimento/editar`,
    LISTAR_PROMOCOES: `${URL_BASE}estabelecimento/listar-promocoes`,
    EDITAR_PROMOCAO: `${URL_BASE}estabelecimento/editar-promocao`,
    SALVAR_PROMOCAO: `${URL_BASE}estabelecimento/salvar-promocao`,
    EXCLUIR_PROMOCAO: `${URL_BASE}estabelecimento/excluir-promocao`,
    BUSCAR_ESTABELECIMENTOS: `${URL_BASE}estabelecimento/lugares-proximos`,
    DETALHES_ESTABELECIMENTO: `${URL_BASE}estabelecimento/informacoes`,
    RECUPERAR_DASHBOARD: `${URL_BASE}usuario/dashboard`,
    RECUPERAR_LOCAIS_USUARIO: `${URL_BASE}usuario/meus-lugares`,
    SEGUIR_ESTABELECIMENTO: `${URL_BASE}estabelecimento/seguir`,
    CREDITAR_PONTOS: `${URL_BASE}usuario/creditar-pontos`,
    RESGATAR_PROMOCAO: `${URL_BASE}voucher/gerar-voucher`,
    LISTA_VOUCHERS: `${URL_BASE}voucher/lista-voucher`,
    DETALHES_VOUCHER: `${URL_BASE}voucher/visualizar`,
    ESTABELECIMENTO_RESGATAR_PROMOCAO: `${URL_BASE}voucher/resgatar-promocao`,
    RECUPERA_CLIENTES: `${URL_BASE}estabelecimento/clientes`,
    RECUPERA_PONTOS_CREDITADOS: `${URL_BASE}estabelecimento/pontos-creditados`,
    RECUPERAR_RESGATE_PROMOCOES: `${URL_BASE}estabelecimento/resgates-promocoes`,
    RECUPERAR_LISTA_MOVIMENTACOES: `${URL_BASE}estabelecimento/lista-movimentacao`,
    RECUPERAR_DADOS_USUARIO: `${URL_BASE}usuario/buscar`,
    EDITAR_PERFIL: `${URL_BASE}usuario/editar`,
    EDITAR_SENHA: `${URL_BASE}usuario/alterar-senha`,
    RECUPERAR_DADOS_CLIENTE: `${URL_BASE}estabelecimento/cliente-info`
}
