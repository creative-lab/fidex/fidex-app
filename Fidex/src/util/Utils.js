export const formatUrl = (unFormatedUrl) => {
    if (!unFormatedUrl) {
        return unFormatedUrl
    }
    let formatedUrl = unFormatedUrl.toLowerCase();
    if (!(formatedUrl.startsWith('http://') || formatedUrl.startsWith('https://'))) {
        formatedUrl = `https://${formatedUrl}`
    }
    return formatedUrl
}

export const addProviderInUrl = (provider, action, name) =>{
    name = name.toLowerCase()
    if(name){
        if(!(name.startsWith(`http://www.${provider}/`)
                || name.startsWith(`http://${provider}/`)
                || name.startsWith(`http://${provider}`)
                || name.startsWith(`https://www.${provider}/`)
                || name.startsWith(`https://${provider}/`)
                || name.startsWith(`https://${provider}`)
                || name.startsWith(`www.${provider}`)
                || name.startsWith(`www.${provider}/`)
                || name.startsWith(`${provider}`)
                || name.startsWith(`${provider}/`))){
                    action(`https://${provider}/${name}`)
                    // console.log(`https://${provider}/${name}`)
                }    

    }

}
