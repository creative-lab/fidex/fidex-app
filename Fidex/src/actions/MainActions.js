import {
    CHANGE_TAB_SELECTED,
    CHANGE_TYPE_OF_SEARCH,
    CHANGE_STATUS_SEARCH_PLACES
} from './types';

export const changeTab = (text) => {
    return {
        type: CHANGE_TAB_SELECTED,
        payload: text
    }
}

export const changeTypeOfSearchPlaces = (text) => {
    return {
        type: CHANGE_TYPE_OF_SEARCH,
        payload: text
    }
}

export const changeStatusSearchPlaces = (text) => {
    return {
        type: CHANGE_STATUS_SEARCH_PLACES,
        payload: text
    }
}

