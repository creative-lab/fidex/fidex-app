import axios from 'axios';
import { Alert, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';

import {
    CHANGE_CPF,
    CHANGE_PASS,
    CHANGE_NAME,
    CHANGE_EMAIL,
    CHANGE_CONFIRM_PASS,
    CHANGE_PHONE,
    ACTIVITY_LOADER,
    SAVE_USER_LOGGED,
    CHANGE_LATITUDE_USUARIO,
    CHANGE_LONGITUDE_USUARIO,
    CHANGE_ERROR_GEOPOSITION,
    CHANGE_NEW_PASS,
    CHANGE_CONFIRM_NEW_PASS
} from './types';

import { strings } from '../../res/strings';
import { endpoints } from '../util/endpoints';

export const alterarSenha = (senha, confirmacao, user_id) => {

    let id = parseInt(user_id.split(':')[1])

    return dispatch => {
        dispatch(showLoader(true));
        axios.post(endpoints.EDITAR_SENHA,
            {
                senha,
                confirmar_senha: confirmacao,
                id,
            })
            .then(response => alterarSenhaSucess(response, dispatch))
            .catch(error => alterarSenhaFail(error, dispatch));
    }
}

const alterarSenhaSucess = (response, dispatch) => {
    dispatch(showLoader(false));
    Alert.alert(strings.default_title_alert, "Senha alterada com sucesso");
    dispatch(Actions.pop())
}

const alterarSenhaFail = (error, dispatch) => {
    dispatch(showLoader(false));
    console.log(error)
    if (error.response) {
        switch (error.response.status) {
            case 409:
                Alert.alert(strings.default_title_alert, error.response.data.error);
                break;
            default:
                Alert.alert(strings.default_title_alert, `${strings.error_conection_default} Código ${error.response.status}`);
        }
    }
}

export const loginUsuario = (cpf, senha) => {
    return dispatch => {
        dispatch(showLoader(true));
        axios.post(endpoints.LOGIN_USUARIO,
            {
                cpf,
                senha
            })
            .then(response => loginUsuarioSucess(response, dispatch))
            .catch(error => loginUsuarioFail(error, dispatch));
    }
}

const loginUsuarioSucess = (response, dispatch) => {
    dispatch(showLoader(false));
    AsyncStorage.setItem('id', `usuario:${response.data.id}`)
    dispatch(toogleUserLogged(`usuario:${response.data.id}`))
    Actions.main();
}

const loginUsuarioFail = (error, dispatch) => {
    dispatch(showLoader(false));
    if (error.response) {
        console.log(error.response.status)
        switch (error.response.status) {
            case 409:
                Alert.alert(strings.default_title_alert, strings.error_login_pass);
                break;
            default:
                Alert.alert(strings.default_title_alert, strings.error_conection_default);
        }
    }
}

export const loginEstabelecimento = (email, senha) => {
    return dispatch => {
        dispatch(showLoader(true));
        axios.post(endpoints.LOGIN_ESTABELECIMENTO,
            {
                email,
                senha
            })
            .then(response => loginEstabelecimentoSucess(response, dispatch))
            .catch(error => loginEstabelecimentoFail(error, dispatch));
    }
}

const loginEstabelecimentoSucess = (response, dispatch) => {
    dispatch(showLoader(false));
    AsyncStorage.setItem('id', `estabelecimento:${response.data.id}`)
    dispatch(toogleUserLogged(`estabelecimento:${response.data.id}`))
    Actions.startEmpresas();
}

const loginEstabelecimentoFail = (error, dispatch) => {
    dispatch(showLoader(false));
    if (error.response) {
        switch (error.response.status) {
            case 409:
                Alert.alert(strings.default_title_alert, strings.error_login_pass);
                break;
            default:
                Alert.alert(strings.default_title_alert, strings.error_conection_default);
        }
    }
}

export const logout = () => {

    console.log('teste')

    // resetToken()

    // return dispatch => {
    // dispatch(Actions.start());
    // dispatch(toogleUserLogged(''));
    // }
}

const resetToken = () => {
    try {
        AsyncStorage.setItem('id', '')
        // toogleUserLogged('')
    } catch (error) {
        console.log(`Erro ao resetar o token localmente ${error}`)
    }
}

export const criarConta = (nome, email, cpf, telefone, senha, tipo) => {
    return dispatch => {
        dispatch(showLoader(true));
        axios.post(endpoints.CADASTRO_CONTA,
            {
                nome,
                email,
                cpf,
                telefone,
                senha,
                tipo
            })
            .then(response => criarContaSucess(response, dispatch))
            .catch(error => criarContaFail(error, dispatch));
    }
}

export const toogleUserLogged = (id) => {
    return {
        type: SAVE_USER_LOGGED,
        payload: id
    }
}

const criarContaSucess = (response, dispatch) => {
    dispatch(showLoader(false));
    Alert.alert(strings.default_title_alert, "Conta criada com sucesso");
    console.log(response.data)
    if (response.data.tipo === 1) {
        //     // usuario
        AsyncStorage.setItem('id', `usuario:${response.data.id}`)
        dispatch(toogleUserLogged(`usuario:${response.data.id}`))
        console.log(`usuario:${response.data.id}`)
        Actions.main();
    } else {
        AsyncStorage.setItem('id', `estabelecimento:${response.data.id}`)
        console.log(`estabelecimento:${response.data.id}`)
        dispatch(toogleUserLogged(`estabelecimento:${response.data.id}`))
        Actions.startEmpresas();
    }
}

const criarContaFail = (error, dispatch) => {
    dispatch(showLoader(false));

    if (error.response) {
        switch (error.response.status) {
            case 409:
                Alert.alert(strings.default_title_alert, strings.error_user_early_singin);
                break;
            default:
                Alert.alert(strings.default_title_alert, strings.error_conection_default);
        }
    }
}

export const changeErrorGeoposition = (error) => {
    return {
        type: CHANGE_ERROR_GEOPOSITION,
        payload: error
    }
}

export const changeLatitude = (latitude) => {
    return {
        type: CHANGE_LATITUDE_USUARIO,
        payload: latitude
    }
}

export const changeNewPass = (newPass) => {
    return {
        type: CHANGE_NEW_PASS,
        payload: newPass
    }
}

export const changeConfirmNewPass = (newPass) => {
    return {
        type: CHANGE_CONFIRM_NEW_PASS,
        payload: newPass
    }
}

export const changeLongitude = (longitude) => {
    return {
        type: CHANGE_LONGITUDE_USUARIO,
        payload: longitude
    }
}

export const changeCPF = (text) => {
    return {
        type: CHANGE_CPF,
        payload: text
    }
}

export const changeEmail = (text) => {
    return {
        type: CHANGE_EMAIL,
        payload: text
    }
}

export const changePhone = (text) => {
    return {
        type: CHANGE_PHONE,
        payload: text
    }
}

export const changePass = (text) => {
    return {
        type: CHANGE_PASS,
        payload: text
    }
}

export const changeName = (text) => {
    return {
        type: CHANGE_NAME,
        payload: text
    }
}

export const changeConfirmPass = (text) => {
    return {
        type: CHANGE_CONFIRM_PASS,
        payload: text
    }
}

export const showLoader = (show) => {
    return {
        type: ACTIVITY_LOADER,
        payload: show
    }
}