import axios from 'axios';
import { Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';

import {
    CHANGE_DASHBOARD,
    CHANGE_MY_PLACES,
    CHANGE_LIST_VOUCHERS,
    CHANGE_DETAIL_VOUCHER,
    CHANGE_USUARIO,
    CHANGE_NOME_USUARIO,
    CHANGE_CPF_USUARIO,
    CHANGE_EMAIL_USUARIO,
    CHANGE_TELEFONE_USUARIO,
    CHANGE_AVATAR,
    CHANGE_UF,
    CHANGE_CIDADE,
} from './types';

import { endpoints } from '../util/endpoints';
import { strings } from '../../res/strings';

import {
    showLoader
} from './AuthActions';

import {
    recuperarPaginaEstabelecimentos
} from './EstabelecimentoActions'

export const editarPerfil = (nome, email, cpf, telefone, cidade, uf, avatar, user_id) => {
    let id = parseInt(user_id.split(':')[1])

    if (avatar != null && !avatar.includes("data:image/jpeg;base64,")) {
        avatar = `data:image/jpeg;base64,${avatar}`
    }

    return dispatch => {

        dispatch(showLoader(true));
        axios.post(endpoints.EDITAR_PERFIL,
            {
                nome,
                email,
                cpf,
                telefone,
                cidade,
                uf,
                avatar: avatar, // passar o avatar como base64
                id,
            })
            .then(response => editarPerfilSucess(response, dispatch, user_id))
            .catch(error => editarPerfilFail(error, dispatch));
    }
}

const editarPerfilSucess = (response, dispatch, user_id) => {
    dispatch(showLoader(false));
    Alert.alert(strings.default_title_alert, "Perfil editado com sucesso");
    if(user_id.includes('usuario')){
        dispatch(recuperarDashboard(user_id))
    }
    dispatch(Actions.pop())
}

const editarPerfilFail = (error, dispatch) => {
    dispatch(showLoader(false));

    if (error.response) {
        switch (error.response.status) {
            case 409:
                Alert.alert(strings.default_title_alert, error.response.data.error);
                break;
            default:
                Alert.alert(strings.default_title_alert, `${strings.error_conection_default} Código ${error.response.status}`);
        }
    }
}

export const recuperarDadosUsuario = (user_id) => {

    let id = parseInt(user_id.split(':')[1])

    return dispatch => {

        dispatch(showLoader(true));
        axios.get(endpoints.RECUPERAR_DADOS_USUARIO,
            {
                params: {
                    id,
                }
            })
            .then(response => recuperarDadosUsuarioSucess(response, dispatch))
            .catch(error => recuperarDadosUsuarioFail(error, dispatch));
    }
}

const recuperarDadosUsuarioSucess = (response, dispatch) => {
    dispatch(showLoader(false));
    dispatch(changeUsuario(response.data))
    dispatch(changeNome(response.data.nome))
    dispatch(changeCPF(response.data.cpf))
    dispatch(changeEmail(response.data.email))
    dispatch(changeTelefone(response.data.telefone))
    dispatch(changeCidade(response.data.cidade))
    if(response.data.uf === null){
        dispatch(changeUf('UF'))
    } else {
        dispatch(changeUf(response.data.uf))
    }
    if(!response.data.avatar.includes('http://fidexapp.com.br/')){
        dispatch(changeAvatar(`http://fidexapp.com.br/img/avatar/${response.data.avatar}`))
    } else {
        dispatch(changeAvatar(response.data.avatar))
    }
}

const recuperarDadosUsuarioFail = (error, dispatch) => {
    dispatch(showLoader(false));

    if (error.response) {
        switch (error.response.status) {
            case 409:
                Alert.alert(strings.default_title_alert, error.response.data.error);
                break;
            default:
                Alert.alert(strings.default_title_alert, `${strings.error_conection_default} Código ${error.response.status}`);
        }
    }
}


export const recuperarDetalhesVoucher = (user_id, idVoucher) => {

    let usuarioId = parseInt(user_id.split(':')[1])

    return dispatch => {

        dispatch(showLoader(true));
        axios.get(endpoints.DETALHES_VOUCHER,
            {
                params: {
                    usuarioId,
                    id: idVoucher
                }
            })
            .then(response => recuperarDetalhesVoucherSucess(response, dispatch))
            .catch(error => recuperarDetalhesVoucherFail(error, dispatch));
    }
}

const recuperarDetalhesVoucherSucess = (response, dispatch) => {
    dispatch(showLoader(false));
    dispatch(changeDetalheVoucher(response.data))
}

const recuperarDetalhesVoucherFail = (error, dispatch) => {
    dispatch(showLoader(false));

    if (error.response) {
        switch (error.response.status) {
            case 409:
                Alert.alert(strings.default_title_alert, error.response.data.error);
                break;
            default:
                Alert.alert(strings.default_title_alert, `${strings.error_conection_default} Código ${error.response.status}`);
        }
    }
}

export const recuperarListaVouchers = (user_id) => {

    let usuarioId = parseInt(user_id.split(':')[1])
    return dispatch => {

        dispatch(showLoader(true));
        axios.get(endpoints.LISTA_VOUCHERS,
            {
                params: {
                    usuarioId
                }
            })
            .then(response => recuperarListaVouchersSucess(response, dispatch))
            .catch(error => recuperarListaVouchersFail(error, dispatch));
    }
}

const recuperarListaVouchersSucess = (response, dispatch) => {
    dispatch(showLoader(false));
    dispatch(changeListaVouchers(response.data.wallets))
}

const recuperarListaVouchersFail = (error, dispatch) => {
    dispatch(showLoader(false));

    if (error.response) {
        switch (error.response.status) {
            case 409:
                Alert.alert(strings.default_title_alert, error.response.data.error);
                break;
            default:
                Alert.alert(strings.default_title_alert, `${strings.error_conection_default} Código ${error.response.status}`);
        }
    }
}


export const resgatarPromocao = (user_id, promocaoId, estabelecimentoId) => {

    let usuarioId = parseInt(user_id.split(':')[1])

    return dispatch => {

        dispatch(showLoader(true));
        axios.get(endpoints.RESGATAR_PROMOCAO,
            {
                params: {
                    usuarioId,
                    promocaoId
                }
            })
            .then(response => resgatarPromocaoSucess(response, dispatch, user_id, estabelecimentoId))
            .catch(error => resgatarPromocaoFail(error, dispatch));
    }
}

const resgatarPromocaoSucess = (response, dispatch, user_id, estabelecimentoId) => {
    dispatch(showLoader(false));
    Alert.alert(strings.default_title_alert, "Promoção resgatada")
    dispatch(recuperarPaginaEstabelecimentos(user_id, estabelecimentoId))
}

const resgatarPromocaoFail = (error, dispatch) => {
    dispatch(showLoader(false));

    if (error.response) {
        switch (error.response.status) {
            case 409:
                Alert.alert(strings.default_title_alert, error.response.data.error);
                break;
            default:
                Alert.alert(strings.default_title_alert, `${strings.error_conection_default} Código ${error.response.status}`);
        }
    }
}

export const recuperarLocaisDoUsuario = (user_id) => {

    let usuarioId = parseInt(user_id.split(':')[1])

    return dispatch => {

        dispatch(showLoader(true));
        axios.get(endpoints.RECUPERAR_LOCAIS_USUARIO,
            {
                params: {
                    id: usuarioId
                }
            })
            .then(response => recuperarLocaisDoUsuarioSucess(response, dispatch))
            .catch(error => recuperarLocaisDoUsuarioFail(error, dispatch));
    }
}

const recuperarLocaisDoUsuarioSucess = (response, dispatch) => {
    dispatch(showLoader(false));
    dispatch(changeMeusLugares(response.data.meusLugares))
}

const recuperarLocaisDoUsuarioFail = (error, dispatch) => {
    dispatch(showLoader(false));
    if (error.response) {
        switch (error.response.status) {
            case 409:
                Alert.alert(strings.default_title_alert, error.response.data.error);
                break;
            default:
                Alert.alert(strings.default_title_alert, `${strings.error_conection_default} Código ${error.response.status}`);
        }
    }
}

export const recuperarDashboard = (user_id) => {

    let usuarioId = parseInt(user_id.split(':')[1])

    return dispatch => {

        dispatch(showLoader(true));
        axios.get(endpoints.RECUPERAR_DASHBOARD,
            {
                params: {
                    id: usuarioId
                }
            })
            .then(response => recuperarDashboardSucess(response, dispatch))
            .catch(error => recuperarDashboardFail(error, dispatch));
    }
}

const recuperarDashboardSucess = (response, dispatch) => {
    dispatch(showLoader(false));
    dispatch(changeDashboard(response.data))
}

const recuperarDashboardFail = (error, dispatch) => {
    dispatch(showLoader(false));
    if (error.response) {
        switch (error.response.status) {
            case 409:
                Alert.alert(strings.default_title_alert, error.response.data.error);
                break;
            default:
                Alert.alert(strings.default_title_alert, `${strings.error_conection_default} Código ${error.response.status}`);
        }
    }
}

export const changeDashboard = (dashboard) => {
    return {
        type: CHANGE_DASHBOARD,
        payload: dashboard
    }
}

export const changeUsuario = (usuario) => {
    return {
        type: CHANGE_USUARIO,
        payload: usuario
    }
}


export const changeMeusLugares = (meusLugares) => {
    return {
        type: CHANGE_MY_PLACES,
        payload: meusLugares
    }
}

export const changeListaVouchers = (listaVouchers) => {
    return {
        type: CHANGE_LIST_VOUCHERS,
        payload: listaVouchers
    }
}

export const changeDetalheVoucher = (detalhes) => {
    return {
        type: CHANGE_DETAIL_VOUCHER,
        payload: detalhes
    }
}

export const changeNome = (nome) => {
    return {
        type: CHANGE_NOME_USUARIO,
        payload: nome
    }
}

export const changeCPF = (cpf) => {
    return {
        type: CHANGE_CPF_USUARIO,
        payload: cpf
    }
}

export const changeEmail = (email) => {
    return {
        type: CHANGE_EMAIL_USUARIO,
        payload: email
    }
}

export const changeTelefone = (telefone) => {
    return {
        type: CHANGE_TELEFONE_USUARIO,
        payload: telefone
    }
}

export const changeAvatar = (avatar) => {
    return {
        type: CHANGE_AVATAR,
        payload: avatar
    }
}

export const changeUf = (uf) => {
    return {
        type: CHANGE_UF,
        payload: uf
    }
}

export const changeCidade = (cidade) => {
    return {
        type: CHANGE_CIDADE,
        payload: cidade
    }
}

    