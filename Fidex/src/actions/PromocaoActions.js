import axios from 'axios';
import { Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';

import {
    CHANGE_NOME_PROMOCAO,
    CHANGE_PONTOS_PROMOCAO,
    CHANGE_DESCRICAO_PROMOCAO,
    CHANGE_ID_PROMOCAO,
    SAVE_PROMOCOES

} from '../actions/types';

import {
    showLoader
} from './AuthActions';

import { endpoints } from '../util/endpoints';
import { strings } from '../../res/strings';

export const excluirPromocoes = (id, estabelecimentoId) => {

    return dispatch => {

        dispatch(showLoader(true));
        axios.get(endpoints.EXCLUIR_PROMOCAO,
            {
                params: {
                    id
                }
            })
            .then(response => excluirPromocoesSucess(response, dispatch, estabelecimentoId))
            .catch(error => excluirPromocoesFail(error, dispatch));
    }
}

const excluirPromocoesSucess = (response, dispatch, estabelecimento_id) => {
    dispatch(showLoader(false));
    Actions.pop()
    dispatch(listarPromocoes(estabelecimento_id));
}

const excluirPromocoesFail = (error, dispatch) => {
    dispatch(showLoader(false));

    if (error.response) {
        switch (error.response.status) {
            case 409:
                Alert.alert(strings.default_title_alert, error.response.data.error);
                break;
            default:
                Alert.alert(strings.default_title_alert, `${strings.error_conection_default} Código ${error.response.status}`);
        }
    }
}


export const criarPromocao = (promocao, pontos, descricao, estabelecimento_id) => {

    return dispatch => {

        dispatch(showLoader(true));
        axios.post(endpoints.SALVAR_PROMOCAO,
            {
                promocao,
                pontos: parseInt(pontos),
                descricao,
                estabelecimento_id
            })
            .then(response => criarPromocaoSucess(response, dispatch, estabelecimento_id))
            .catch(error => criarPromocaoFail(error, dispatch));
    }
}

const criarPromocaoSucess = (response, dispatch, estabelecimento_id) => {
    dispatch(showLoader(false));
    Actions.pop()
    dispatch(listarPromocoes(estabelecimento_id));
}

const criarPromocaoFail = (error, dispatch) => {
    dispatch(showLoader(false));


    if (error.response) {
        switch (error.response.status) {
            case 409:
                Alert.alert(strings.default_title_alert, error.response.data.error);
                break;
            default:
                Alert.alert(strings.default_title_alert, `${strings.error_conection_default} Código ${error.response.status}`);
        }
    }
}


export const editarPromocao = (promocao, pontos, id, descricao, estabelecimento_id) => {

    return dispatch => {

        dispatch(showLoader(true));
        axios.post(endpoints.EDITAR_PROMOCAO,
            {
                promocao,
                pontos: parseInt(pontos),
                descricao,
                estabelecimento_id,
                id,
            })
            .then(response => editarEstabelecimentoSucess(response, dispatch, estabelecimento_id))
            .catch(error => editarEstabelecimentoFail(error, dispatch));
    }
}

const editarEstabelecimentoSucess = (response, dispatch, estabelecimento_id) => {
    dispatch(showLoader(false));
    Actions.pop()
    dispatch(listarPromocoes(estabelecimento_id));
}

const editarEstabelecimentoFail = (error, dispatch) => {
    dispatch(showLoader(false));


    if (error.response) {
        switch (error.response.status) {
            case 409:
                Alert.alert(strings.default_title_alert, error.response.data.error);
                break;
            default:
                Alert.alert(strings.default_title_alert, `${strings.error_conection_default} Código ${error.response.status}`);
        }
    }
}

export const listarPromocoes = (estabelecimentoId) => {

    return dispatch => {

        dispatch(showLoader(true));
        axios.get(endpoints.LISTAR_PROMOCOES,
            {
                params: {
                    estabelecimentoId
                }
            })
            .then(response => listarPromocoesSucess(response, dispatch))
            .catch(error => listarPromocoesFail(error, dispatch));
    }
}

const listarPromocoesSucess = (response, dispatch) => {
    dispatch(showLoader(false));
    dispatch(savePromocoes(response.data.promocoes))
}

export const savePromocoes = (promocoes) => {
    return {
        type: SAVE_PROMOCOES,
        payload: promocoes
    }
}

const listarPromocoesFail = (error, dispatch) => {
    dispatch(showLoader(false));

    if (error.response) {
        switch (error.response.status) {
            case 409:
                Alert.alert(strings.default_title_alert, error.response.data.error);
                break;
            default:
                Alert.alert(strings.default_title_alert, `${strings.error_conection_default} Código ${error.response.status}`);
        }
    }
}

export const changePromocao = (promocao) => {
    return dispatch => {
        dispatch(changeNome(promocao.promocao));
        dispatch(changePontos(`${promocao.pontos}`));
        dispatch(changeDescricao(promocao.descricao));
        dispatch(changeId(promocao.id));
    }
}

export const resetPromocao = () => {
    return dispatch => {
        dispatch(changeNome(''));
        dispatch(changePontos(''));
        dispatch(changeDescricao(''));
        dispatch(changeId(0));
    }
}

export const changeNome = (text) => {
    return {
        type: CHANGE_NOME_PROMOCAO,
        payload: text
    }
}

export const changePontos = (text) => {
    return {
        type: CHANGE_PONTOS_PROMOCAO,
        payload: text
    }
}

export const changeDescricao = (text) => {
    return {
        type: CHANGE_DESCRICAO_PROMOCAO,
        payload: text
    }
}

export const changeId = (text) => {
    return {
        type: CHANGE_ID_PROMOCAO,
        payload: text
    }
}