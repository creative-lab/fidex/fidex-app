import axios from 'axios';
import { Alert } from 'react-native';

import { strings } from '../../res/strings';

import {
    CHANGE_NOME_ESTABELECIMENTO,
    CHANGE_EMAIL_ESTABELECIMENTO,
    CHANGE_PHONE_ESTABELECIMENTO,
    CHANGE_WHATSAPP_ESTABELECIMENTO,
    CHANGE_CNPJ_ESTABELECIMENTO,
    CHANGE_ENDERECO_ESTABELECIMENTO,
    CHANGE_BAIRRO_ESTABELECIMENTO,
    CHANGE_CIDADE_ESTABELECIMENTO,
    CHANGE_UF_ESTABELECIMENTO,
    CHANGE_CEP_ESTABELECIMENTO,
    CHANGE_DESCRIPTION_ESTABELECIMENTO,
    CHANGE_KEYWORDS_ESTABELECIMENTO,
    CHANGE_FACEBOOK_ESTABELECIMENTO,
    CHANGE_INSTAGRAM_ESTABELECIMENTO,
    CHANGE_TWITTER_ESTABELECIMENTO,
    CHANGE_CATEGORIA_ESTABELECIMENTO,
    SAVE_ESTABELECIMENTOS,
    CHANGE_ID_ESTABELECIMENTO,
    CHANGE_AVATAR_ESTABELECIMENTO,
    CHANGE_TYPE_OF_SEARCH,
    CHANGE_STATUS_SEARCH_PLACES,
    CHANGE_SEARCH_ESTABELECIMENTO,
    CHANGE_FOLLOW_ESTABELECIMENTO,
    CHANGE_PROMOCOES_ESTABELECIMENTO,
    CHANGE_PROMOCOES_RESGATADAS,
    CHANGE_PONTOS_USUARIO,
    CHANGE_LATITUDE_ESTABELECIMENTO,
    CHANGE_LONGITUDE_ESTABELECIMENTO,
    CHANGE_REGION_ESTABELECIMENTO,
    RESET_REGION_ESTABELECIMENTO,
    CHANGE_REGION_ESTABELECIMENTO2,
    CHANGE_CREDITAR_PONTOS_CPF,
    CHANGE_CREDITAR_PONTOS_VALOR,
    CHANGE_RESGATAR_PROMOCAO_CPF,
    CHANGE_RESGATAR_PROMOCAO_VOUCHER,
    CHANGE_RESGATAR_PROMOCAO_ERROR,
    CHANGE_CLIENTES,
    CHANGE_PONTOS_CREDITADOS,
    CHANGE_RESGATE_PROMOCOES,
    CHANGE_MOVIMENTACOES,
    CHANGE_CLIENTE_ATUAL
} from './types';

import { endpoints } from '../util/endpoints';
import { Actions } from 'react-native-router-flux';

import {
    showLoader
} from './AuthActions';

export const recuperarDadosCliente = (usuarioId, estabelecimentoId) => {
    console.log(`${usuarioId}, ${estabelecimentoId}`)
    return dispatch => {
        dispatch(showLoader(true));
        axios.get(endpoints.RECUPERAR_DADOS_CLIENTE,
            {
                params: {
                    usuarioId,
                    estabelecimentoId
                }
            })
            .then(response => recuperarDadosClienteSucess(response, dispatch))
            .catch(error => recuperarDadosClienteFail(error, dispatch));
    }
}

const recuperarDadosClienteSucess = (response, dispatch) => {
    dispatch(showLoader(false));
    dispatch(changeClienteAtual(response.data))
    console.log(response.data)
}

const recuperarDadosClienteFail = (error, dispatch) => {
    dispatch(showLoader(false));
    if (error.response) {
        console.log(error.response)
        switch (error.response.status) {
            case 409:
                Alert.alert(strings.default_title_alert, error.response.data.error);
                break;
            default:
                Alert.alert(strings.default_title_alert, `${strings.error_conection_default} Código ${error.response.status}`);
        }
    }
}


export const recuperarListaMovimentacoes = (estabelecimentoId) => {

    return dispatch => {

        dispatch(showLoader(true));
        axios.get(endpoints.RECUPERAR_LISTA_MOVIMENTACOES,
            {
                params: {
                    id: estabelecimentoId
                }
            })
            .then(response => recuperarListaMovimentacoesSucess(response, dispatch))
            .catch(error => recuperarListaMovimentacoesFail(error, dispatch));
    }
}

const recuperarListaMovimentacoesSucess = (response, dispatch) => {
    dispatch(showLoader(false));
    dispatch(changeListaMovimentacoes(response.data))
    console.log(response.data)
}

const recuperarListaMovimentacoesFail = (error, dispatch) => {
    dispatch(showLoader(false));
    console.log(error)

    if (error.response) {
        switch (error.response.status) {
            case 409:
                dispatch(changeResgatarPromocaoError(error.response.data.error))
                break;
            default:
                Alert.alert(strings.default_title_alert, `${strings.error_conection_default} Código ${error.response.status}`);
        }
    }
}

export const recuperarListaResgatesPromocao = (estabelecimentoId) => {

    return dispatch => {

        dispatch(showLoader(true));
        axios.get(endpoints.RECUPERAR_RESGATE_PROMOCOES,
            {
                params: {
                    id: estabelecimentoId
                }
            })
            .then(response => recuperarListaResgatesPromocaoSucess(response, dispatch))
            .catch(error => recuperarListaResgatesPromocaoFail(error, dispatch));
    }
}

const recuperarListaResgatesPromocaoSucess = (response, dispatch) => {
    dispatch(showLoader(false));
    dispatch(changeResgatePromocoes(response.data[0]))
}

const recuperarListaResgatesPromocaoFail = (error, dispatch) => {
    dispatch(showLoader(false));
    console.log(error)

    if (error.response) {
        switch (error.response.status) {
            case 409:
                dispatch(changeResgatarPromocaoError(error.response.data.error))
                break;
            default:
                Alert.alert(strings.default_title_alert, `${strings.error_conection_default} Código ${error.response.status}`);
        }
    }
}


export const recuperarListaPontosCreditados = (estabelecimentoId) => {

    return dispatch => {

        dispatch(showLoader(true));
        axios.get(endpoints.RECUPERA_PONTOS_CREDITADOS,
            {
                params: {
                    id: estabelecimentoId
                }
            })
            .then(response => recuperarListaPontosCreditadosSucess(response, dispatch))
            .catch(error => recuperarListaPontosCreditadosFail(error, dispatch));
    }
}

const recuperarListaPontosCreditadosSucess = (response, dispatch) => {
    dispatch(showLoader(false));
    dispatch(changePontosCreditados(response.data[0]))
}

const recuperarListaPontosCreditadosFail = (error, dispatch) => {
    dispatch(showLoader(false));
    console.log(error)

    if (error.response) {
        switch (error.response.status) {
            case 409:
                dispatch(changeResgatarPromocaoError(error.response.data.error))
                break;
            default:
                Alert.alert(strings.default_title_alert, `${strings.error_conection_default} Código ${error.response.status}`);
        }
    }
}


export const recuperarClientes = (estabelecimentoId) => {

    return dispatch => {

        dispatch(showLoader(true));
        axios.get(endpoints.RECUPERA_CLIENTES,
            {
                params: {
                    id: estabelecimentoId
                }
            })
            .then(response => recuperarClientesSucess(response, dispatch))
            .catch(error => recuperarClientesFail(error, dispatch));
    }
}

const recuperarClientesSucess = (response, dispatch) => {
    dispatch(showLoader(false));
    dispatch(changeClientes(response.data[0]))
}

const recuperarClientesFail = (error, dispatch) => {
    dispatch(showLoader(false));
    console.log(error)

    if (error.response) {
        switch (error.response.status) {
            case 409:
                dispatch(changeResgatarPromocaoError(error.response.data.error))
                break;
            default:
                Alert.alert(strings.default_title_alert, `${strings.error_conection_default} Código ${error.response.status}`);
        }
    }
}



export const resgatarPromocao = (cpf, codigo, estabelecimentoId) => {

    return dispatch => {

        dispatch(showLoader(true));
        axios.post(endpoints.ESTABELECIMENTO_RESGATAR_PROMOCAO,
            {
                cpf,
                codigo,
                estabelecimentoId
            })
            .then(response => resgatarPromocaoSucess(response, dispatch))
            .catch(error => resgatarPromocaoFail(error, dispatch));
    }
}

const resgatarPromocaoSucess = (response, dispatch) => {
    dispatch(showLoader(false));
    dispatch(changeResgatarPromocaoCpf(''))
    dispatch(changeRestatarPromocaoVoucher(''))
    dispatch(changeResgatarPromocaoError(''))
    Alert.alert(strings.default_title_alert, response.data.sucesso)
    console.log(response.data.sucesso)
}

const resgatarPromocaoFail = (error, dispatch) => {
    dispatch(showLoader(false));
    console.log(error)

    if (error.response) {
        switch (error.response.status) {
            case 409:
                dispatch(changeResgatarPromocaoError(error.response.data.error))
                break;
            default:
                Alert.alert(strings.default_title_alert, `${strings.error_conection_default} Código ${error.response.status}`);
        }
    }
}

export const creditarPontos = (cpf, vlCompra, estabelecimentoId) => {

    return dispatch => {

        dispatch(showLoader(true));
        axios.get(endpoints.CREDITAR_PONTOS,
            {
                params: {
                    cpf,
                    vlCompra,
                    estabelecimentoId
                }
            })
            .then(response => creditarPontosSucess(response, dispatch))
            .catch(error => creditarPontosFail(error, dispatch));
    }
}

const creditarPontosSucess = (response, dispatch) => {
    dispatch(showLoader(false));
    dispatch(changeCreditarPontosCPF(''))
    dispatch(changeCreditarPontosValor(''))
    Alert.alert(strings.default_title_alert, "Pontos creditados para este usuário.")
}

const creditarPontosFail = (error, dispatch) => {
    dispatch(showLoader(false));
    console.log(error)

    if (error.response) {
        switch (error.response.status) {
            case 409:
                Alert.alert(strings.default_title_alert, error.response.data.error);
                break;
            default:
                Alert.alert(strings.default_title_alert, `${strings.error_conection_default} Código ${error.response.status}`);
        }
    }
}


export const seguirEstabelecimento = (user_id, estabelecimentoId) => {

    let usuarioId = parseInt(user_id.split(':')[1])

    return dispatch => {

        dispatch(showLoader(true));
        axios.get(endpoints.SEGUIR_ESTABELECIMENTO,
            {
                params: {
                    usuarioId,
                    estabelecimentoId
                }
            })
            .then(response => seguirEstabelecimentoSucess(response, dispatch, user_id, estabelecimentoId))
            .catch(error => seguirEstabelecimentoFail(error, dispatch));
    }
}

const seguirEstabelecimentoSucess = (response, dispatch, usuarioId, estabelecimentoId) => {
    dispatch(showLoader(false));
    dispatch(recuperarPaginaEstabelecimentos(usuarioId, estabelecimentoId));
}

const seguirEstabelecimentoFail = (error, dispatch) => {
    dispatch(showLoader(false));
    console.log(error)

    if (error.response) {
        switch (error.response.status) {
            case 409:
                Alert.alert(strings.default_title_alert, error.response.data.error);
                break;
            default:
                Alert.alert(strings.default_title_alert, `${strings.error_conection_default} Código ${error.response.status}`);
        }
    }
}

export const recuperarPaginaEstabelecimentos = (user_id, estabelecimentoId) => {
    
    let usuarioId = parseInt(user_id.split(':')[1])
    return dispatch => {
        dispatch(showLoader(true));
        axios.get(endpoints.DETALHES_ESTABELECIMENTO,
            {
                params: {
                    usuarioId,
                    estabelecimentoId
                }
            })
            .then(response => recuperarPaginaEstabelecimentosSucess(response, dispatch))
            .catch(error => recuperarPaginaEstabelecimentosFail(error, dispatch));
    }
}

const recuperarPaginaEstabelecimentosSucess = (response, dispatch) => {
    dispatch(showLoader(false));
    let estabelecimento = response.data.estabelecimento;
    console.log(estabelecimento)
    
    dispatch(changeEstabelecimento(estabelecimento))
    dispatch(changeFollow(estabelecimento.segue))
    dispatch(changeEndereco(estabelecimento.endereco))
    dispatch(changeBairro(estabelecimento.bairro))
    dispatch(changeCidade(estabelecimento.cidade))
    dispatch(changeFacebook(estabelecimento.facebook))
    dispatch(changeInstagram(estabelecimento.instagram))
    dispatch(changeTwitter(estabelecimento.twitter))
    dispatch(changeUF(estabelecimento.uf))
    dispatch(changeCep(estabelecimento.endecepeco))
    dispatch(changePromocoes(response.data.promocoes))
    if (response.data.pontos_usuario) {
        dispatch(changePontosUsuario(response.data.pontos_usuario))
    } else {
        dispatch(changePontosUsuario(0))
    }
    dispatch(changePromocoesResgatadas(response.data.promocoes_resgatadas))

    

}

const recuperarPaginaEstabelecimentosFail = (error, dispatch) => {
    dispatch(showLoader(false));
    console.log(error.response)

    if (error.response) {
        switch (error.response.status) {
            case 409:
                Alert.alert(strings.default_title_alert, error.response.data.error);
                break;
            default:
                Alert.alert(strings.default_title_alert, `${strings.error_conection_default} Código ${error.response.status}`);
        }
    }
}

export const buscarEstabelecimentos = (lat, long, busca) => {

    return dispatch => {

        dispatch(showLoader(true));
        axios.get(endpoints.BUSCAR_ESTABELECIMENTOS,
            {
                params: {
                    lat,
                    long,
                    busca
                }
            })
            .then(response => buscarEstabelecimentosSucess(response, dispatch))
            .catch(error => buscarEstabelecimentosFail(error, dispatch));
    }
}

const buscarEstabelecimentosSucess = (response, dispatch) => {
    dispatch(showLoader(false));
    console.log(response)
    dispatch(saveEstabelecimentos(response.data));
}

const buscarEstabelecimentosFail = (error, dispatch) => {
    dispatch(showLoader(false));
    console.log(error.response)

    if (error.response) {
        switch (error.response.status) {
            case 409:
                Alert.alert(strings.default_title_alert, error.response.data.error);
                break;
            default:
                Alert.alert(strings.default_title_alert, `${strings.error_conection_default} Código ${error.response.status}`);
        }
    }
}

export const editarEstabelecimento = (nome, email, cnpj,
    endereco, bairro, cidade, uf, cep,
    telefone, whatsapp, descricao, palavra_chave, categoria,
    avatar, facebook, instagram, twitter, id, latitude, longitude) => {

    if (avatar != null && !avatar.includes("data:image/jpeg;base64,")) {
        avatar = `data:image/jpeg;base64,${avatar}`
    }

    return dispatch => {

        dispatch(showLoader(true));
        axios.post(endpoints.EDITAR_ESTABELECIMENTO,
            {
                nome,
                email,
                cnpj,
                endereco,
                bairro,
                cidade,
                uf,
                cep,
                telefone,
                whatsapp,
                descricao,
                palavra_chave,
                categoria,
                avatar: avatar, // passar o avatar como base64
                facebook,
                instagram,
                twitter,
                id,
                latitude,
                longitude
            })
            .then(response => editarEstabelecimentoSucess(response, dispatch))
            .catch(error => editarEstabelecimentoFail(error, dispatch));
    }
}

const editarEstabelecimentoSucess = (response, dispatch) => {
    dispatch(showLoader(false));
    Alert.alert(strings.default_title_alert, "Estabelecimento editado com sucesso");
    Actions.startEmpresas()
}

const editarEstabelecimentoFail = (error, dispatch) => {
    dispatch(showLoader(false));

    console.log(error)

    if (error.response) {
        switch (error.response.status) {
            case 409:
                Alert.alert(strings.default_title_alert, error.response.data.error);
                break;
            default:
                Alert.alert(strings.default_title_alert, `${strings.error_conection_default} Código ${error.response.status}`);
        }
    }
}

export const listarEstabelecimentos = (user_id) => {

    return dispatch => {

        let usuarioId = parseInt(user_id.split(':')[1])
        console.log(usuarioId)

        dispatch(showLoader(true));
        axios.get(endpoints.LISTA_ESTABELECIMENTO,
            {
                params: {
                    usuarioId
                }
            })
            .then(response => listarEstabelecimentosSucess(response, dispatch))
            .catch(error => listarEstabelecimentosFail(error, dispatch));
    }
}

const listarEstabelecimentosSucess = (response, dispatch) => {
    dispatch(showLoader(false));
    dispatch(saveEstabelecimentos(response.data.estabelecimentos))
    if (response.data.estabelecimentos && response.data.estabelecimentos.length > 0) {
        Actions.mainEstabelecimento();
    } else {
        Actions.homeNoneEstabelecimentos();
    }
}

const listarEstabelecimentosFail = (error, dispatch) => {
    dispatch(showLoader(false));

    if (error.response) {
        switch (error.response.status) {
            case 409:
                Alert.alert(strings.default_title_alert, error.response.data.error);
                break;
            default:
                Alert.alert(strings.default_title_alert, `${strings.error_conection_default} Código ${error.response.status}`);
        }
    }
}


export const criarEstabelecimento = (nome, email, cnpj,
    endereco, bairro, cidade, uf, cep,
    telefone, whatsapp, descricao, palavra_chave, categoria,
    avatar, facebook, instagram, twitter, user_id, latitude, longitude) => {

    if (!avatar.includes("data:image/jpeg;base64,")) {
        avatar = `data:image/jpeg;base64,${avatar}`
    }

    return dispatch => {

        let usuario_id = user_id.split(':')[1]
        console.log(usuario_id)
        dispatch(showLoader(true));
        axios.post(endpoints.CADASTRO_ESTABELECIMENTO,
            {
                nome,
                email,
                cnpj,
                endereco,
                bairro,
                cidade,
                uf,
                cep,
                telefone,
                whatsapp,
                descricao,
                palavra_chave,
                categoria,
                avatar, // passar o avatar como base64
                facebook,
                instagram,
                twitter,
                usuario_id,
                latitude,
                longitude
            })
            .then(response => criarEstabelecimentoSucess(response, dispatch, usuario_id))
            .catch(error => criarEstabelecimentoFail(error, dispatch));
    }
}

export const saveEstabelecimentos = (estabelecimentos) => {
    return {
        type: SAVE_ESTABELECIMENTOS,
        payload: estabelecimentos
    }
}

const criarEstabelecimentoSucess = (response, dispatch, usuario_id) => {
    dispatch(showLoader(false));
    Alert.alert(strings.default_title_alert, "Estabelecimento criado com sucesso");
    Actions.startEmpresas()
}

const criarEstabelecimentoFail = (error, dispatch) => {
    dispatch(showLoader(false));

    if (error.response) {
        switch (error.response.status) {
            case 409:
                Alert.alert(strings.default_title_alert, error.response.data.error);
                break;
            default:
                Alert.alert(strings.default_title_alert, `${strings.error_conection_default} Código ${error.response.status}`);
        }
    }
}

export const changeEstabelecimento = (estabelecimento) => {
    return dispatch => {
        dispatch(changeId(estabelecimento.id))
        dispatch(changeName(estabelecimento.nome))
        dispatch(changeEmail(estabelecimento.email))
        dispatch(changePhone(estabelecimento.telefone))
        dispatch(changeAvatar(estabelecimento.avatar))
        dispatch(changeWhatsApp(estabelecimento.whatsapp))
        dispatch(changeCnpj(estabelecimento.cnpj))
        dispatch(changeEndereco(estabelecimento.endereco))
        dispatch(changeBairro(estabelecimento.bairro))
        dispatch(changeCidade(estabelecimento.cidade))
        dispatch(changeUF(estabelecimento.uf))
        dispatch(changeCep(estabelecimento.cep))
        dispatch(changeDescription(estabelecimento.descricao))
        dispatch(changeKeywords(estabelecimento.palavra_chave))
        dispatch(changeFacebook(estabelecimento.facebook))
        dispatch(changeCategory(estabelecimento.categoria))
        dispatch(changeInstagram(estabelecimento.instagram))
        dispatch(changeTwitter(estabelecimento.twitter))
        if (estabelecimento.latitude) {
            dispatch(changeLatitudeEstabelecimento(estabelecimento.latitude))
        } else {
            dispatch(changeLatitudeEstabelecimento(null))
            dispatch(resetRegion())
        }
        if (estabelecimento.longitude) {
            dispatch(changeLongitudeEstabelecimento(estabelecimento.longitude))
        } else {
            dispatch(changeLongitudeEstabelecimento(null))
            dispatch(resetRegion())
        }
    }
}

export const changeResgatarPromocaoError = (error) => {
    return {
        type: CHANGE_RESGATAR_PROMOCAO_ERROR,
        payload: error
    }
}

export const changeListaMovimentacoes = (movimentacoes) => {
    return {
        type: CHANGE_MOVIMENTACOES,
        payload: movimentacoes
    }
}

export const changeResgatePromocoes = (resgates) => {
    return {
        type: CHANGE_RESGATE_PROMOCOES,
        payload: resgates
    }
}

export const changePontosCreditados = (pontosCreditados) => {
    return {
        type: CHANGE_PONTOS_CREDITADOS,
        payload: pontosCreditados
    }
}

export const changeClientes = (clientes) => {
    return {
        type: CHANGE_CLIENTES,
        payload: clientes
    }
}

export const changeResgatarPromocaoCpf = (cpf) => {
    return {
        type: CHANGE_RESGATAR_PROMOCAO_CPF,
        payload: cpf
    }
}

export const changeRestatarPromocaoVoucher = (codigo) => {
    return {
        type: CHANGE_RESGATAR_PROMOCAO_VOUCHER,
        payload: codigo
    }
}

export const changeCreditarPontosValor = (valor) => {
    return {
        type: CHANGE_CREDITAR_PONTOS_VALOR,
        payload: valor
    }
}

export const changeCreditarPontosCPF = (cpf) => {
    return {
        type: CHANGE_CREDITAR_PONTOS_CPF,
        payload: cpf
    }
}

export const resetRegion = () => {
    return {
        type: RESET_REGION_ESTABELECIMENTO
    }
}

export const changeRegiaoEstabelecimento2 = (region) => {
    return {
        type: CHANGE_REGION_ESTABELECIMENTO2,
        payload: region
    }
}

export const changeRegiaoEstabelecimento = (coordinate) => {
    return {
        type: CHANGE_REGION_ESTABELECIMENTO,
        payload: coordinate
    }
}

export const changeLatitudeEstabelecimento = (latitude) => {
    return {
        type: CHANGE_LATITUDE_ESTABELECIMENTO,
        payload: latitude
    }
}

export const changeLongitudeEstabelecimento = (longitude) => {
    return {
        type: CHANGE_LONGITUDE_ESTABELECIMENTO,
        payload: longitude
    }
}

export const changePontosUsuario = (pontos) => {
    return {
        type: CHANGE_PONTOS_USUARIO,
        payload: pontos
    }
}

export const changePromocoesResgatadas = (promocoes) => {
    return {
        type: CHANGE_PROMOCOES_RESGATADAS,
        payload: promocoes
    }
}

export const changeFollow = (folow) => {
    return {
        type: CHANGE_FOLLOW_ESTABELECIMENTO,
        payload: folow
    }
}

export const changeEndereco = (endereco) => {
    return {
        type: CHANGE_ENDERECO_ESTABELECIMENTO,
        payload: endereco
    }
}

export const changeBairro = (bairro) => {
    return {
        type: CHANGE_BAIRRO_ESTABELECIMENTO,
        payload: bairro
    }
}

export const changeCidade = (cidade) => {
    return {
        type: CHANGE_CIDADE_ESTABELECIMENTO,
        payload: cidade
    }
}

export const changeUF = (uf) => {
    return {
        type: CHANGE_UF_ESTABELECIMENTO,
        payload: uf
    }
}

export const changeCep = (cep) => {
    return {
        type: CHANGE_CEP_ESTABELECIMENTO,
        payload: cep
    }
}

export const changeId = (id) => {
    return {
        type: CHANGE_ID_ESTABELECIMENTO,
        payload: id
    }
}

export const changeName = (text) => {
    return {
        type: CHANGE_NOME_ESTABELECIMENTO,
        payload: text
    }
}

export const changePromocoes = (promocoes) => {
    return {
        type: CHANGE_PROMOCOES_ESTABELECIMENTO,
        payload: promocoes
    }
}

export const changeEmail = (text) => {
    return {
        type: CHANGE_EMAIL_ESTABELECIMENTO,
        payload: text
    }
}

export const changePhone = (text) => {
    return {
        type: CHANGE_PHONE_ESTABELECIMENTO,
        payload: text
    }
}

export const changeWhatsApp = (text) => {
    return {
        type: CHANGE_WHATSAPP_ESTABELECIMENTO,
        payload: text
    }
}

export const changeCnpj = (text) => {
    return {
        type: CHANGE_CNPJ_ESTABELECIMENTO,
        payload: text
    }
}

export const changeDescription = (text) => {
    return {
        type: CHANGE_DESCRIPTION_ESTABELECIMENTO,
        payload: text
    }
}

export const changeKeywords = (text) => {
    return {
        type: CHANGE_KEYWORDS_ESTABELECIMENTO,
        payload: text
    }
}
export const changeFacebook = (text) => {
    return {
        type: CHANGE_FACEBOOK_ESTABELECIMENTO,
        payload: text
    }
}

export const changeCategory = (text) => {
    return {
        type: CHANGE_CATEGORIA_ESTABELECIMENTO,
        payload: text
    }
}
export const changeAvatar = (text) => {
    return {
        type: CHANGE_AVATAR_ESTABELECIMENTO,
        payload: text
    }
}

export const changeInstagram = (text) => {
    return {
        type: CHANGE_INSTAGRAM_ESTABELECIMENTO,
        payload: text
    }
}

export const changeTwitter = (text) => {
    return {
        type: CHANGE_TWITTER_ESTABELECIMENTO,
        payload: text
    }
}

export const changeTypeOfSearchPlaces = (text) => {
    return {
        type: CHANGE_TYPE_OF_SEARCH,
        payload: text
    }
}

export const changeStatusSearchPlaces = (text) => {
    return {
        type: CHANGE_STATUS_SEARCH_PLACES,
        payload: text
    }
}

export const changePesquisaEstabelecimento = (text) => {
    return {
        type: CHANGE_SEARCH_ESTABELECIMENTO,
        payload: text
    }
}

export const changeClienteAtual = (cliente) => {
    return {
        type: CHANGE_CLIENTE_ATUAL,
        payload: cliente
    }
}