import React, { Component } from 'react';
import { View, TouchableHighlight, Text, Image } from 'react-native';

import { colors } from '../../../res/colors';

class CustomButtonAux extends Component {

    renderImage() {
        if (this.props.source) {
            return (
                <Image style={{ marginLeft: 20, marginRight: 20, height: 20, width: 15 }} source={this.props.source} />
            )
        }
        return null;
    }
    render() {
        return (
            <TouchableHighlight
                onPress={this.props.onPress}
                underlayColor={colors.bg_color_button_pressed}
                activeOpacity={0.7}>
                <View style={{
                    borderRadius: 6,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderColor: colors.primary_color,
                    borderWidth: 1,
                    width: this.props.width,
                    height: this.props.height ? this.props.height : 40,
                    flexDirection: 'row'
                }}>

                    {this.renderImage()}

                    <Text style={{

                        color: colors.primary_color,
                        fontSize: 16,
                        fontWeight: 'bold',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}>{this.props.title}</Text >
                </View>
            </TouchableHighlight>
        )
    }
}

export default CustomButtonAux;
