import React from 'react';
import { View, TouchableHighlight, Text } from 'react-native';

import { colors } from '../../../res/colors';

const CustomButton = props => (

    <TouchableHighlight
        onPress={props.onPress}
        underlayColor={colors.bg_color_button_pressed}
        activeOpacity={0.7}>
        <View style={{
            backgroundColor: colors.primary_color,
            borderRadius: 6,
            justifyContent: 'center',
            alignItems: 'center',
            width: props.width,
            height: 60
        }}>

            <Text style={{
                color: colors.text_button,
                fontWeight: 'bold',
                fontSize: 18
            }}>{props.title}</Text >
        </View>
    </TouchableHighlight>

);

export default CustomButton;
