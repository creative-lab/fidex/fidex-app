import React from 'react';
import { View, TextInput } from 'react-native';

import { colors } from '../../../res/colors';

const CustomTextArea = props => (


        <View style={{
                borderRadius: 6,
                height: props.height ? props.height : 60,
                width: props.width,
                borderColor: colors.border_color,
                borderWidth: 1
        }}>

                <TextInput
                        style={{
                                flex: 1, height: 80, marginHorizontal: 20, marginVertical: 10
                        }}
                        underlineColorAndroid="#FFFFFF"
                        value={props.value}
                        numberOfLines={50}
                        multiline={true}
                        secureTextEntry={props.secureTextEntry}
                        placeholderTextColor={colors.border_color}
                        onChangeText={props.onChangeText}
                        keyboardType={props.keyboardType}
                        maxLength={props.maxLength ? props.maxLength : 50}
                        returnKeyType={props.returnKeyType ? props.returnKeyType : null}
                        onSubmit={props.onSubmit ? props.onSubmit : null}
                        blurOnSubmit={props.blurOnSubmit}
                        onFocus={props.onFocus}
                        placeholder={props.placeholder} />

        </View>


);

export default CustomTextArea;
