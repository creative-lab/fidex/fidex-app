import React from 'react';
import { View } from 'react-native';
import { Dropdown } from 'react-native-material-dropdown';
import Icon from 'react-native-vector-icons/MaterialIcons'

import { colors } from '../../../res/colors';

const CustomDropdown = props => (

        <View style={{
                borderRadius: 6,
                height: 60,
                width: props.width,
                borderColor: colors.border_color,
                borderWidth: 1}}>
                <View style={{ flex: 1, marginTop: -20, justifyContent: 'center' }}>
                        <Icon name='arrow-drop-down' size={25} color={colors.border_color} style={{ position: 'absolute', right: 0, top: 37 }} />
                        <Dropdown
                                style={{ flex: 1, marginLeft: 20, marginRight: 20 }}
                                label=''
                                labelFontSize={0}
                                onChangeText={props.onChangeText}
                                fontSize={14}
                                baseColor='transparent'
                                selectedItemColor={colors.primary_color}
                                data={props.data}
                                value={props.value}
                                textColor='black'
                        />
                </View>
        </View>
);

export default CustomDropdown;
