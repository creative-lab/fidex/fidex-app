import React from 'react';
import { View, TouchableHighlight, Text } from 'react-native';

import { colors } from '../../../res/colors';

const CustomButton = props => (

    <TouchableHighlight
        onPress={props.onPress}
        style={{ flex: 1, marginHorizontal: 8 }}
        underlayColor={colors.bg_color_button_pressed}
        activeOpacity={0.7}>
        <View style={{
            backgroundColor: colors.primary_color,
            borderRadius: 6,
            alignItems: 'center',
            width: props.width,
            height: 60,
            flexDirection: 'row',
            padding: 8
        }}>
            <Text style={{
                color: colors.text_button,
                fontWeight: 'bold',
                fontSize: 36,
                marginRight: 8
            }}>{props.number}</Text >

            <Text style={{
                flex: 1,
                color: colors.text_button
            }}>{props.title}</Text >
        </View>
    </TouchableHighlight>

);

export default CustomButton;
