import React, { Component } from 'react';
import { View, TextInput } from 'react-native';

import { colors } from '../../../res/colors';

class CustomTextInput extends Component {
        render() {
                return (
                        <View style={{
                                borderRadius: 6,
                                height: 60,
                                width: this.props.width,
                                borderColor: colors.border_color,
                                borderWidth: 1,

                        }}>

                                <TextInput
                                        style={{ flex: 1, height: 80, marginLeft: 20, marginRight: 20 }}
                                        underlineColorAndroid="#FFFFFF"
                                        value={this.props.value}
                                        secureTextEntry={this.props.secureTextEntry}
                                        placeholderTextColor={colors.border_color}
                                        onChangeText={this.props.onChangeText}
                                        keyboardType={this.props.keyboardType}
                                        maxLength={this.props.maxLength ? this.props.maxLength : 50}
                                        returnKeyType={this.props.returnKeyType ? this.props.returnKeyType : null}
                                        blurOnSubmit={this.props.blurOnSubmit}
                                        placeholder={this.props.placeholder}
                                        ref={this.props.refInner}
                                        onSubmitEditing={this.props.onSubmitEditing} />


                        </View>
                )

        }
}

export default CustomTextInput;
