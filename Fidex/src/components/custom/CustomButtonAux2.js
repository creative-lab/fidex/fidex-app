import React from 'react';
import { View, TouchableHighlight, Text} from 'react-native';

import { colors } from '../../../res/colors';

const CustomButtonAux2 = props => (

    <TouchableHighlight
        onPress={props.onPress}
        style={props.style}
        underlayColor={colors.bg_color_button_pressed}
        activeOpacity={0.7}>
        <View style={{
            borderRadius: props.bold ? 9 : 6,
            justifyContent: 'center',
            alignItems: 'center',
            borderColor: colors.primary_color,
            borderWidth: props.bold ? 3 : 1,
            width: props.width,
            minHeight: props.bold ? 40 : 30,
            flexDirection: 'row'
        }}>

            <Text style={{
                color: colors.primary_color,
                marginHorizontal: props.bold ? 12 : 8,
                fontWeight: props.bold ? 'bold' : 'normal',
                fontSize: props.bold ? 18 : 16,
                justifyContent: 'center',
                alignItems: 'center',
                textAlign: 'center'
            }}>{props.title}</Text >
        </View>
    </TouchableHighlight>
);

export default CustomButtonAux2;
