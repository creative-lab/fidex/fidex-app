import React, { Component } from 'react';
import { View, TouchableHighlight, AsyncStorage, Text, Platform } from 'react-native';
import { Router, Scene, Stack, Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons'

import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconMoomConfig from '../selection.json';
const IconCustom = createIconSetFromIcoMoon(iconMoomConfig);

import Splash from "./auth/Splash";
import Start from "./auth/Start";
import Login from "./auth/Login";
import LoginEmpresas from "./auth/LoginEmpresas";
import CreateAccount from "./auth/CreateAccount";
import CreateAccountEmrpesaPasso1 from "./auth/CreateAccountEmrpesaPasso1";
import InformeCPF from "./auth/InformeCPF";
import Main from "./main/Main";
import MainEstabelecimento from "./main/MainEstabelecimento";
import Settings from "./main/Settings";
import Assinar from "./auth/Assinar";
import HomeEmpresas from './main/HomeEmpresas';
import HomeNoneEstabelecimento from './main/HomeNoneEstabelecimento';
import CriarEstabelecimentoPasso1 from './main/CriarEstabelecimentoPasso1';
import CriarEstabelecimentoPasso2 from './main/CriarEstabelecimentoPasso2';
import CriarEstabelecimentoPasso3 from './main/CriarEstabelecimentoPasso3';
import CriarEstabelecimentoPasso4 from './main/CriarEstabelecimentoPasso4';
import StartEmpresas from './main/StartEmpresas';
import EstabelecimentoDetalhe from './main/EstabelecimentoDetalhe';
import EditarPromocao from './main/estabelecimento/EditarPromocao';
import PaginaEstabelecimento from './main/usuario/PaginaEstabelecimento';
import ListaClientes from './main/estabelecimento/ListaClientes';
import ListaMovimentacao from './main/estabelecimento/ListaMovimentacao';
import PontosCreditados from './main/estabelecimento/PontosCreditados';
import ResgateDePromocoes from './main/estabelecimento/ResgateDePromocoes';
import ClienteEstabelecimento from './main/estabelecimento/ClienteEstabelecimento';
import Voucher from './main/usuario/Voucher';
import Termos from './main/Termos';
import UpdatePass from './main/UpdatePass';
import EditProfileAdm from './main/estabelecimento/EditProfileAdm';
import EditProfileUser from './main/usuario/EditProfileUser';

import { colors } from '../../res/colors';
import { strings } from '../../res/strings';

import {
    changeTypeOfSearchPlaces,
    changeStatusSearchPlaces
} from '../actions/MainActions'

import {
    toogleUserLogged
} from '../actions/AuthActions'

import {
    resetPromocao
} from '../actions/PromocaoActions'


class Routes extends Component {

    getData = async () => {
        const item = await AsyncStorage.getItem("id")
        // console.log(item)
        if (item !== null) {
            this.props.toogleUserLogged(item)
            if (item.includes("usuario")) {
                Actions.main();
            } else if (item.includes("estabelecimento")) {
                Actions.startEmpresas()
            } else {
                Actions.start()
            }
        } else {
            Actions.start()
        }
    }

    componentWillMount() {
        this.getData()
    }

    changeStatus() {
        if (this.props.statusBuscaPorLocais === 'normal') {
            this.props.changeStatusSearchPlaces('pesquisando')
        } else {
            this.props.changeStatusSearchPlaces('normal')
        }
    }

    renderCustomTitle() {
        if (this.props.titleSelectedTab === "Explorar") {
            if (Platform.OS === 'ios') {
                return (
                    <TouchableHighlight
                        underlayColor={colors.bg_color_button_pressed}
                        activeOpacity={0.7}
                        onPress={() => this.changeStatus()} >
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={{ fontWeight: 'bold', fontSize: 17, color: colors.primary_color }}>{this.props.tipoBuscaPorLocais}</Text>
                            <Icon style={{ marginLeft: 16 }} name='keyboard-arrow-down' size={32} color={colors.primary_color} />

                        </View>
                    </TouchableHighlight>
                )
            } else {
                return (
                    <TouchableHighlight
                        underlayColor={colors.bg_color_button_pressed}
                        activeOpacity={0.7}
                        onPress={() => this.changeStatus()} >
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={{ fontWeight: 'bold', marginLeft: 16, fontSize: 20, color: colors.primary_color }}>{this.props.tipoBuscaPorLocais}</Text>
                            <Icon style={{ marginLeft: 16 }} name='keyboard-arrow-down' size={32} color={colors.primary_color} />
                        </View>
                    </TouchableHighlight>
                )
            }
        }
    }

    render() {
        return (
            <Router titleStyle={{ color: colors.primary_color }} nav tintColor={colors.primary_color}>
                <Stack >
                    <Scene key='splash' component={Splash} title="Splash" hideNavBar initial />
                    <Scene key='start' component={Start} title="Início" hideNavBar type="reset" />
                    <Scene key='login' component={Login} title="Login" hideNavBar />
                    <Scene key='createAccount' component={CreateAccount} title="CreateAccount" hideNavBar />
                    <Scene key='createAccountEmrpesaPasso1' component={CreateAccountEmrpesaPasso1} title="CreateAccountEmrpesaPasso1" hideNavBar />

                    <Scene key='informeCPF' component={InformeCPF} title="InformeCPF" hideNavBar />
                    <Scene key='main' component={Main} title={this.props.titleSelectedTab} type="reset"
                        renderRightButton=
                        {
                            () => this.renderRigthMenuUsuario()
                        }
                        renderTitle={this.renderCustomTitle()}
                    />
                    <Scene key='settings' component={Settings} title="Opções" />
                    <Scene key='assinar' component={Assinar} title="Novo estabelecimento" />
                    <Scene key='homeEmpresas' component={HomeEmpresas} title="Meu Fidex" type="reset"
                        renderRightButton=
                        {
                            () =>
                                <TouchableHighlight

                                    underlayColor={colors.bg_color_button_pressed}
                                    activeOpacity={0.7}
                                    onPress={() => Actions.settings()} >
                                    <IconCustom style={{ paddingHorizontal: 16 }} name='config' size={32} color={colors.primary_color} />
                                </TouchableHighlight>
                        }
                    />

                    <Scene key='homeNoneEstabelecimentos' component={HomeNoneEstabelecimento} title="Meu Fidex" type="reset"
                        renderRightButton=
                        {
                            () =>
                                <TouchableHighlight

                                    underlayColor={colors.bg_color_button_pressed}
                                    activeOpacity={0.7}
                                    onPress={() => Actions.settings()} >
                                    <IconCustom style={{ paddingHorizontal: 16 }} name='config' size={32} color={colors.primary_color} />
                                </TouchableHighlight>
                        }
                    />

                    <Scene key='mainEstabelecimento' component={MainEstabelecimento} title={this.props.titleSelectedTab} type="reset"
                        renderRightButton=
                        {
                            () =>
                                this.renderRigthMenu()
                        } />

                    <Scene key='startEmpresas' component={StartEmpresas} type="reset" hideNavBar />
                    <Scene key='loginEmpresas' component={LoginEmpresas} title="Login" hideNavBar />
                    <Scene key='criarEstabelecimentoPasso1' component={CriarEstabelecimentoPasso1} title="" />
                    <Scene key='criarEstabelecimentoPasso2' component={CriarEstabelecimentoPasso2} title="" />
                    <Scene key='criarEstabelecimentoPasso3' component={CriarEstabelecimentoPasso3} title="" />
                    <Scene key='criarEstabelecimentoPasso4' component={CriarEstabelecimentoPasso4} title="" />
                    <Scene key='estabelecimentoDetalhe' component={EstabelecimentoDetalhe} title="Estabelecimento" />

                    <Scene key='editarPromocao' component={EditarPromocao} title={this.props.titlePromocao} />
                    <Scene key='paginaEstabelecimento' component={PaginaEstabelecimento} title="Estabelecimento" />
                    <Scene key='listaClientes' component={ListaClientes} title={strings.clientes_estabelecimento} />
                    <Scene key='listaMovimentacao' component={ListaMovimentacao} title={strings.lista_movimentacao} />
                    <Scene key='pontosCreditados' component={PontosCreditados} title={strings.pontos_creditados} />
                    <Scene key='resgateDePromocoes' component={ResgateDePromocoes} title={strings.resgate_promocoes} />
                    <Scene key='clienteEstabelecimento' component={ClienteEstabelecimento} title='Cliente' />
                    <Scene key='voucher' component={Voucher} title='Voucher' />
                    <Scene key='termos' component={Termos} title={strings.terms} />
                    <Scene key='updatePass' component={UpdatePass} title={strings.update_pass} />
                    <Scene key='editProfileAdm' component={EditProfileAdm} title={strings.edit_profile} />
                    <Scene key='editProfileUser' component={EditProfileUser} title={strings.edit_profile} />


                </Stack>

            </Router>
        )
    }

    renderRigthMenuUsuario() {
        if (this.props.titleSelectedTab === "Meu Fidex") {
            return (
                <TouchableHighlight

                    underlayColor={colors.bg_color_button_pressed}
                    activeOpacity={0.7}
                    onPress={() => Actions.settings()} >
                    <IconCustom style={{ paddingHorizontal: 16 }} name='config' size={32} color={colors.primary_color} />
                </TouchableHighlight>
            )
        }
    }

    renderRigthMenu() {
        if (this.props.titleSelectedTab === "Promoções") {
            return (
                <TouchableHighlight
                    underlayColor={colors.bg_color_button_pressed}
                    activeOpacity={0.7}
                    onPress={() => { this.props.resetPromocao(); Actions.editarPromocao() }}>
                    <Icon style={{ paddingHorizontal: 16 }} name='add' size={32} color={colors.primary_color} />
                </TouchableHighlight>);
        } if (this.props.titleSelectedTab === "Meu Fidex") {
            return (
                <TouchableHighlight
                    underlayColor={colors.bg_color_button_pressed}
                    activeOpacity={0.7}
                    onPress={() => Actions.settings()}>

                    <IconCustom style={{ paddingHorizontal: 16 }} name='config' size={32} color={colors.primary_color} />
                </TouchableHighlight>);
        }
    }
}

mapStateToProps = state => (
    {
        titleSelectedTab: state.MainReducer.titleSelectedTab,
        tipoBuscaPorLocais: state.EstabelecimentoReducer.tipoBuscaPorLocais,
        statusBuscaPorLocais: state.EstabelecimentoReducer.statusBuscaPorLocais,
        idPromocao: state.PromocaoReducer.id,
        titlePromocao: state.PromocaoReducer.titleEdicao
    }
)

export default connect(mapStateToProps, { toogleUserLogged, resetPromocao, changeTypeOfSearchPlaces, changeStatusSearchPlaces })(Routes);