import React, { Component } from 'react';
import { View, Image, TouchableHighlight } from 'react-native';
import { Actions } from 'react-native-router-flux';

defaultImage = require('../../../res/imgs/base.png');
import { colors } from '../../../res/colors'

class ItemLugares extends Component {
    
    renderBadge() {
        if (this.props.lugar.badge) {
            return (
                <View style={{ position: "absolute", height: 20, width: 20, borderRadius: 10, backgroundColor: 'red', top: 10, right: 10 }} />
            );
        }
    }

    abrirEstabelecimento() {
        // console.log(this.props.lugar)
        estabelecimento = {
            id: this.props.lugar.id
        }
        Actions.paginaEstabelecimento({ estabelecimento })
    }

    render() {
        return (
            <TouchableHighlight
                underlayColor={colors.bg_color_button_pressed}
                activeOpacity={0.7}
                onPress={() => this.abrirEstabelecimento()} >
                <View >
                    <Image style={{ height: 90, width: 90, borderRadius: 45 }} source={{ uri: this.props.lugar.avatar }} />
                    {this.renderBadge()}

                </View>
            </TouchableHighlight>
        );
    }
}

export default ItemLugares;