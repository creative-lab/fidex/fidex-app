import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import TabNavigator from 'react-native-tab-navigator';

import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconMoomConfig from '../../selection.json';
const Icon = createIconSetFromIcoMoon(iconMoomConfig);

import { colors } from '../../../res/colors';
import Home from './Home';
import Browser from './Browser';
import MyPlaces from './MyPlaces';
import MyWallet from './MyWallet';

import { changeTab } from '../../actions/MainActions';

class Main extends Component {

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.bg }}>

                <TabNavigator style={{ flex: 1, }}>
                    <TabNavigator.Item
                        selected={this.props.selectedTab === 'home'}
                        tabStyle={{ marginBottom: -7 }}
                        selectedTitleStyle={{ color: "#3496f0" }}
                        renderIcon={() => <Icon name='home' size={40} color={colors.border_color} />}
                        renderSelectedIcon={() => <Icon name='home' size={40} color={colors.primary_color} />}
                        onPress={() => this.props.changeTab('home')}>
                        <Home />

                    </TabNavigator.Item>

                    <TabNavigator.Item
                        selected={this.props.selectedTab === 'browser'}
                        tabStyle={{ marginBottom: -7 }}
                        selectedTitleStyle={{ color: "#3496f0" }}
                        renderIcon={() => <Icon name='compass' size={40} color={colors.border_color} /> }
                        renderSelectedIcon={() => <Icon name='compass' size={40} color={colors.primary_color} />}
                        onPress={() => this.props.changeTab('browser')}>
                        <Browser />
                    </TabNavigator.Item>

                    <TabNavigator.Item
                        selected={this.props.selectedTab === 'myplaces'}
                        tabStyle={{ marginBottom: -7 }}
                        selectedTitleStyle={{ color: "#3496f0" }}
                        renderIcon={() => <Icon name='business' size={40} color={colors.border_color} />}
                        renderSelectedIcon={() => <Icon name='business' size={40} color={colors.primary_color} />}
                        onPress={() => this.props.changeTab('myplaces')}>
                        <MyPlaces />
                    </TabNavigator.Item>

                    <TabNavigator.Item
                        selected={this.props.selectedTab === 'mywallet'}
                        tabStyle={{ marginBottom: -7 }}
                        selectedTitleStyle={{ color: "#3496f0" }}
                        renderIcon={() => <Icon name='wallet' size={40} color={colors.border_color} />}
                        renderSelectedIcon={() => <Icon name='wallet' size={40} color={colors.primary_color} />}
                        onPress={() => this.props.changeTab('mywallet')}>
                        <MyWallet />
                    </TabNavigator.Item>

                </TabNavigator>

            </View>
        )
    }
}

mapStateToProps = state => (
    {
        selectedTab: state.MainReducer.selectedTab
    }
)


export default connect(mapStateToProps, { changeTab })(Main);
