import React, { Component } from 'react';
import { View } from 'react-native';
import SegmentedControlTab from 'react-native-segmented-control-tab';

import { colors } from '../../../res/colors';

import ResgatarPromocaoForm from './estabelecimento/ResgatarPromocaoForm';
import CreditarPontosForm from './estabelecimento/CreditarPontosForm';

class Transation extends Component {

    constructor() {
        super()
        this.state = {
            selectedIndex: 0,
        };
    }

    handleIndexChange = (index) => {
        this.setState({
            ...this.state,
            selectedIndex: index,
        });
    }

    renderTabContent() {
        if (this.state.selectedIndex === 0) {
            return (<CreditarPontosForm />)
        } else {
            return (<ResgatarPromocaoForm />)
        }
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ marginTop: 16, marginHorizontal: 16 }}>
                    <SegmentedControlTab

                        tabTextStyle={{ color: colors.primary_color }}
                        activeTabTextStyle={{ color: 'white' }}
                        tabStyle={{ borderColor: colors.primary_color }}
                        activeTabStyle={{ backgroundColor: colors.primary_color }}
                        values={['Creditar Pontos', 'Resgatar promoção']}
                        selectedIndex={this.state.selectedIndex}
                        onTabPress={this.handleIndexChange}
                    />
                </View>
                {this.renderTabContent()}
            </View>
        )
    }
}

export default Transation;