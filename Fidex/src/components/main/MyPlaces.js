import React, { Component } from 'react';
import { View, ListView, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

import { colors } from '../../../res/colors';
import ItemMeusEstabelecimentos from './usuario/ItemMeusEstabelecimentos';
import { recuperarLocaisDoUsuario } from '../../actions/UsuarioActions';

class MyPlaces extends Component {

    componentDidMount() {
        this.props.recuperarLocaisDoUsuario(this.props.userId);
    }

    renderListEstabelecimentos() {
        if (this.props.meusLugares) {
            const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })

            let fonteDeDadosEstabelecimentos = ds.cloneWithRows(this.props.meusLugares)

            return (
                <ListView
                    enableEmptySections
                    dataSource={fonteDeDadosEstabelecimentos}
                    renderSeparator={(rowId) =>
                        <View key={rowId} style={{
                            flex: 1,
                            marginHorizontal: 16,
                            height: StyleSheet.hairlineWidth,
                            backgroundColor: colors.border_color,
                        }}
                        />}
                    renderRow={estabelecimento => <ItemMeusEstabelecimentos estabelecimento={estabelecimento} />} />
            )
        }
    }

    render() {
        return (
            <View style={{ flex: 1, }}>
                {this.renderListEstabelecimentos()}
            </View>
        )
    }
}

mapStateToProps = state => (
    {
        userId: state.AuthReducer.id,
        meusLugares: state.UsuarioReducer.meusLugares
    }
)

export default connect(mapStateToProps, { recuperarLocaisDoUsuario })(MyPlaces);