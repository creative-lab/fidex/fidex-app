import React, { Component } from 'react';
import { View, Text, Image, ScrollView, Alert, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-customized-image-picker';

import CustomButton from '../custom/CustomButton';
import CustomTextInput from '../custom/CustomTextInput';

import { colors } from '../../../res/colors';
import { strings } from '../../../res/strings';

import { changeFacebook, changeInstagram, changeTwitter, criarEstabelecimento, editarEstabelecimento } from '../../actions/EstabelecimentoActions';

import { addProviderInUrl } from '../../util/Utils'

var selectdImage = '';

class CriarEstabelecimentoPasso4 extends Component {

    constructor() {
        super();
        this.state = {
            images: null
        };
    }

    pickSingleBase64(cropit) {
        ImagePicker.openPicker({
            width: 300,
            height: 300,
            cropping: cropit,
            includeBase64: true,
            isCamera: true
        }).then(images => {
            this.setState({
                ...this.state,
                images: images.map(i => {
                    console.log(`received image ${i.mine}`);
                    return { uri: i.path, width: i.width, height: i.height, mime: i.mime, data: i.data };
                })
            });
        }).catch(e => { alert(e); console.log(e) });
    }

    renderLocalImage(image) {

        selectdImage = image.data
        console.log(selectdImage)

        return (
            <Image
                style={{
                    borderRadius: 50,
                    resizeMode: 'contain',
                    height: 99,
                    width: 99,
                    borderColor: colors.border_color,
                    borderWidth: 0,
                }}
                source={image} />
        )

    }

    renderRemoteImage() {
        return (
            <Image
                style={{
                    height: 99,
                    width: 99,
                    borderRadius: 50
                }}
                source={{ uri: this.props.avatar }} />
        )
    }

    renderImageLogalUri() {

        return this.state.images ? this.state.images.map(i =>
            <View style={{
                borderColor: colors.bg,
                borderWidth: 0,
            }} key={i.uri}>

                {
                    this.renderLocalImage(i)
                }

            </View>) : this.renderRemoteImage();
    }

    salvarEstabelecimento() {

        // addProviderInUrl('facebook.com', this.props.changeFacebook, this.props.facebook)
        // addProviderInUrl('instagram.com',  (a) => this.props.changeInstagram(a), this.props.instagram)
        // addProviderInUrl('twitter.com', this.props.changeTwitter, this.props.twitter)

        // console.log(this.props.instagram)

        if (this.props.estabelecimento_id == 0) {

            this.props.criarEstabelecimento(
                this.props.nome,
                this.props.email,
                this.props.cnpj,
                this.props.endereco,
                this.props.bairro,
                this.props.cidade,
                this.props.uf,
                this.props.cep,
                this.props.phone,
                this.props.whatsapp,
                this.props.description,
                this.props.keywords,
                this.props.category,
                selectdImage,
                this.props.facebook,
                this.props.instagram,
                this.props.twitter,
                this.props.userId,
                this.props.latitude,
                this.props.longitude
            )
        } else {
            this.props.editarEstabelecimento(
                this.props.nome,
                this.props.email,
                this.props.cnpj,
                this.props.endereco,
                this.props.bairro,
                this.props.cidade,
                this.props.uf,
                this.props.cep,
                this.props.phone,
                this.props.whatsapp,
                this.props.description,
                this.props.keywords,
                this.props.category,
                selectdImage == '' ? null : selectdImage,
                this.props.facebook,
                this.props.instagram,
                this.props.twitter,
                this.props.estabelecimento_id,
                this.props.latitude,
                this.props.longitude
            )
        }
    }

    checkData() {
        let message = '';
        if (!selectdImage && !this.props.avatar) {
            message += 'Avatar';
        }

        return message;
    }

    attemptCreateEstabelecimento() {
        let message = this.checkData();


        if (message) {
            Alert.alert(strings.title_error, `${strings.error_requireds_fiels} ${message}`);
        } else {
            this.salvarEstabelecimento()
        }
    }

    render() {

        return (
            <View style={{ flex: 1, backgroundColor: colors.bg }}>
                <ScrollView style={{ flex: 1 }}>
                    <View style={{ alignItems: 'center', flex: 1, paddingBottom: 16, paddingTop: 32 }}>

                        <Text style={{ fontSize: 20, textAlign: 'center', color: colors.primary_color, paddingHorizontal: 16 }}>Esse é o último passo! Ufa.</Text>

                        <View style={{ height: 20 }} />

                        <View style={{
                            borderRadius: 50,
                            height: 100,
                            width: 100,
                            borderColor: colors.border_color,
                            borderWidth: 1,

                        }}>
                            {this.renderImageLogalUri()}
                        </View>

                        <TouchableHighlight onPress={() => this.pickSingleBase64(true)} underlayColor={colors.bg_color_button_pressed}
                            style={{ margin: 16 }}
                            activeOpacity={0.7}>
                            <Text style={{ color: colors.primary_color }}>{this.props.estabelecimento_id == 0 ? "Adicionar avatar" : "Editar avatar"}</Text >
                        </TouchableHighlight>

                        <Text style={{ fontSize: 10, color: colors.border_color, width: 260, textAlign: 'center' }} >Você pode adicionar a logo ou uma imagem do seu estabelecimento.</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ fontSize: 10, color: colors.border_color, fontWeight: 'bold' }}>Dica: </Text>
                            <Text style={{ fontSize: 10, color: colors.border_color, fontStyle: 'italic' }} >Inserir sua marca ajuda a ter mais credibilidade </Text>
                        </View>

                        <View style={{ height: 15 }} />

                        <CustomTextInput
                            placeholder={strings.placeholder_facebook}
                            width={260}
                            onChangeText={text => this.props.changeFacebook(text)}
                            value={this.props.facebook}
                            returnKeyType={"next"}
                            keyboardType='url'
                            blurOnSubmit={false} />

                        <View style={{ height: 15 }} />

                        <CustomTextInput
                            placeholder={strings.placeholder_instagram}
                            width={260}
                            onChangeText={text => this.props.changeInstagram(text)}
                            value={this.props.instagram}
                            returnKeyType={"next"}
                            keyboardType='url'
                            blurOnSubmit={false} />

                        <View style={{ height: 15 }} />

                        <CustomTextInput
                            placeholder={strings.placeholder_twitter}
                            width={260}
                            onChangeText={text => this.props.changeTwitter(text)}
                            value={this.props.twitter}
                            returnKeyType={"next"}
                            keyboardType='url'
                            blurOnSubmit={false} />

                        <View style={{ height: 15 }} />

                        <CustomButton
                            title="Finalizar"
                            width={260}
                            onPress={() => this.attemptCreateEstabelecimento()} />

                    </View>
                </ScrollView>
            </View>
        )
    }


}

mapStateToProps = state => (
    {
        userId: state.AuthReducer.id,
        facebook: state.EstabelecimentoReducer.facebook,
        instagram: state.EstabelecimentoReducer.instagram,
        twitter: state.EstabelecimentoReducer.twitter,
        estabelecimento_id: state.EstabelecimentoReducer.id,
        avatar: state.EstabelecimentoReducer.avatar,
    }
)

export default connect(mapStateToProps, { changeFacebook, changeInstagram, changeTwitter, criarEstabelecimento, editarEstabelecimento })(CriarEstabelecimentoPasso4);