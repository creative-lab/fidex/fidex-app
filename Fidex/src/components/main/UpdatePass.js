import React, { Component } from 'react'
import { View, ScrollView, Alert } from 'react-native'
import { connect } from 'react-redux';

import { strings } from '../../../res/strings'
import CustomTextInput from '../custom/CustomTextInput'
import CustomButton from '../custom/CustomButton'

import { changeNewPass, changeConfirmNewPass, alterarSenha } from '../../actions/AuthActions'
import { colors } from '../../../res/colors'

class UpdatePass extends Component {

    checkData() {
        let message = '';
        if (!this.props.newPass) {
            message += 'Senha';
        }
        if (!this.props.confirmeNewPass) {
            if (message) {
                message += ', ';
            }
            message += 'Confirmação';
        }
        return message;
    }

    attemptLogin() {
        let message = this.checkData();
        if (message) {
            Alert.alert(strings.title_error, `${strings.error_requireds_fiels} ${message}`);
        } else if (this.props.newPass !== this.props.confirmeNewPass) {
            Alert.alert(strings.title_error, "Senha e confirmação não são iguais");
        } else {
            this.props.alterarSenha(this.props.newPass, this.props.confirmeNewPass, this.props.userId)
        }
    }

    componentWillUnmount() {
        this.props.changeNewPass('')
        this.props.changeConfirmNewPass('')
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.bg }}>
                <ScrollView style={{ flex: 1 }}>
                    <View style={{ alignItems: 'center', flex: 1, paddingBottom: 16, paddingTop: 36 }}>

                        <CustomTextInput
                            placeholder="Digite a nova senha"
                            width={260}
                            onChangeText={text => this.props.changeNewPass(text)}
                            value={this.props.newPass}
                            maxLength={14}
                            secureTextEntry
                            returnKeyType={"next"}
                            blurOnSubmit={false}
                            onSubmitEditing={() => this.customInput.refs.inputPass.focus()}
                            refInner="inputPass"
                        />
                        <View style={{ height: 15 }} />

                        <CustomTextInput
                            placeholder="Repita a nova senha"
                            width={260}
                            returnKeyType={"done"}
                            blurOnSubmit={false}
                            onChangeText={text => this.props.changeConfirmNewPass(text)}
                            secureTextEntry
                            maxLength={10}
                            value={this.props.confirmeNewPass}
                            refInner="inputPass"
                            onSubmitEditing={() => this.attemptLogin()}
                            ref={ref => this.customInput = ref}
                        />
                        <View style={{ height: 36 }} />

                        <CustomButton
                            title="Concluir"
                            width={260}
                            onPress={() => this.attemptLogin()} />

                    </View>
                </ScrollView>
            </View>
        )
    }
}

mapStateToProps = state => (
    {
        userId: state.AuthReducer.id,
        newPass: state.AuthReducer.newPass,
        confirmeNewPass: state.AuthReducer.confirmeNewPass,
    }
)

export default connect(mapStateToProps, { changeNewPass, changeConfirmNewPass, alterarSenha })(UpdatePass);