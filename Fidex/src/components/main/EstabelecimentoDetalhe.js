import React, { Component } from 'react';
import { View, Text, Image, ScrollView, Alert, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';
import Communications from 'react-native-communications';

import { colors } from '../../../res/colors';
import CustomButtonAux2 from '../custom/CustomButtonAux2';

import { Actions } from 'react-native-router-flux';
import { strings } from '../../../res/strings';

import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconMoomConfig from '../../selection.json';
const Icon = createIconSetFromIcoMoon(iconMoomConfig);
import { formatUrl } from '../../util/Utils'

import { changeEstabelecimento } from '../../actions/EstabelecimentoActions';

class EstabelecimentoDetalhe extends Component {

    renderFacebookButton() {
        let link = formatUrl(this.props.estabelecimento.facebook);
        return (
            <TouchableHighlight
                style={{ marginRight: 8 }}
                underlayColor={colors.bg_color_button_pressed}
                activeOpacity={0.7}
                onPress={() =>
                    link ?
                        Communications.web(link) :
                        Alert.alert("Facebook", strings.link_not_found)
                } >
                <Icon name='facebook_ico' size={40} color={colors.primary_color} />
            </TouchableHighlight>
        )
    }

    renderInstagramButton() {
        let link = formatUrl(this.props.estabelecimento.instagram);
        return (
            <TouchableHighlight
                style={{ marginRight: 8 }}
                underlayColor={colors.bg_color_button_pressed}
                activeOpacity={0.7}
                onPress={() =>
                    link ?
                        Communications.web(link) :
                        Alert.alert("Instagram", strings.link_not_found)
                } >
                <Icon name='instagram_ico' size={40} color={colors.primary_color} />
            </TouchableHighlight>
        )
    }

    renderTwitterButton() {
        let link = formatUrl(this.props.estabelecimento.twitter);
        return (
            <TouchableHighlight
                style={{ marginRight: 8 }}
                underlayColor={colors.bg_color_button_pressed}
                activeOpacity={0.7}
                onPress={() =>
                    link ?
                        Communications.web(link) :
                        Alert.alert("Twitter", strings.link_not_found)
                } >
                <Icon name='twitter_ico' size={40} color={colors.primary_color} />
            </TouchableHighlight>
        )
    }

    openEdition() {
        this.props.changeEstabelecimento(this.props.estabelecimento)
        Actions.criarEstabelecimentoPasso1()
    }


    render() {

        return (
            <ScrollView style={{ flex: 1, backgroundColor: colors.bg, padding: 16 }}>
                <View >

                    <CustomButtonAux2
                        style={{ position: "absolute", top: 0, right: 0 }}
                        underlayColor={colors.bg_color_button_pressed}
                        activeOpacity={0.7}
                        title="Editar"
                        onPress={() => this.openEdition()} />

                    <View style={{ flexDirection: 'row', marginVertical: 20 }}>
                        <View style={{ height: 80, width: 80, borderRadius: 40, backgroundColor: colors.border_color }}>
                            <Image source={{ uri: this.props.estabelecimento.avatar }}
                                style={{ height: 80, width: 80, borderRadius: 40 }} />
                        </View>

                        <View style={{ marginLeft: 16, justifyContent: 'center' }}>

                            <Text style={{ color: colors.primary_color, fontSize: 20, fontWeight: 'bold' }}>{this.props.estabelecimento.nome}</Text>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={{ color: colors.primary_color, fontSize: 20 }}>CNPJ:</Text>
                                <Text style={{ marginLeft: 8, color: colors.primary_color, fontSize: 20 }}>{this.props.estabelecimento.cnpj ? this.props.estabelecimento.cnpj : "Não informado"}</Text>
                            </View>
                        </View>
                    </View>

                    <Text style={{ color: colors.border_color }}>{this.props.estabelecimento.descricao}</Text>
                    <View style={{ width: '100%', height: 1, backgroundColor: colors.border_color, marginVertical: 20 }} />

                    <View style={{ marginBottom: 8 }}>
                        <Text style={{ color: colors.primary_color, fontWeight: 'bold' }}>E-mail</Text>
                        <Text style={{ color: colors.border_color }}>{this.props.estabelecimento.email}</Text>
                    </View>

                    <View style={{ marginBottom: 8 }}>
                        <Text style={{ color: colors.primary_color, fontWeight: 'bold' }}>Telefone</Text>
                        <Text style={{ color: colors.border_color }}>{this.props.estabelecimento.telefone}</Text>
                    </View>

                    <View style={{ marginBottom: 8 }}>
                        <Text style={{ color: colors.primary_color, fontWeight: 'bold' }}>WhatsApp</Text>
                        <Text style={{ color: colors.border_color }}>{this.props.estabelecimento.whatsapp}</Text>
                    </View>

                    <View style={{ flexDirection: 'row' }}>
                        {this.renderFacebookButton()}
                        {this.renderInstagramButton()}
                        {this.renderTwitterButton()}
                    </View>

                    <View style={{ width: '100%', height: 1, backgroundColor: colors.border_color, marginVertical: 20 }} />

                    <View style={{ marginBottom: 8 }}>
                        <Text style={{ color: colors.primary_color, fontWeight: 'bold' }}>Categoria</Text>
                        <Text style={{ color: colors.border_color }}>{this.props.estabelecimento.categoria}</Text>
                    </View>

                    <View style={{ marginBottom: 8 }}>
                        <Text style={{ color: colors.primary_color, fontWeight: 'bold' }}>Palavras-chave</Text>
                        <Text style={{ color: colors.border_color }}>{this.props.estabelecimento.palavra_chave}</Text>
                    </View>

                </View>
            </ScrollView>
        )
    }
}


mapStateToProps = state => (
    {
        titleSelectedTab: state.MainReducer.titleSelectedTab
    }
)

export default connect(mapStateToProps, { changeEstabelecimento })(EstabelecimentoDetalhe);