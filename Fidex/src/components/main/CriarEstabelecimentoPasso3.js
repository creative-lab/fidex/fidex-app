import React, { Component } from 'react';
import { View, Text, ScrollView, Alert, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import MapView, { Marker, ProviderPropType } from 'react-native-maps';

import CustomButton from '../custom/CustomButton';

import { colors } from '../../../res/colors';
import { strings } from '../../../res/strings';

import { changeLatitudeEstabelecimento, changeLongitudeEstabelecimento, changeRegiaoEstabelecimento, changeRegiaoEstabelecimento2 } from '../../actions/EstabelecimentoActions';

let id = 0;

class CriarEstabelecimentoPasso3 extends Component {

    constructor(props) {
        super(props);

        this.state = {
            markers: [],
        };
    }

    componentWillMount() {
        if (this.props.latitude && this.props.longitude) {
            let coordinate = {
                latitude: this.props.latitude,
                longitude: this.props.longitude
            }
            this.props.changeRegiaoEstabelecimento(coordinate)
        }
    }

    onMapPress(e) {
        this.setState({
            markers: [
                {
                    coordinate: e.nativeEvent.coordinate,
                    key: id++,
                    description: this.props.nome,
                    title: this.props.nome,
                },
            ],
        });
        this.props.changeLatitudeEstabelecimento(e.nativeEvent.coordinate.latitude);
        this.props.changeLongitudeEstabelecimento(e.nativeEvent.coordinate.longitude);
    }


    checkData() {
        let message = '';
        if (!this.props.latitude || !this.props.longitude) {
            message += "Informe a localização do seu estabelecimento tocando no mapa";
        }

        return message;
    }

    attemptCreateEstabelecimento() {
        let message = this.checkData();

        if (message) {
            Alert.alert(strings.title_error, `${strings.error_requireds_fiels} ${message}`);
        } else {
            console.log(this.props.nome)
            Actions.criarEstabelecimentoPasso4({
                nome: this.props.nome,
                email: this.props.email,
                phone: this.props.phone,
                whatsapp: this.props.whatsapp,
                cnpj: this.props.cnpj,
                endereco: this.props.endereco,
                bairro: this.props.bairro,
                cidade: this.props.cidade,
                uf: this.props.uf,
                cep: this.props.cep,
                description: this.props.description,
                category: this.props.category,
                keywords: this.props.keywords,
                user_id: this.props.user_id,
                latitude: this.props.latitude,
                longitude: this.props.longitude
            });
        }
    }

    renderMarker() {
        if (this.props.latitude && this.props.longitude) {
            let coordinate = {
                latitude: this.props.latitude,
                longitude: this.props.longitude
            }

            return (
                <Marker
                    key={this.props.nome}
                    coordinate={coordinate}
                    description={this.props.keywords}
                    title={this.props.nome}
                />
            )
        }
    }

    renderMap() {
        console.log(this.props.defaultRegion)

        return (
            <MapView
                style={{ ...StyleSheet.absoluteFillObject }}
                region={this.props.defaultRegion}
                onPress={(e) => this.onMapPress(e)}
                onRegionChangeComplete={(region) => this.props.changeRegiaoEstabelecimento2(region)}
            >

                {this.state.markers.map(marker => (
                    <Marker
                        key={marker.key}
                        coordinate={marker.coordinate}
                        description={this.props.keywords}
                        title={this.props.nome}
                    />
                ))}
                {this.renderMarker()}
            </MapView>
        )
    }

    render() {

        return (
            <View style={{ flex: 1, backgroundColor: colors.bg }}>
                <ScrollView style={{ flex: 1 }}>
                    <View style={{ alignItems: 'center', flex: 1, paddingBottom: 16, paddingTop: 32 }}>
                        <Text style={{ fontSize: 20, textAlign: 'center', color: colors.primary_color, paddingHorizontal: 16 }}>Agora informe a localização do seu estabelecimento</Text>
                        <Text style={{ color: colors.border_color, textAlign: 'center', marginTop: 30, paddingHorizontal: 16 }}>Use o mapa abaixo para marcar a localização do seu estabelecimento. Assim seus clientes poderão te encontrar facilmente.</Text>
                        <View style={{ backgroundColor: colors.border_color, width: '100%', height: 300, marginVertical: 30 }} >
                            {this.renderMap()}
                        </View>

                        <CustomButton
                            title="Continuar"
                            width={260}
                            onPress={() => this.attemptCreateEstabelecimento()} />

                    </View>
                </ScrollView>
            </View>
        )
    }
}

CriarEstabelecimentoPasso3.propTypes = {
    provider: ProviderPropType,
};

mapStateToProps = state => (
    {
        latitude: state.EstabelecimentoReducer.latitude,
        longitude: state.EstabelecimentoReducer.longitude,
        estabelecimento_id: state.EstabelecimentoReducer.id,
        defaultRegion: state.EstabelecimentoReducer.defaultRegion
    }
)

export default connect(mapStateToProps, { changeLatitudeEstabelecimento, changeLongitudeEstabelecimento, changeRegiaoEstabelecimento, changeRegiaoEstabelecimento2 })(CriarEstabelecimentoPasso3);