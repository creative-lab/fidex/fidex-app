import React, { Component } from 'react';
import { View, ListView } from 'react-native';
import { connect } from 'react-redux';

import ItemEstabelecimento from './ItemEstabelecimento';
import { listarEstabelecimentos } from '../../actions/EstabelecimentoActions'
class HomeEmpresas extends Component {

    componentDidMount() {
        this.props.listarEstabelecimentos(this.props.userId)
    }

    renderListEstabelecimentos() {
        if (this.props.estabelecimentos) {
            console.log(this.props.estabelecimentos)

            const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })

            let fonteDeDadosEstabelecimentos = ds.cloneWithRows(this.props.estabelecimentos)

            return (
                <ListView
                    enableEmptySections
                    dataSource={fonteDeDadosEstabelecimentos}
                    renderRow={estabelecimento => <ItemEstabelecimento estabelecimento={estabelecimento} />} />
            )
        }
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                {this.renderListEstabelecimentos()}
            </View>
        )
    }
}

mapStateToProps = state => (
    {
        userId: state.AuthReducer.id,
        estabelecimentos: state.EstabelecimentoReducer.estabelecimentos
    }
)

export default connect(mapStateToProps, { listarEstabelecimentos })(HomeEmpresas);