import React from 'react';
import { View, Text, Image, TouchableHighlight } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons'

import { colors } from '../../../res/colors';
import { Actions } from 'react-native-router-flux';

const ItemEstabelecimento = props => (

    <TouchableHighlight
        onPress={() => Actions.estabelecimentoDetalhe({ estabelecimento: props.estabelecimento })}
        underlayColor={colors.bg_color_button_pressed}
        activeOpacity={0.7}>

        <View
            style={{
                backgroundColor: colors.primary_color,
                borderRadius: 6,
                padding: 16,
                margin: 8
            }}>

            <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>{props.estabelecimento.nome}</Text>
            <View style={{ width: '100%', height: 1, backgroundColor: 'white', marginVertical: 10 }}></View>

            <View
                style={{ flexDirection: 'row' }}>
                <View style={{ flex: 1 }}>

                    <View
                        style={{ flexDirection: 'row', }}>
                        <Text style={{ fontWeight: 'bold', color: 'white', marginRight: 4 }}>CNPJ:</Text>
                        <Text style={{ color: 'white' }}>{props.estabelecimento.cnpj ? props.estabelecimento.cnpj : "Não informado"}</Text>
                    </View>

                    <View
                        style={{ flexDirection: 'row', }}>
                        <Text style={{ fontWeight: 'bold', color: 'white', marginRight: 4 }}>Clientes ativos:</Text>
                        <Text style={{ color: 'white' }}>{props.estabelecimento.clientesAtivos ? props.estabelecimento.clientesAtivos : 0}</Text>
                    </View>

                    <View
                        style={{ flexDirection: 'row', }}>
                        <Text style={{ fontWeight: 'bold', color: 'white', marginRight: 4 }}>Seguidores:</Text>
                        <Text style={{ color: 'white' }}>{props.estabelecimento.seguidores ? props.estabelecimento.seguidores : 0}</Text>
                    </View>

                    <View
                        style={{ flexDirection: 'row', }}>
                        <Text style={{ fontWeight: 'bold', color: 'white', marginRight: 4 }}>Promoções:</Text>
                        <Text style={{ color: 'white' }}>{props.estabelecimento.promocoes ? props.estabelecimento.promocoes : 0}</Text>
                    </View>

                </View>
                <Icon name='keyboard-arrow-right' size={32} color='white' />

            </View>

        </View>
    </TouchableHighlight>
)
export default ItemEstabelecimento;