import React, { Component } from 'react';
import { View, Text, Image, AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';

import CustomButton from '../custom/CustomButton';
import { colors } from '../../../res/colors';
const home_empresas = require('../../../res/imgs/home_empresas.png')

import { logout, toogleUserLogged } from '../../actions/AuthActions'

class HomeNoneEstabelecimento extends Component {

    resetToken() {
        try {
            AsyncStorage.setItem('id', '')
        } catch (error) {
            console.log(`Erro ao resetar o token localmente ${error}`)
        }
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.bg, alignItems: 'center' }}>
                <Image source={home_empresas} />
                <Text style={{ padding: 16, color: colors.border_color, textAlign: 'center' }}>Você ainda não tem nenhum estabelecimento cadastrado</Text>

                <CustomButton
                    title="Criar estabelecimento"
                    width={260}
                    onPress={() => Actions.assinar()} />
            </View>
        )
    }
}

export default connect(null, { logout, toogleUserLogged })(HomeNoneEstabelecimento);