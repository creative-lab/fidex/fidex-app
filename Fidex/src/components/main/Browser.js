import React, { Component } from 'react';
import { View, ListView, StyleSheet, TextInput, Alert, TouchableHighlight, Text } from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons'

import { colors } from '../../../res/colors';
import { strings } from '../../../res/strings';
import ItemSelecaoBuscaLocais from './usuario/ItemSelecaoBuscaLocais';
import ItemEstabelecimentosNavegar from './usuario/ItemEstabelecimentosNavegar';

import { changePesquisaEstabelecimento, buscarEstabelecimentos } from '../../actions/EstabelecimentoActions';
import { changeLatitude, changeLongitude, changeErrorGeoposition } from '../../actions/AuthActions';

class Browser extends Component {

    search() {

        if (this.props.tipoBuscaPorLocais === 'Lugares próximos') {
            if (this.props.latitudeUsuario == null || this.props.longitudeUsuario == null) {
                Alert.alert(strings.title_error, "Ainda estamos determinando sua localização, aguarde mais um pouco");
            } else {
                this.props.buscarEstabelecimentos(this.props.latitudeUsuario, this.props.longitudeUsuario, this.props.pesquisaEstabelecimento);
            }
        } else {
            this.props.buscarEstabelecimentos(null, null, this.props.pesquisaEstabelecimento);
        }

    }

    componentDidMount() {
        if (this.props.latitudeUsuario == null || this.props.longitudeUsuario == null) {
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    this.props.changeLatitude(position.coords.latitude)
                    this.props.changeLongitude(position.coords.longitude)
                    this.props.changeErrorGeoposition(null)
                },
                (error) => {
                    this.props.changeErrorGeoposition(error.message)
                    if (error.code === 2) {
                        Alert.alert(strings.title_error, "Ligue o GPS do seu dispositivo e tente novamente.");
                    }
                },
                { enableHighAccuracy: true, timeout: 20000, maximumAge: 10000 },
            );
        }
        this.search()
    }

    renderSearch() {
        if (this.props.statusBuscaPorLocais !== 'normal') {
            const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
            this.state = {
                fonteDeTipos: ds.cloneWithRows(this.props.tiposBuscaPorLocais)
            }
            return (
                <ListView
                    enableEmptySections

                    dataSource={this.state.fonteDeTipos}
                    renderSeparator={(rowId) =>
                        <View key={rowId} style={{
                            flex: 1,
                            marginHorizontal: 16,
                            height: StyleSheet.hairlineWidth,
                            backgroundColor: colors.border_color,
                        }}
                        />}
                    renderRow={tipo => <ItemSelecaoBuscaLocais tipo={tipo} />} />
            )
        } else {

            return (
                <View style={{ flex: 1 }}>

                    <View
                        style={{ padding: 16 }}>

                        <View style={{
                            borderRadius: 6,
                            flexDirection: 'row',
                            height: 40,
                            width: '100%',
                            borderColor: colors.border_color,
                            borderWidth: 1,
                            alignItems: 'center'
                        }}>

                            <TextInput
                                placeholder='Pesquisar por nome'
                                underlineColorAndroid="#FFFFFF"
                                value={this.props.pesquisaEstabelecimento}
                                onChangeText={text => this.props.changePesquisaEstabelecimento(text)}
                                style={{ flex: 1, marginHorizontal: 10, fontStyle: 'italic' }}
                                returnKeyType='search'
                                onSubmitEditing={(event) => { this.search() }}
                            />

                            <TouchableHighlight
                                underlayColor={colors.bg_color_button_pressed}
                                activeOpacity={0.7}
                                onPress={() => this.search()} >

                                <Icon style={{ paddingHorizontal: 8 }} name='search' size={20} />
                            </TouchableHighlight>

                        </View>
                    </View>

                    {this.renderListEstabelecimentos()}

                </View>
            )
        }
    }

    renderListEstabelecimentos() {

        if (this.props.estabelecimentos !== null) {
            if (this.props.estabelecimentos.length > 0) {
                const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })

                let fonteDeDadosEstabelecimentos = ds.cloneWithRows(this.props.estabelecimentos)

                return (
                    <ListView
                        enableEmptySections
                        dataSource={fonteDeDadosEstabelecimentos}
                        renderSeparator={(rowId) =>
                            <View key={rowId} style={{
                                flex: 1,
                                marginHorizontal: 16,
                                height: StyleSheet.hairlineWidth,
                                backgroundColor: colors.border_color,
                            }}
                            />}
                        renderRow={estabelecimento => <ItemEstabelecimentosNavegar estabelecimento={estabelecimento} />} />
                )
            } else {
                return (
                    <View style={{ justifyContent: 'center', alignItems: 'center', flex:1 }}>
                        <Text style={{padding: 20, textAlign: 'center'}}>{'Nenhum lugar encontrado com os parâmetros informados'}</Text>
                    </View>
                )
            }
        }
    }

    render() {
        console.log(this.props.user_id)
        return (
            this.renderSearch()
        )
    }
}

mapStateToProps = state => (
    {
        statusBuscaPorLocais: state.EstabelecimentoReducer.statusBuscaPorLocais,
        tiposBuscaPorLocais: state.EstabelecimentoReducer.tiposBuscaPorLocais,
        tipoBuscaPorLocais: state.EstabelecimentoReducer.tipoBuscaPorLocais,
        pesquisaEstabelecimento: state.EstabelecimentoReducer.pesquisaEstabelecimento,
        user_id: state.AuthReducer.id,
        estabelecimentos: state.EstabelecimentoReducer.estabelecimentos,
        latitudeUsuario: state.AuthReducer.latitudeUsuario,
        longitudeUsuario: state.AuthReducer.longitudeUsuario,
        errorGeoposition: state.AuthReducer.errorGeoposition,
    }
)

export default connect(mapStateToProps, { changePesquisaEstabelecimento, buscarEstabelecimentos, changeLatitude, changeLongitude, changeErrorGeoposition })(Browser);