import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { connect } from 'react-redux';
import GridView from 'react-native-super-grid';

import { colors } from '../../../res/colors';

import SecondCustomButton from '../custom/SecondCustomButton';
import ItemLugares from './ItemLugares';

import { recuperarDashboard } from '../../actions/UsuarioActions'
class Home extends Component {

    componentDidMount() {
        this.props.recuperarDashboard(this.props.userId)
    }

    renderAvatar() {

        if (this.props.dashboard != null) {
            return (
                <View style={{ flexDirection: 'row', marginTop: 16 }}>
                    <Image style={{ height: 90, width: 90, borderRadius: 45 }} source={{ uri: this.props.dashboard.user.perfil }} />
                    <View style={{ flex: 1, justifyContent: "center", paddingLeft: 16 }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{this.props.dashboard.user.nome}</Text>
                        <Text>{this.props.dashboard.user.cidade}/{this.props.dashboard.user.uf}</Text>
                    </View>
                </View>
            )
        } else {
            return (
                <View style={{ flexDirection: 'row', marginTop: 16 }}>
                    <Image style={{ height: 90, width: 90, borderRadius: 45 }} source={{ uri: 'https://d2gg9evh47fn9z.cloudfront.net/800px_COLOURBOX28408818.jpg' }} />
                    <View style={{ flex: 1, justifyContent: "center", paddingLeft: 16 }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold' }}></Text>
                        <Text>/</Text>
                    </View>
                </View>
            )
        }
    }

    renderPoints() {
        if (this.props.dashboard != null) {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <SecondCustomButton
                        title="Lugares visitados"
                        number={this.props.dashboard.lugaresVisitados}
                        onPress={() => false} />

                    <SecondCustomButton
                        title="Resgate de promoções"
                        number={this.props.dashboard.resgateDePromocoes}
                        onPress={() => false} />
                </View>
            )
        } else {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <SecondCustomButton
                        title="Lugares visitados"
                        number={" "}
                        onPress={() => false} />

                    <SecondCustomButton
                        title="Resgate de promoções"
                        number={" "}
                        onPress={() => false} />
                </View>
            )
        }
    }

    renderEstabelecimentos() {
        if (this.props.dashboard != null) {
            if (this.props.dashboard.ultimosLugares && this.props.dashboard.ultimosLugares.length > 0) {

                // console.log(this.props.dashboard.ultimosLugares)
                return (
                    <GridView
                        itemDimension={90}
                        style={{ flex: 1 }}
                        items={this.props.dashboard.ultimosLugares}
                        renderItem={item => (<ItemLugares lugar={item} user={this.props.userId} />)} />
                )
            } else {
                return (
                    <View>

                        <Text style={{
                            width: '100%', padding: 20, textAlign: 'center',
                        }}>
                            Você não visitou nenhum lugar ainda. Pontue com seu Fidex nos estabelecimentos que você frequenta</Text>
                    </View>
                )
            }

        }
    }

    render() {
        return (
            <View style={{ flex: 1, paddingHorizontal: 16 }}>
                {this.renderAvatar()}
                <View style={{ width: '100%', height: 1, backgroundColor: colors.border_color, marginVertical: 16 }} />
                {this.renderPoints()}
                <View style={{ width: '100%', height: 1, backgroundColor: colors.border_color, marginVertical: 16 }} />

                <Text>Últimos lugares</Text>
                {this.renderEstabelecimentos()}
            </View>
        )
    }
}

mapStateToProps = state => (
    {
        userId: state.AuthReducer.id,
        dashboard: state.UsuarioReducer.dashboard
    }
)

export default connect(mapStateToProps, { recuperarDashboard })(Home);