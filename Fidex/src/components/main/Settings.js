import React, { Component } from 'react';
import { View, Text, ScrollView, AsyncStorage, TouchableHighlight, Alert } from 'react-native';
import { connect } from 'react-redux';

import { colors } from '../../../res/colors';
import { strings } from '../../../res/strings';

import { logout, toogleUserLogged } from '../../actions/AuthActions'
import { Actions } from 'react-native-router-flux';
import { version } from '../../../package.json';

// const package = require('../../../package.json');

class Settings extends Component {

    _logout() {
        this.props.toogleUserLogged('')
        this.resetToken()
        Actions.start()
    }

    resetToken() {
        try {
            AsyncStorage.setItem('id', '')
            // toogleUserLogged('')
        } catch (error) {
            console.log(`Erro ao resetar o token localmente ${error}`)
        }
    }

    confirmLogout() {
        Alert.alert(
            strings.default_title_alert,
            strings.msg_logout,
            [
                { text: strings.action_yes, onPress: () => this._logout(), style: 'destructive' },
                { text: strings.action_no, onPress: () => false, style: 'cancel' }
            ],
            { cancelable: true }
        )
    }

    renderProfile() {
        if (this.props.user_id.includes("usuario")) {
            return (
                <TouchableHighlight
                    style={{ padding: 16 }}
                    onPress={() => Actions.editProfileAdm()} underlayColor={colors.bg_color_button_pressed}
                    activeOpacity={0.7}>
                    <Text style={{ color: colors.primary_color }}>{strings.edit_profile_user}</Text>
                </TouchableHighlight>
            )
        } else {
            return (
                <TouchableHighlight
                    style={{ padding: 16 }}
                    onPress={() => Actions.editProfileAdm()} underlayColor={colors.bg_color_button_pressed}
                    activeOpacity={0.7}>
                    <Text style={{ color: colors.primary_color }}>{strings.edit_profile}</Text>
                </TouchableHighlight>
            )
        }
    }

    editarPerfil() {
        Alert.alert(strings.default_title_alert, strings.in_development);
    }

    render() {
        return (
            <ScrollView style={{ padding: 16 }}>
                <View >
                    {this.renderProfile()}

                    <View style={{ width: '100%', height: 1, backgroundColor: colors.border_color }} />

                    <TouchableHighlight
                        style={{ padding: 16 }}
                        onPress={() => Actions.updatePass()}
                        underlayColor={colors.bg_color_button_pressed}
                        activeOpacity={0.7}>
                        <Text style={{ color: colors.primary_color }}>{strings.update_pass}</Text >
                    </TouchableHighlight>

                    <View style={{ width: '100%', height: 1, backgroundColor: colors.border_color }} />
                    <TouchableHighlight
                        style={{ padding: 16 }}
                        onPress={() => false}
                        underlayColor={colors.bg_color_button_pressed}
                        activeOpacity={0.7}>
                        <Text style={{ color: colors.border_color }}>{`Versão ${version}`}</Text >
                    </TouchableHighlight>

                    <View style={{ width: '100%', height: 1, backgroundColor: colors.border_color }} />

                    <TouchableHighlight
                        style={{ padding: 16 }}
                        onPress={() => Actions.termos()}
                        underlayColor={colors.bg_color_button_pressed}
                        activeOpacity={0.7}>
                        <Text style={{ color: colors.primary_color }}>{strings.terms}</Text >
                    </TouchableHighlight>

                    <View style={{ width: '100%', height: 1, backgroundColor: colors.border_color }} />

                    <TouchableHighlight
                        style={{ padding: 16 }}
                        onPress={() => this.confirmLogout()} underlayColor={colors.bg_color_button_pressed}
                        activeOpacity={0.7}>
                        <Text style={{ color: colors.primary_color }}>Sair</Text >
                    </TouchableHighlight>

                </View>
            </ScrollView>
        )
    }
}

mapStateToProps = state => (
    {
        user_id: state.AuthReducer.id
    }
)

export default connect(mapStateToProps, { logout, toogleUserLogged })(Settings);