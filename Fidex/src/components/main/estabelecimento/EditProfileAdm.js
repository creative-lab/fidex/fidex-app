import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableHighlight, Image, Alert } from 'react-native'
import { connect } from 'react-redux';
import ImagePicker from 'react-native-customized-image-picker';
import CPF from 'gerador-validador-cpf';

import CustomTextInput from '../../custom/CustomTextInput'
import CustomButton from './../../custom/CustomButton'
import CustomDropdown from './../../custom/CustomDropdown'

import { colors } from '../../../../res/colors'
import { strings } from '../../../../res/strings'
import Validator from '../../../util/Validator';

import { recuperarDadosUsuario, editarPerfil, changeNome, changeCPF, changeEmail, changeTelefone, changeUf, changeCidade } from '../../../actions/UsuarioActions'

var selectdImage = '';
class EditProfileAdm extends Component {

    constructor() {
        super();
        this.state = {
            images: null,
            ufs: [
                {
                    value: 'UF',
                }, {
                    value: 'AC',
                }, {
                    value: 'AL',
                }, {
                    value: 'AM',
                }, {
                    value: 'AP',
                }, {
                    value: 'BA',
                }, {
                    value: 'CE',
                }, {
                    value: 'DF',
                }, {
                    value: 'ES',
                }, {
                    value: 'GO',
                }, {
                    value: 'MA',
                }, {
                    value: 'MT',
                }, {
                    value: 'MS',
                }, {
                    value: 'MG',
                }, {
                    value: 'PA',
                }, {
                    value: 'PB',
                }, {
                    value: 'PR',
                }, {
                    value: 'PE',
                }, {
                    value: 'PI',
                }, {
                    value: 'RJ',
                }, {
                    value: 'RN',
                }, {
                    value: 'RS',
                }, {
                    value: 'RO',
                }, {
                    value: 'RR',
                }, {
                    value: 'SC',
                }, {
                    value: 'SP',
                }, {
                    value: 'SE',
                }, {
                    value: 'TO',
                }
            ]
        };
    }

    componentDidMount() {
        this.props.recuperarDadosUsuario(this.props.userId)
    }

    pickSingleBase64(cropit) {
        ImagePicker.openPicker({
            width: 300,
            height: 300,
            cropping: cropit,
            includeBase64: true,
            isCamera: true
        }).then(images => {
            this.setState({
                ...this.state,
                images: images.map(i => {
                    console.log(`received image ${i.mine}`, );
                    return { uri: i.path, width: i.width, height: i.height, mime: i.mime, data: i.data };
                })
            });
        }).catch(e => { alert(e); console.log(e) });
    }

    renderLocalImage(image) {

        selectdImage = image.data
        console.log(selectdImage)

        return (
            <Image
                style={{
                    borderRadius: 50,
                    resizeMode: 'contain',
                    height: 99,
                    width: 99,
                    borderColor: colors.border_color,
                    borderWidth: 0,
                }}
                source={image} />
        )

    }

    renderRemoteImage() {
        return (
            <Image
                style={{
                    height: 99,
                    width: 99,
                    borderRadius: 50
                }}
                source={{ uri: this.props.avatar }} />
        )
    }

    renderImageLogalUri() {

        return this.state.images ? this.state.images.map(i =>
            <View style={{
                borderColor: colors.bg,
                borderWidth: 0,
            }} key={i.uri}>

                {
                    this.renderLocalImage(i)
                }

            </View>) : this.renderRemoteImage();
    }

    checkData() {
        let message = '';
        if (!this.props.nome) {
            message += "Nome completo";
        }
        if (!this.props.cpf) {
            if (message) {
                message += ', ';
            }
            message += 'CPF';
        }
        if (!this.props.email) {
            if (message) {
                message += ', ';
            }
            message += 'Email';
        }

        if (!this.props.telefone) {
            if (message) {
                message += ', ';
            }
            message += 'Telefone';
        }

        if (!this.props.cidade) {
            if (message) {
                message += ', ';
            }
            message += 'Cidade';
        }

        if (!this.props.uf || this.props.uf === 'UF') {
            if (message) {
                message += ', ';
            }
            message += 'UF';
        }
        return message;
    }

    attemptSaveProfile() {
        let message = this.checkData();
        if (message) {
            Alert.alert(strings.title_error, `${strings.error_requireds_fiels} ${message}`);
        } else if (Validator.isEmailValid(this.props.email)) {
            if (CPF.validate(this.props.cpf)) {
                this.props.editarPerfil(
                    this.props.nome,
                    this.props.email,
                    this.props.cpf,
                    this.props.telefone,
                    this.props.cidade,
                    this.props.uf,
                    selectdImage == '' ? null : selectdImage,
                    this.props.userId
                )
            } else {
                Alert.alert(strings.title_error, strings.invalid_cpf);
            }
        } else {
            Alert.alert(strings.title_error, strings.invalid_email);
        }
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.bg }}>
                <ScrollView style={{ flex: 1 }}>
                    <View style={{ alignItems: 'center', flex: 1, paddingBottom: 16, paddingTop: 32 }}>

                        <View style={{
                            borderRadius: 50,
                            height: 100,
                            width: 100,
                            borderColor: colors.border_color,
                            borderWidth: 1,

                        }}>
                            {this.renderImageLogalUri()}
                        </View>

                        <TouchableHighlight onPress={() => this.pickSingleBase64(true)} underlayColor={colors.bg_color_button_pressed}
                            style={{ margin: 16 }}
                            activeOpacity={0.7}>
                            <Text style={{ color: colors.primary_color }}>{this.props.usuario === null ? null : this.props.usuario.avatar === null ? "Adicionar avatar" : "Editar avatar"}</Text >
                        </TouchableHighlight>

                        <View style={{ height: 15 }} />

                        <CustomTextInput
                            placeholder="Nome Completo"
                            width={260}
                            returnKeyType={"next"}
                            value={this.props.nome}
                            onChangeText={text => this.props.changeNome(text)}
                            onSubmitEditing={() => this.customInputCPF.refs.inputCPF.focus()}
                            blurOnSubmit={false} />

                        <View style={{ height: 15 }} />

                        <CustomTextInput
                            placeholder='Seu CPF'
                            width={260}
                            maxLength={14}
                            returnKeyType={"done"}
                            value={this.props.cpf}
                            onChangeText={text => this.props.changeCPF(text)}
                            blurOnSubmit={false}
                            onSubmitEditing={() => this.customInputEmail.refs.inputEmail.focus()}
                            keyboardType='numeric'
                            refInner="inputCPF"
                            ref={ref => this.customInputCPF = ref}
                        />
                        <View style={{ height: 15 }} />

                        <CustomTextInput
                            placeholder="Seu e-mail"
                            width={260}
                            keyboardType='email-address'
                            returnKeyType={"next"}
                            value={this.props.email}
                            onChangeText={text => this.props.changeEmail(text)}
                            refInner="inputEmail"
                            ref={ref => this.customInputEmail = ref}
                            onSubmitEditing={() => this.customInputPhone.refs.inputPhone.focus()}
                            blurOnSubmit={false} />

                        <View style={{ height: 15 }} />

                        <CustomTextInput
                            placeholder='Telefone de contato'
                            width={260}
                            returnKeyType={"done"}
                            value={this.props.telefone}
                            onSubmitEditing={() => this.customInputCidade.refs.inputCidade.focus()}
                            blurOnSubmit={false}
                            refInner="inputPhone"
                            ref={ref => this.customInputPhone = ref}
                        />

                        <View style={{ height: 15 }} />

                        <CustomTextInput
                            placeholder="Cidade"
                            width={260}
                            returnKeyType={"next"}
                            value={this.props.cidade}
                            onChangeText={text => this.props.changeCidade(text)}
                            onSubmitEditing={() => this.customInputCPF.refs.inputCPF.focus()}
                            blurOnSubmit={false}
                            ref={ref => this.customInputCidade = ref}
                            refInner="inputCidade"
                            onSubmitEditing={() => this.attemptSaveProfile()}
                        />

                        <View style={{ height: 15 }} />

                        <CustomDropdown
                            placeholder={strings.placeholder_descricao_estabelecimento}
                            width={260}
                            onChangeText={text => this.props.changeUf(text)}
                            data={this.state.ufs}
                            value={this.props.uf} />

                        <View style={{ height: 20 }} />


                        <CustomButton
                            title="Concluir"
                            width={260}
                            onPress={() => this.attemptSaveProfile()} />

                    </View>
                </ScrollView>
            </View>
        )
    }



}

mapStateToProps = state => (
    {
        userId: state.AuthReducer.id,
        usuario: state.UsuarioReducer.usuario,
        nome: state.UsuarioReducer.nome,
        cpf: state.UsuarioReducer.cpf,
        email: state.UsuarioReducer.email,
        telefone: state.UsuarioReducer.telefone,
        avatar: state.UsuarioReducer.avatar,
        uf: state.UsuarioReducer.uf,
        cidade: state.UsuarioReducer.cidade,
    }
)

export default connect(mapStateToProps, { recuperarDadosUsuario, changeNome, changeCPF, changeEmail, changeTelefone, editarPerfil, changeCidade, changeUf })(EditProfileAdm);