import React, { Component } from 'react'
import { View, Alert, ScrollView, Text } from 'react-native';
import CPF from 'gerador-validador-cpf';
import CustomTextInput from '../../custom/CustomTextInput';
import CustomButton from '../../custom/CustomButton';
import { connect } from 'react-redux';

import { strings } from '../../../../res/strings';
import { changeResgatarPromocaoCpf, changeRestatarPromocaoVoucher, resgatarPromocao } from '../../../actions/EstabelecimentoActions'

class ResgatarPromocaoForm extends Component {

    constructor() {
        super()
        this.state = {
            cpfInvalid: false,
        };
    }

    renderCPFError() {
        if (this.state.cpfInvalid) {
            return (
                <Text style={{ color: 'red' }}>CPF digitado é inválido</Text>
            )
        }
    }

    renderVoucherError() {
        if (this.props.resgatarPromocaoError) {
            return (
                <Text style={{ color: 'red' }}>{this.props.resgatarPromocaoError}</Text>
            )
        }
    }

    checkData() {
        let message = '';
        if (!this.props.resgatarPromocaoCpf) {
            message += strings.field_cpf;
        }
        if (!this.props.resgatarPromocaoCodigoVoucher) {
            if (message) {
                message += ', ';
            }
            message += "Voucher";
        }
        return message;
    }

    attemptLogin() {
        let message = this.checkData();
        if (message) {
            Alert.alert(strings.title_error, `${strings.error_requireds_fiels} ${message}`);
        } else if (!CPF.validate(this.props.resgatarPromocaoCpf)) {
            this.setState({ ...this.state, cpfInvalid: true })
        } else if (this.props.resgatarPromocaoCodigoVoucher.length < 4) {
            this.setState({ ...this.state, cpfInvalid: false, voucherInvalid: true })
        } else {
            this.setState({ ...this.state, cpfInvalid: false, voucherInvalid: false })
            this.props.resgatarPromocao(this.props.resgatarPromocaoCpf, this.props.resgatarPromocaoCodigoVoucher, this.props.estabelecimentos[0].id)
        }
    }

    render() {
        return (
            <ScrollView style={{ flex: 1 }}>
                <View style={{ alignItems: 'center', flex: 1 }}>

                    <View style={{ height: 20 }} />

                    <CustomTextInput
                        placeholder="Digite o CPF do cliente"
                        width={260}
                        maxLength={14}
                        value={this.props.resgatarPromocaoCpf}
                        returnKeyType={"done"}
                        blurOnSubmit={false}
                        onChangeText={text => this.props.changeResgatarPromocaoCpf(text)}
                        onSubmitEditing={() => this.customInput.refs.inputValue.focus()}
                        keyboardType='numeric'
                    />
                    <View style={{ height: 15 }} />

                    <CustomTextInput
                        placeholder="Código do voucher"
                        width={260}
                        value={this.props.resgatarPromocaoCodigoVoucher}
                        returnKeyType={"done"}
                        blurOnSubmit={false}
                        refInner="inputValue"
                        onChangeText={text => this.props.changeRestatarPromocaoVoucher(text)}
                        onSubmitEditing={() => this.attemptLogin()}
                        ref={ref => this.customInput = ref}
                    />
                    <View style={{ height: 20 }} />

                    {this.renderCPFError()}
                    {this.renderVoucherError()}

                    <View style={{ height: 20 }} />

                    <CustomButton
                        title="Confirmar resgate"
                        width={260}
                        onPress={() => this.attemptLogin()} />

                </View>

            </ScrollView>
        )
    }
}

mapStateToProps = state => (
    {
        estabelecimentos: state.EstabelecimentoReducer.estabelecimentos,
        resgatarPromocaoCpf: state.EstabelecimentoReducer.resgatarPromocaoCpf,
        resgatarPromocaoCodigoVoucher: state.EstabelecimentoReducer.resgatarPromocaoCodigoVoucher,
        resgatarPromocaoError: state.EstabelecimentoReducer.resgatarPromocaoError
    }
)

export default connect(mapStateToProps, { changeResgatarPromocaoCpf, changeRestatarPromocaoVoucher, resgatarPromocao })(ResgatarPromocaoForm);