import React, { Component } from 'react';
import { View, ListView, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

import { colors } from '../../../../res/colors';
import ItemPromocao from './ItemPromocao';
import { listarPromocoes } from '../../../actions/PromocaoActions'

class ListaPromocoes extends Component {

    renderListPlans() {
        if (this.props.promocoes && this.props.promocoes.length > 0) {

            const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
            this.state = {
                fonteDeDadosPromocoes: ds.cloneWithRows(this.props.promocoes)
            }
            return (
                <ListView
                    enableEmptySections
                    dataSource={this.state.fonteDeDadosPromocoes}
                    renderSeparator={(rowId) =>
                        <View key={rowId} style={{
                            flex: 1,
                            marginHorizontal: 16,
                            height: StyleSheet.hairlineWidth,
                            backgroundColor: colors.border_color,
                        }}
                        />}
                    renderRow={promocao => <ItemPromocao promocao={promocao} />} />
            )
        } else {
            return (
                <Text style={{
                    width: '100%',
                    height: '100%',
                    paddingHorizontal: 16,
                    paddingVertical: 60,
                    textAlign: 'center',
                }}>
                    Nenhuma promoção encontrada. Cadastre uma promoção no seu Fidex</Text>
            )
        }
    }

    componentDidMount() {
        if (this.props.estabelecimentos) {
            // alert(this.props.estabelecimentos[0].id)
            this.props.listarPromocoes(this.props.estabelecimentos[0].id)
        }
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                {this.renderListPlans()}
            </View>
        )
    }

}
mapStateToProps = state => (
    {
        promocoes: state.PromocaoReducer.promocoes,
        estabelecimentos: state.EstabelecimentoReducer.estabelecimentos
    }
)

export default connect(mapStateToProps, { listarPromocoes })(ListaPromocoes);