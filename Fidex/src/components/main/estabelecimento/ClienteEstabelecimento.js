import React, { Component } from 'react';
import { View, Image, Text, ScrollView } from 'react-native';
import { connect } from 'react-redux';

import { colors } from '../../../../res/colors';

import { recuperarDadosCliente } from '../../../actions/EstabelecimentoActions'

class ClienteEstabelecimento extends Component {

    componentDidMount() {
        this.props.recuperarDadosCliente(this.props.id, this.props.estabelecimentos[0].id)
    }

    returnURL(possible) {
        if (possible != undefined && !possible.includes('http://fidexapp.com.br/')) {
            return `http://fidexapp.com.br/img/avatar/${possible}`
        } else {
            return possible
        }
    }

    renderCliente() {
        if (this.props.clienteAtual === null) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: "center" }}>
                    <Text>Recuperando dados do cliente, aguarde.</Text>
                </View>
            )
        } else {
            return (
                <ScrollView style={{ backgroundColor: colors.bg }}>
                    <View style={{ padding: 16 }}>
                        <View style={{ flexDirection: 'row', marginTop: 16 }}>
                            <Image style={{ height: 90, width: 90, borderRadius: 45 }} source={{ uri: this.returnURL(this.props.clienteAtual.avatar) }} />
                            <View style={{ flex: 1, justifyContent: "center", paddingLeft: 16 }}>
                                <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{this.props.clienteAtual.usuario.nome}</Text>
                                <Text>{`${this.props.clienteAtual.usuario.cidade}/${this.props.clienteAtual.usuario.uf}`}</Text>
                                <Text>{this.props.clienteAtual.cpf}</Text>
                            </View>
                        </View>

                        <View style={{ width: '100%', height: 1, backgroundColor: colors.border_color, marginVertical: 16 }} />

                        <View style={{ flexDirection: 'row', marginVertical: 4 }}>
                            <Text style={{ fontWeight: 'bold', marginHorizontal: 8 }}>E-mail:</Text>
                            <Text>{this.props.clienteAtual.usuario.email}</Text>
                        </View>

                        <View style={{ flexDirection: 'row', marginVertical: 4 }}>
                            <Text style={{ fontWeight: 'bold', marginHorizontal: 8 }}>Telefone:</Text>
                            <Text>{this.props.clienteAtual.usuario.telefone}</Text>
                        </View>

                        <View style={{ flexDirection: 'row', marginVertical: 4 }}>
                            <Text style={{ fontWeight: 'bold', marginHorizontal: 8 }}>Pontos no estabelecimento:</Text>
                            <Text>{this.props.clienteAtual.pontos_estabelecimento[0].pontos}</Text>
                        </View>

                        <View style={{ flexDirection: 'row', marginVertical: 4 }}>
                            <Text style={{ fontWeight: 'bold', marginHorizontal: 8 }}>Gasto no estabelecimento:</Text>
                            <Text>{`R$ ${this.props.clienteAtual.gastos_estabelecimento[0].vlCompra}`}</Text>
                        </View>

                        <View style={{ flexDirection: 'row', marginVertical: 4 }}>
                            <Text style={{ fontWeight: 'bold', marginHorizontal: 8 }}>Pontos gastos:</Text>
                            <Text>{this.props.clienteAtual.pontos_gastos.pontos}</Text>
                        </View>

                        <View style={{ flexDirection: 'row', marginVertical: 4 }}>
                            <Text style={{ fontWeight: 'bold', marginHorizontal: 8 }}>Quantidade de resgates:</Text>
                            <Text>{this.props.clienteAtual.qtd_resgatadas.promocoes_resgatadas}</Text>
                        </View>

                        <View style={{ width: '100%', height: 1, backgroundColor: colors.border_color, marginVertical: 16 }} />

                    </View>
                </ScrollView>
            )
        }

    }
    render() {
        return (
            this.renderCliente()
        )
    }
}

mapStateToProps = state => (
    {
        clienteAtual: state.EstabelecimentoReducer.clienteAtual,
        estabelecimentos: state.EstabelecimentoReducer.estabelecimentos,
    }
)

export default connect(mapStateToProps, { recuperarDadosCliente })(ClienteEstabelecimento);