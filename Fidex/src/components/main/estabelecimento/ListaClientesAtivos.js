import React, { Component } from 'react';
import { View, ListView, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

import { colors } from '../../../../res/colors';
import ItemCliente from './ItemCliente';

class ListaClientesAtivos extends Component {

    renderList() {
        if (this.props.clientes === null) {
            return (
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Text>Recuperando clientes ....</Text>
                </View>
            )
        } else {
            const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
            let fonteDeDados = ds.cloneWithRows(this.props.clientes.ativos)
            return (

                <ListView
                    enableEmptySections
                    dataSource={fonteDeDados}
                    renderSeparator={(rowId) =>
                        <View key={rowId} style={{
                            flex: 1,
                            marginHorizontal: 16,
                            height: StyleSheet.hairlineWidth,
                            backgroundColor: colors.border_color,
                        }}
                        />}
                    renderRow={item => <ItemCliente cliente={item} />} />
            )
        }
    }

    render() {
        return (
            this.renderList()
        )
    }

}

mapStateToProps = state => (
    {
        clientes: state.EstabelecimentoReducer.clientes,
    }
)

export default connect(mapStateToProps, null)(ListaClientesAtivos);