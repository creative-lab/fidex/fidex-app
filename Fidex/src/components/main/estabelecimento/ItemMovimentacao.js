import React from 'react';
import { View, Text } from 'react-native';

import { colors } from '../../../../res/colors';

const ItemMovimentacao = (props) => (
    <View style={{ padding: 16 }}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={{ color: colors.primary_color, fontWeight: 'bold' }}>{props.tipo === "credito" ? "Crédito" : "Resgate"}</Text>
            <Text>{`${props.movimentacao.data} às ${props.movimentacao.hora}`}</Text>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={{ fontWeight: 'bold', color: 'black' }}>{props.movimentacao.nome}</Text>
            <Text style={{ color: 'black' }}>{props.movimentacao.cpf}</Text>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
            <Text style={{ flex: 3, color: 'black' }} numberOfLines={1} ellipsizeMode='tail' >{props.tipo === "credito" ? props.movimentacao.vl_compra : props.movimentacao.promocao}</Text>
            <Text style={{ flex: 2, fontWeight: 'bold', fontSize: 18, color: props.tipo === 'credito' ? 'green' : 'red', textAlign: 'right' }}>{`${props.movimentacao.pontos} pontos`}</Text>
        </View>
    </View>
)
export default ItemMovimentacao;