import React, { Component } from 'react'
import { View, ScrollView, Text, Alert } from 'react-native'
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import { colors } from '../../../../res/colors';
import CustomButton from '../../custom/CustomButton';
import CustomButtonAux from '../../custom/CustomButtonAux';

import CustomTextInput from '../../custom/CustomTextInput';
import CustomTextArea from '../../custom/CustomTextArea';

import { changeNome, changePontos, changeDescricao, changeId, editarPromocao, criarPromocao, excluirPromocoes } from '../../../actions/PromocaoActions';

import { strings } from '../../../../res/strings';

class EditarPromocao extends Component {

    componentDidMount() {
        if (!this.props.estabelecimentos || this.props.estabelecimentos.length === 0) {
            Alert.alert(strings.default_title_alert, "Primeiramente cadastre um estatabelecimento");
            Actions.pop();
        }
    }

    render() {
        return (
            <ScrollView style={{ flex: 1, backgroundColor: colors.bg, paddingHorizontal: 16 }}>

                <View style={{ alignItems: 'center', flex: 1, paddingBottom: 16, paddingTop: 32 }}>
                    <Text style={{ fontWeight: 'bold', color: colors.primary_color, textAlign: 'center' }}>Importante!
                                <Text style={{ textAlign: 'center', fontWeight: 'normal', color: colors.primary_color }}> Cada R$ 3,00 gastos pelo seu cliente equivalem a 1 ponto do Fidex</Text>
                    </Text>

                    <View style={{ height: 20 }} />

                    <CustomTextInput
                        placeholder="Nome da promoção (produto, serviço, etc)"
                        width={260}
                        onChangeText={text => this.props.changeNome(text)}
                        value={this.props.nome} />

                    <View style={{ height: 15 }} />

                    <CustomTextInput
                        placeholder="Pontos para recarga"
                        width={260}
                        onChangeText={text => this.props.changePontos(text)}
                        value={this.props.pontosRestate}
                        returnKeyType={"next"}
                        blurOnSubmit={false}
                        keyboardType='number-pad' />
                    <View style={{ height: 15 }} />

                    <CustomTextArea
                        placeholder="Descrição/Regulamento"
                        width={260}
                        height={200}
                        maxLength={1000}
                        onChangeText={text => this.props.changeDescricao(text)}
                        value={this.props.descricao} />
                    <View style={{ height: 15 }} />

                    {/* <Text>{this.props.id}</Text> */}

                    {this.renderButtonsActions()}


                </View>
            </ScrollView>
        )
    }

    deletar() {
        // alert("Deletar")
        this.props.excluirPromocoes(this.props.id, this.props.idEstabelecimento)
    }

    confirmDelete() {
        Alert.alert(
            strings.default_title_alert,
            strings.msg_delete_promocao,
            [
                { text: strings.action_yes, onPress: () => this.deletar() },
                { text: strings.action_no, onPress: () => false, style: 'cancel' }
            ],
            { cancelable: true }
        )
    }

    checkData() {
        let message = '';
        if (!this.props.nome) {
            message += "Nome";
        }
        if (!this.props.pontosRestate) {
            if (message) {
                message += ', ';
            }
            message += "Pontos";
        }
        if (!this.props.descricao) {
            if (message) {
                message += ', ';
            }
            message += "Descrição";
        }
        return message;
    }

    attemptSave() {
        let message = this.checkData();
        if (message) {
            Alert.alert(strings.title_error, `${strings.error_requireds_fiels} ${message}`);
            // alert(message)
        } else {
            if (this.props.id == 0) {
                console.log(
                    this.props.id,
                    this.props.nome,
                    this.props.descricao,
                    this.props.pontosRestate,
                    this.props.estabelecimentos[0].id
                )
                // alert('cadastrar')

                this.props.criarPromocao(
                    this.props.nome,
                    this.props.pontosRestate,
                    this.props.descricao,
                    this.props.estabelecimentos[0].id
                )
            } else {
                console.log(
                    this.props.id,
                    this.props.nome,
                    this.props.descricao,
                    this.props.pontosRestate,
                    this.props.idEstabelecimento
                )
                this.props.editarPromocao(
                    this.props.nome,
                    this.props.pontosRestate,
                    this.props.id,
                    this.props.descricao,
                    this.props.idEstabelecimento)

            }
        }
    }

    renderButtonsActions() {
        if (this.props.id == 0) {
            return (
                <CustomButton
                    title="Criar Promoção"
                    width={260}
                    onPress={() => this.attemptSave()} />
            )
        } else {
            return (
                <View>
                    <CustomButton
                        title="Salvar"
                        width={260}
                        onPress={() => this.attemptSave()} />

                    <View style={{ height: 15 }} />

                    <CustomButtonAux
                        title="Excluir promoção"
                        width={260}
                        height={60}
                        onPress={() => this.confirmDelete()} />
                </View>
            )
        }
    }
}

mapStateToProps = state => (
    {
        nome: state.PromocaoReducer.nome,
        pontosRestate: state.PromocaoReducer.pontosRestate,
        descricao: state.PromocaoReducer.descricao,
        id: state.PromocaoReducer.id,
        estabelecimentos: state.EstabelecimentoReducer.estabelecimentos
    }
)

export default connect(mapStateToProps, { changeNome, changePontos, changeDescricao, changeId, editarPromocao, criarPromocao, excluirPromocoes })(EditarPromocao);