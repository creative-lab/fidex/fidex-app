import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import SegmentedControlTab from 'react-native-segmented-control-tab';

import ListaClientesAtivos from './ListaClientesAtivos';
import ListaClientesSeguidores from './ListaClientesSeguidores';

import { colors } from '../../../../res/colors';

import { recuperarClientes } from '../../../actions/EstabelecimentoActions'

class ListaClientes extends Component {

    constructor() {
        super()
        this.state = {
            selectedIndex: 0,
        };
    }

    componentDidMount() {
        this.props.recuperarClientes(this.props.estabelecimentos[0].id)
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.bg }}>
                <View style={{ marginTop: 16, marginHorizontal: 16 }}>
                    <SegmentedControlTab

                        tabTextStyle={{ color: colors.primary_color }}
                        activeTabTextStyle={{ color: 'white' }}
                        tabStyle={{ borderColor: colors.primary_color }}
                        activeTabStyle={{ backgroundColor: colors.primary_color }}
                        values={['Clientes Ativos', 'Seguidores']}
                        selectedIndex={this.state.selectedIndex}
                        onTabPress={this.handleIndexChange}
                    />
                </View>
                {this.renderTabContent()}
            </View>
        )
    }

    handleIndexChange = (index) => {
        this.setState({
            ...this.state,
            selectedIndex: index,
        });
    }

    renderTabContent() {
        if (this.state.selectedIndex === 0) {
            return (<ListaClientesAtivos />)
        } else {
            return (<ListaClientesSeguidores />)
        }
    }
}
mapStateToProps = state => (
    {
        estabelecimentos: state.EstabelecimentoReducer.estabelecimentos,
    }
)

export default connect(mapStateToProps, { recuperarClientes })(ListaClientes);