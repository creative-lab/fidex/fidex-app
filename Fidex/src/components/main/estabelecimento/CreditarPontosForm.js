import React, { Component } from 'react'
import { View, Alert, ScrollView, Text } from 'react-native';
import CPF from 'gerador-validador-cpf';
import CustomTextInput from '../../custom/CustomTextInput';
import CustomButton from '../../custom/CustomButton';
import { connect } from 'react-redux';

import { strings } from '../../../../res/strings';
import { changeCreditarPontosCPF, changeCreditarPontosValor, creditarPontos } from '../../../actions/EstabelecimentoActions'

class CreditarPontosForm extends Component {

    constructor() {
        super()
        this.state = {
            cpfInvalid: false,
        };
    }

    renderCPFError() {
        if (this.state.cpfInvalid) {
            return (
                <Text style={{ color: 'red' }}>CPF digitado é inválido</Text>
            )
        }
    }

    checkData() {
        let message = '';
        if (!this.props.creditarPontosCpf) {
            message += strings.field_cpf;
        }
        if (!this.props.creditarPontosValor) {
            if (message) {
                message += ', ';
            }
            message += "Valor";
        }
        return message;
    }

    attemptLogin() {
        let message = this.checkData();
        if (message) {
            Alert.alert(strings.title_error, `${strings.error_requireds_fiels} ${message}`);
        } else if (CPF.validate(this.props.creditarPontosCpf)) {
            this.setState({ ...this.state, cpfInvalid: false })

            // console.log(estabelecimentos)

            this.props.creditarPontos(this.props.creditarPontosCpf, this.props.creditarPontosValor, this.props.estabelecimentos[0].id)
        } else {
            this.setState({ ...this.state, cpfInvalid: true })
        }
    }

    render() {
        return (

            <ScrollView style={{ flex: 1 }}>
                <View style={{ alignItems: 'center', flex: 1, }}>

                    <View style={{ height: 20 }} />

                    <CustomTextInput
                        placeholder="Digite o CPF do cliente"
                        width={260}
                        maxLength={14}
                        value={this.props.creditarPontosCpf}
                        returnKeyType={"done"}
                        blurOnSubmit={false}
                        onChangeText={text => this.props.changeCreditarPontosCPF(text)}
                        onSubmitEditing={() => this.customInput.refs.inputValue.focus()}
                        keyboardType='numeric'
                    />
                    <View style={{ height: 15 }} />

                    <CustomTextInput
                        placeholder="Valor dastro pelo cliente (mínimo R$ 3,00)"
                        width={260}
                        value={this.props.creditarPontosValor}
                        returnKeyType={"done"}
                        blurOnSubmit={false}
                        refInner="inputValue"
                        onChangeText={text => this.props.changeCreditarPontosValor(text)}
                        onSubmitEditing={() => this.attemptLogin()}
                        keyboardType='numeric'
                        ref={ref => this.customInput = ref}
                    />
                    <View style={{ height: 20 }} />

                    {this.renderCPFError()}

                    <View style={{ height: 20 }} />

                    <CustomButton
                        title="Confirmar crédito"
                        width={260}
                        onPress={() => this.attemptLogin()} />

                </View>

            </ScrollView>
        )
    }
}



mapStateToProps = state => (
    {
        estabelecimentos: state.EstabelecimentoReducer.estabelecimentos,
        creditarPontosCpf: state.EstabelecimentoReducer.creditarPontosCpf,
        creditarPontosValor: state.EstabelecimentoReducer.creditarPontosValor,

    }
)

export default connect(mapStateToProps, { changeCreditarPontosCPF, changeCreditarPontosValor, creditarPontos })(CreditarPontosForm);