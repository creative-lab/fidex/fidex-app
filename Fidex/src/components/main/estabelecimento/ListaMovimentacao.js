import React, { Component } from 'react';
import { View, Text, ListView, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { colors } from '../../../../res/colors';

import ItemMovimentacao from './ItemMovimentacao';

import { recuperarListaMovimentacoes } from '../../../actions/EstabelecimentoActions'

class ListaMovimentacao extends Component {

    componentDidMount() {
        this.props.recuperarListaMovimentacoes(this.props.estabelecimentos[0].id)
    }

    renderList() {

        if (this.props.listaMovimentacoes === null) {
            return (
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Text>Recuperando movimentações ...</Text>
                </View>
            )
        } else {
            const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
            let fonteDeDados = ds.cloneWithRows(this.props.listaMovimentacoes)
            return (
                <View style={{ flex: 1, backgroundColor: colors.bg }}>
                    <ListView
                        enableEmptySections
                        dataSource={fonteDeDados}
                        renderSeparator={(rowId) =>
                            <View key={rowId} style={{
                                flex: 1,
                                marginHorizontal: 16,
                                height: StyleSheet.hairlineWidth,
                                backgroundColor: colors.border_color,
                            }}
                            />}
                        renderRow={item => <ItemMovimentacao movimentacao={item} tipo={item.tipo} />} />
                </View>
            )
        }
    }

    render() {
        return (
            this.renderList()
        )
    }
}

mapStateToProps = state => (
    {
        estabelecimentos: state.EstabelecimentoReducer.estabelecimentos,
        listaMovimentacoes: state.EstabelecimentoReducer.listaMovimentacoes
    }
)

export default connect(mapStateToProps, { recuperarListaMovimentacoes })(ListaMovimentacao);