import React, { Component } from 'react';
import { View, Text, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialIcons'

import { changePromocao } from '../../../actions/PromocaoActions';
import { colors } from '../../../../res/colors';

class ItemPromocao extends Component {

    editarPromocao() {
        console.log(this.props.promocao);
        this.props.changePromocao(this.props.promocao);

        Actions.editarPromocao({ idEstabelecimento: this.props.promocao.estabelecimento_id });
    }

    render() {
        return (
            <TouchableHighlight
                onPress={() => this.editarPromocao()}
                underlayColor={colors.bg_color_button_pressed}
                activeOpacity={0.7}>
                <View
                    style={{ flexDirection: 'row', padding: 16, justifyContent: 'space-between', alignItems: 'center' }}>
                    <View >
                        <Text style={{ fontSize: 18 }}>{this.props.promocao.promocao}</Text>
                        <Text style={{ fontSize: 14 }}>{this.props.promocao.pontos}</Text>
                    </View>

                    <Icon name='keyboard-arrow-right' size={32} color={colors.primary_color} />
                </View>
            </TouchableHighlight>
        )
    }
}

export default connect(null, { changePromocao })(ItemPromocao);