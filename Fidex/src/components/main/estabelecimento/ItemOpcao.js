import React, { Component } from 'react';
import { View, Text, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialIcons'

import { changePromocao } from '../../../actions/PromocaoActions';
import { colors } from '../../../../res/colors';

class ItemOpcao extends Component {

    openScreen() {
        switch (parseInt(this.props.position)) {
            case 0:
                Actions.listaMovimentacao();
                break
            case 1:
                Actions.pontosCreditados();
                break;
            case 2:
                Actions.resgateDePromocoes();
                break;
            case 3:
                Actions.listaClientes();
                break;
        }
    }

    render() {
        return (
            <TouchableHighlight
                onPress={() => this.openScreen()}
                underlayColor={colors.bg_color_button_pressed}
                activeOpacity={0.7}>
                <View
                    style={{ flexDirection: 'row', padding: 16, justifyContent: 'space-between', alignItems: 'center' }}>
                    <Text>{this.props.item.opcao}</Text>
                    <Icon name='keyboard-arrow-right' size={32} color={colors.primary_color} />
                </View>
            </TouchableHighlight>
        )
    }
}

export default connect(null, { changePromocao })(ItemOpcao);