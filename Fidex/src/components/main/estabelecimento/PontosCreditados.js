import React, { Component } from 'react';
import { View, Text, ListView, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

import { colors } from '../../../../res/colors';
import ItemMovimentacao from './ItemMovimentacao';
import CustomButtonAux2 from '../../custom/CustomButtonAux2';
import { recuperarListaPontosCreditados } from '../../../actions/EstabelecimentoActions'

class PontosCreditados extends Component {

    componentDidMount() {
        this.props.recuperarListaPontosCreditados(this.props.estabelecimentos[0].id)
    }

    renderList() {

        if (this.props.pontosCreditados === null) {
            return (
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Text>Recuperando pontos creditados ...</Text>
                </View>
            )
        } else {
            const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
            let fonteDeDados = ds.cloneWithRows(this.props.pontosCreditados.lista)
            return (
                <View style={{ flex: 1, backgroundColor: colors.bg }}>
                    <View style={{ flexDirection: 'row', padding: 16 }}>
                        <View style={{ flex: 1, paddingHorizontal: 16, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ paddingBottom: 4 }}>Gastos de clientes</Text>
                            <CustomButtonAux2
                                underlayColor={colors.bg_color_button_pressed}
                                activeOpacity={0.7}
                                bold
                                title={`R$ ${this.props.pontosCreditados.gastos_clientes}`}
                                onPress={() => false} />
                        </View>
                        <View style={{ flex: 1, paddingHorizontal: 16, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ paddingBottom: 4 }}>Pontos Creditados</Text>
                            <CustomButtonAux2
                                underlayColor={colors.bg_color_button_pressed}
                                activeOpacity={0.7}
                                bold
                                title={`${this.props.pontosCreditados.pontos_creditados[0].pontos} pontos`}
                                onPress={() => false} />
                        </View>
                    </View>
                    <View style={{ width: '100%', height: 1, backgroundColor: colors.border_color }} />
                    <ListView
                        enableEmptySections
                        dataSource={fonteDeDados}
                        renderSeparator={(rowId) =>
                            <View key={rowId} style={{
                                flex: 1,
                                marginHorizontal: 16,
                                height: StyleSheet.hairlineWidth,
                                backgroundColor: colors.border_color,
                            }}
                            />}
                        renderRow={item => <ItemMovimentacao movimentacao={item} tipo='credito' />} />
                </View>
            )
        }
    }

    render() {
        return (
            this.renderList()
        )
    }
}
mapStateToProps = state => (
    {
        pontosCreditados: state.EstabelecimentoReducer.pontosCreditados,
        estabelecimentos: state.EstabelecimentoReducer.estabelecimentos,
    }
)

export default connect(mapStateToProps, { recuperarListaPontosCreditados })(PontosCreditados);