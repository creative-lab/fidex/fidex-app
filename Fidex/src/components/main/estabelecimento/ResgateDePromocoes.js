import React, { Component } from 'react';
import { View, Text, ListView, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

import { colors } from '../../../../res/colors';
import ItemMovimentacao from './ItemMovimentacao';
import CustomButtonAux2 from '../../custom/CustomButtonAux2';
import { recuperarListaResgatesPromocao } from '../../../actions/EstabelecimentoActions'

class ResgateDePromocoes extends Component {

    componentDidMount() {
        this.props.recuperarListaResgatesPromocao(this.props.estabelecimentos[0].id)
    }

    renderList() {

        if (this.props.resgatePromocoes === null) {
            return (
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Text>Recuperando resgate de promoções ...</Text>
                </View>
            )
        } else {

            const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
            let fonteDeDados = ds.cloneWithRows(this.props.resgatePromocoes.lista)
            return (
                <View style={{ flex: 1, backgroundColor: colors.bg }}>
                    <View style={{ flexDirection: 'row', padding: 16 }}>
                        <View style={{ flex: 1, paddingHorizontal: 16, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ paddingBottom: 4 }}>Pontos utilizados</Text>
                            <CustomButtonAux2
                                underlayColor={colors.bg_color_button_pressed}
                                activeOpacity={0.7}
                                bold
                                title={`${this.props.resgatePromocoes.pontos_utilizados.pontos} pontos`}
                                onPress={() => false} />
                        </View>
                        <View style={{ flex: 1, paddingHorizontal: 16, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ paddingBottom: 4 }}>Total de resgates</Text>
                            <CustomButtonAux2
                                underlayColor={colors.bg_color_button_pressed}
                                activeOpacity={0.7}
                                bold
                                title={`${this.props.resgatePromocoes.total_resgates.promocoes_resgatadas} resgates`}
                                onPress={() => false} />
                        </View>
                    </View>
                    <View style={{ width: '100%', height: 1, backgroundColor: colors.border_color }} />
                    <ListView
                        enableEmptySections
                        dataSource={fonteDeDados}
                        renderSeparator={(rowId) =>
                            <View key={rowId} style={{
                                flex: 1,
                                marginHorizontal: 16,
                                height: StyleSheet.hairlineWidth,
                                backgroundColor: colors.border_color,
                            }}
                            />}
                        renderRow={item => <ItemMovimentacao movimentacao={item} tipo='resgate' />} />
                </View>
            )
        }
    }

    render() {
        return (
            this.renderList()
        )
    }
}
mapStateToProps = state => (
    {
        estabelecimentos: state.EstabelecimentoReducer.estabelecimentos,
        resgatePromocoes: state.EstabelecimentoReducer.resgatePromocoes,
    }
)

export default connect(mapStateToProps, { recuperarListaResgatesPromocao })(ResgateDePromocoes);