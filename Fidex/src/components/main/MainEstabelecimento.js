import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import TabNavigator from 'react-native-tab-navigator';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconMoomConfig from '../../selection.json';
const Icon = createIconSetFromIcoMoon(iconMoomConfig);

import { colors } from '../../../res/colors';
import HomeEmpresas from './HomeEmpresas';
import ListaPromocoes from './estabelecimento/ListaPromocoes';
import Transation from './Transation';
import Chart from './Chart';

import { changeTab } from '../../actions/MainActions';

class Main extends Component {

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.bg }}>

                <TabNavigator style={{ flex: 1, }}>
                    <TabNavigator.Item
                        selected={this.props.selectedTab === 'home'}
                        tabStyle={{ marginBottom: -7 }}
                        selectedTitleStyle={{ color: "#3496f0" }}
                        renderIcon={() => <Icon name='home' size={40} color={colors.border_color} />}
                        renderSelectedIcon={() => <Icon name='home' size={40} color={colors.primary_color} />}
                        onPress={() => this.props.changeTab('home')}>
                        <HomeEmpresas />
                    </TabNavigator.Item>

                    <TabNavigator.Item
                        selected={this.props.selectedTab === 'promotion'}
                        tabStyle={{ marginBottom: -7 }}
                        selectedTitleStyle={{ color: "#3496f0" }}
                        renderIcon={() => <Icon name='promotion' size={40} color={colors.border_color} />}
                        renderSelectedIcon={() => <Icon name='promotion' size={40} color={colors.primary_color} />}
                        onPress={() => this.props.changeTab('promotion')}>
                        <ListaPromocoes />
                    </TabNavigator.Item>

                    <TabNavigator.Item
                        selected={this.props.selectedTab === 'transation'}
                        tabStyle={{ marginBottom: -7 }}
                        selectedTitleStyle={{ color: "#3496f0" }}
                        renderIcon={() => <Icon name='transation' size={40} color={colors.border_color} />}
                        renderSelectedIcon={() => <Icon name='transation' size={40} color={colors.primary_color} />}
                        onPress={() => this.props.changeTab('transation')}>
                        <Transation />
                    </TabNavigator.Item>

                    <TabNavigator.Item
                        selected={this.props.selectedTab === 'chart'}
                        tabStyle={{ marginBottom: -7 }}
                        selectedTitleStyle={{ color: "#3496f0" }}
                        renderIcon={() => <Icon name='chart' size={40} color={colors.border_color} />}
                        renderSelectedIcon={() => <Icon name='chart' size={40} color={colors.primary_color} />}
                        onPress={() => this.props.changeTab('chart')}>
                        <Chart />
                    </TabNavigator.Item>

                </TabNavigator>
            </View>
        )
    }
}

mapStateToProps = state => (
    {
        selectedTab: state.MainReducer.selectedTab
    }
)


export default connect(mapStateToProps, { changeTab })(Main);
