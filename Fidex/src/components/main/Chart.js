import React, { Component } from 'react';
import { View, ListView, StyleSheet } from 'react-native';

import { connect } from 'react-redux';
import { colors } from '../../../res/colors';
import { strings } from '../../../res/strings';
import ItemOpcao from './estabelecimento/ItemOpcao';

class Chart extends Component {


    constructor() {
        super()
        this.state = {
            opcoes: [
                {
                    opcao: strings.lista_movimentacao
                },
                {
                    opcao: strings.pontos_creditados
                },
                {
                    opcao: strings.resgate_promocoes
                },
                {
                    opcao: strings.clientes_estabelecimento
                },
            ]
        };
    }

    renderOptions() {
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })

        let fonteDeDados = ds.cloneWithRows(this.state.opcoes)

        return (
            <ListView
                enableEmptySections
                dataSource={fonteDeDados}
                renderSeparator={(rowId) =>
                    <View key={rowId} style={{
                        flex: 1,
                        marginHorizontal: 16,
                        height: StyleSheet.hairlineWidth,
                        backgroundColor: colors.border_color,
                    }}
                    />}
                renderRow={(item, sectionId, rowId) => <ItemOpcao item={item} position={rowId} />} />
        )
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                {this.renderOptions()}
            </View>
        )
    }
}


mapStateToProps = state => (
    {
    }
)

export default connect(mapStateToProps, null)(Chart);