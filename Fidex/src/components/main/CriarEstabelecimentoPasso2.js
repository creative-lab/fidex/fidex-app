import React, { Component } from 'react';
import { View, Text, ScrollView, Alert } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import CustomButton from '../custom/CustomButton';
import CustomTextInput from '../custom/CustomTextInput';
import CustomTextArea from '../custom/CustomTextArea';
import CustomDropdown from '../custom/CustomDropdown';
import { colors } from '../../../res/colors';
import { strings } from '../../../res/strings';
import { changeDescription, changeKeywords, changeCategory } from '../../actions/EstabelecimentoActions';

class CriarEstabelecimentoPasso2 extends Component {

    constructor(props) {
        super(props)

        this.state = {
            categories: [{
                value: 'Categoria',
            }, {
                value: 'Saúde e Beleza',
            }, {
                value: 'Bares e Restaurantes',
            }, {
                value: 'Cafés',
            }]
        }
    }

    checkData() {
        let message = '';
        if (!this.props.description) {
            message += strings.field_description;
        }

        if (this.props.category === 'Categoria') {
            message += 'Categoria';
        }

        if (!this.props.keywords) {
            if (message) {
                message += ', ';
            }
            message += strings.placeholder_keywords;
        }

        return message;
    }

    attemptCreateEstabelecimento() {
        let message = this.checkData();


        if (message) {
            Alert.alert(strings.title_error, `${strings.error_requireds_fiels} ${message}`);
        } else {
            console.log(this.props.nome)
            Actions.criarEstabelecimentoPasso3({
                nome: this.props.nome,
                email: this.props.email,
                phone: this.props.phone,
                whatsapp: this.props.whatsapp,
                cnpj: this.props.cnpj,
                endereco: this.props.endereco,
                bairro: this.props.bairro,
                cidade: this.props.cidade,
                uf: this.props.uf,
                cep: this.props.cep,
                description: this.props.description,
                category: this.props.category,
                keywords: this.props.keywords,
                user_id: this.props.user_id
            });
        }
    }

    render() {

        return (
            <View style={{ flex: 1, backgroundColor: colors.bg }}>
                <ScrollView style={{ flex: 1 }}>
                    <View style={{ alignItems: 'center', flex: 1, paddingBottom: 16, paddingTop: 32 }}>

                        <Text style={{ fontSize: 20, textAlign: 'center', color: colors.primary_color, paddingHorizontal: 16 }}>Precisamos de mais algumas informações</Text>

                        <View style={{ height: 20 }} />

                        <CustomTextArea
                            placeholder={strings.placeholder_descricao_estabelecimento}
                            width={260}
                            height={200}
                            maxLength={1000}
                            onChangeText={text => this.props.changeDescription(text)}
                            value={this.props.description} />

                        <View style={{ height: 15 }} />

                        <CustomDropdown
                            placeholder={strings.placeholder_descricao_estabelecimento}
                            width={260}
                            onChangeText={text => this.props.changeCategory(text)}
                            data={this.state.categories}
                            value={this.props.category} />

                        <View style={{ height: 15 }} />

                        <CustomTextInput
                            placeholder={strings.placeholder_keywords}
                            width={260}
                            onChangeText={text => this.props.changeKeywords(text)}
                            value={this.props.keywords}
                            returnKeyType={"next"}
                            blurOnSubmit={false} />
                        <Text style={{ fontSize: 10, color: colors.border_color, width: 260 }}>Use até 5 termos sobre o seu negócio. Ex.: comida japoneza, churrasco, rodízio, picanha, saladas</Text>

                        <View style={{ height: 15 }} />

                        <CustomButton
                            title="Continuar"
                            width={260}
                            onPress={() => this.attemptCreateEstabelecimento()} />

                    </View>
                </ScrollView>
            </View>
        )
    }
}

mapStateToProps = state => (
    {
        description: state.EstabelecimentoReducer.description,
        keywords: state.EstabelecimentoReducer.keywords,
        category: state.EstabelecimentoReducer.category,
        estabelecimento_id: state.EstabelecimentoReducer.id,
    }
)

export default connect(mapStateToProps, { changeDescription, changeKeywords, changeCategory })(CriarEstabelecimentoPasso2);