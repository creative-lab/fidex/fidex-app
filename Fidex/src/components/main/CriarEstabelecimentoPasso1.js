import React, { Component } from 'react';
import { View, Text, ScrollView, Alert } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import CustomButton from '../custom/CustomButton';
import CustomTextInput from '../custom/CustomTextInput';
import CustomDropdown from '../custom/CustomDropdown';
import Validator from '../../util/Validator';

import { colors } from '../../../res/colors';
import { strings } from '../../../res/strings';

import { changeName, changeEmail, changePhone, changeWhatsApp, changeCnpj, changeEndereco, changeBairro, changeCidade, changeUF, changeCep } from '../../actions/EstabelecimentoActions';

class CriarEstabelecimentoPasso1 extends Component {

    constructor(props) {
        super(props)

        this.state = {
            ufs: [
                {
                    value: 'UF',
                }, {
                    value: 'AC',
                }, {
                    value: 'AL',
                }, {
                    value: 'AM',
                }, {
                    value: 'AP',
                }, {
                    value: 'BA',
                }, {
                    value: 'CE',
                }, {
                    value: 'DF',
                }, {
                    value: 'ES',
                }, {
                    value: 'GO',
                }, {
                    value: 'MA',
                }, {
                    value: 'MT',
                }, {
                    value: 'MS',
                }, {
                    value: 'MG',
                }, {
                    value: 'PA',
                }, {
                    value: 'PB',
                }, {
                    value: 'PR',
                }, {
                    value: 'PE',
                }, {
                    value: 'PI',
                }, {
                    value: 'RJ',
                }, {
                    value: 'RN',
                }, {
                    value: 'RS',
                }, {
                    value: 'RO',
                }, {
                    value: 'RR',
                }, {
                    value: 'SC',
                }, {
                    value: 'SP',
                }, {
                    value: 'SE',
                }, {
                    value: 'TO',
                }
            ]
        }
    }

    checkData() {
        let message = '';
        if (!this.props.name) {
            message += strings.field_name_estabelecimento;
        }

        if (!this.props.email) {
            if (message) {
                message += ', ';
            }
            message += strings.placeholder_email_estabelecimento;
        }

        if (!this.props.phone) {
            if (message) {
                message += ', ';
            }
            message += strings.field_phone;
        }

        if (!this.props.whatsapp) {
            if (message) {
                message += ', ';
            }
            message += strings.placeholder_whatsapp;
        }

        if (!this.props.endereco) {
            if (message) {
                message += ', ';
            }
            message += strings.placeholder_endereco;
        }

        if (!this.props.bairro) {
            if (message) {
                message += ', ';
            }
            message += strings.placeholder_bairro;
        }

        if (!this.props.cidade) {
            if (message) {
                message += ', ';
            }
            message += strings.placeholder_cidade;
        }

        if (this.props.uf == 'UF') {
            if (message) {
                message += ', ';
            }
            message += strings.placeholder_uf;
        }

        if (!this.props.cep) {
            if (message) {
                message += ', ';
            }
            message += strings.placeholder_cep;
        }

        return message;
    }

    attemptCreateEstabelecimento() {
        let message = this.checkData();


        if (message) {
            Alert.alert(strings.title_error, `${strings.error_requireds_fiels} ${message}`);
        } else if (!Validator.isEmailValid(this.props.email)) {
            Alert.alert(strings.title_error, strings.invalid_email);
        } else {
            Actions.criarEstabelecimentoPasso2({
                nome: this.props.nome,
                email: this.props.email,
                phone: this.props.phone,
                whatsapp: this.props.whatsapp,
                cnpj: this.props.cnpj,
                endereco: this.props.endereco,
                bairro: this.props.bairro,
                cidade: this.props.cidade,
                uf: this.props.uf,
                cep: this.props.cep,
                user_id: this.props.user_id
            });
        }
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.bg }}>
                <ScrollView style={{ flex: 1 }}>
                    <View style={{ alignItems: 'center', flex: 1, paddingBottom: 16, paddingTop: 32 }}>

                        <Text style={{ fontSize: 20, textAlign: 'center', color: colors.primary_color, paddingHorizontal: 16 }}>Dados básicos do seu negócio</Text>

                        <View style={{ height: 20 }} />

                        <CustomTextInput
                            placeholder={strings.placeholder_nome_estabelecimento}
                            width={260}
                            onChangeText={text => this.props.changeName(text)}
                            value={this.props.nome} />

                        <View style={{ height: 15 }} />

                        <CustomTextInput
                            placeholder={strings.placeholder_email_estabelecimento}
                            width={260}
                            onChangeText={text => this.props.changeEmail(text)}
                            value={this.props.email}
                            returnKeyType={"next"}
                            blurOnSubmit={false}
                            keyboardType='email-address' />
                        <View style={{ height: 15 }} />

                        <CustomTextInput
                            placeholder={strings.placeholder_phone}
                            width={260}
                            onChangeText={text => this.props.changePhone(text)}
                            value={this.props.phone}
                            returnKeyType={"next"}
                            blurOnSubmit={false}
                            keyboardType='phone-pad' />
                        <View style={{ height: 15 }} />

                        <CustomTextInput
                            placeholder={strings.placeholder_whatsapp}
                            width={260}
                            onChangeText={text => this.props.changeWhatsApp(text)}
                            value={this.props.whatsapp}
                            returnKeyType={"next"}
                            blurOnSubmit={false}
                            keyboardType='phone-pad' />
                        <View style={{ height: 15 }} />

                        <CustomTextInput
                            placeholder={strings.placeholder_cnpj_estabelecimento}
                            width={260}
                            onChangeText={text => this.props.changeCnpj(text)}
                            value={this.props.cnpj}
                            returnKeyType={"next"}
                            blurOnSubmit={false}
                            keyboardType='phone-pad' />
                        <View style={{ height: 15 }} />

                        <CustomTextInput
                            placeholder={strings.placeholder_endereco}
                            width={260}
                            onChangeText={text => this.props.changeEndereco(text)}
                            value={this.props.endereco} />

                        <View style={{ height: 15 }} />

                        <CustomTextInput
                            placeholder={strings.placeholder_bairro}
                            width={260}
                            onChangeText={text => this.props.changeBairro(text)}
                            value={this.props.bairro} />

                        <View style={{ height: 15 }} />

                        <CustomTextInput
                            placeholder={strings.placeholder_cidade}
                            width={260}
                            onChangeText={text => this.props.changeCidade(text)}
                            value={this.props.cidade} />

                        <View style={{ height: 15 }} />

                        <CustomDropdown
                            placeholder={strings.placeholder_descricao_estabelecimento}
                            width={260}
                            onChangeText={text => this.props.changeUF(text)}
                            data={this.state.ufs}
                            value={this.props.uf} />

                        <View style={{ height: 15 }} />

                        <CustomTextInput
                            placeholder={strings.placeholder_cep}
                            width={260}
                            onChangeText={text => this.props.changeCep(text)}
                            keyboardType='phone-pad'
                            value={this.props.cep} />

                        <View style={{ height: 30 }} />

                        <CustomButton
                            title="Continuar"
                            width={260}
                            onPress={() => this.attemptCreateEstabelecimento()} />

                    </View>
                </ScrollView>
            </View>
        )
    }
}

mapStateToProps = state => (
    {
        nome: state.EstabelecimentoReducer.name,
        endereco: state.EstabelecimentoReducer.endereco,
        bairro: state.EstabelecimentoReducer.bairro,
        cidade: state.EstabelecimentoReducer.cidade,
        uf: state.EstabelecimentoReducer.uf,
        cep: state.EstabelecimentoReducer.cep,
        email: state.EstabelecimentoReducer.email,
        phone: state.EstabelecimentoReducer.phone,
        whatsapp: state.EstabelecimentoReducer.whatsapp,
        cnpj: state.EstabelecimentoReducer.cnpj,
        user_id: state.AuthReducer.id,
        estabelecimento_id: state.EstabelecimentoReducer.id,
    }
)

export default connect(mapStateToProps, { changeName, changeEmail, changePhone, changeWhatsApp, changeCnpj, changeEndereco, changeBairro, changeCidade, changeUF, changeCep })(CriarEstabelecimentoPasso1);