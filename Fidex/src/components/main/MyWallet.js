import React, { Component } from 'react';
import { View, Text, ListView, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

import { colors } from '../../../res/colors';
import ItemCarteira from './usuario/ItemCarteira';
import { recuperarListaVouchers } from '../../actions/UsuarioActions'

class MyWallet extends Component {

    componentDidMount() {
        this.props.recuperarListaVouchers(this.props.userId)
    }

    renderCarteira() {
        // console.log(this.props.listaVouchers != null && this.props.listaVouchers.length > 0)
        if (this.props.listaVouchers != null && this.props.listaVouchers.length > 0) {

            const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })

            let fonteDeDados = ds.cloneWithRows(this.props.listaVouchers)

            return (
                <ListView
                    enableEmptySections
                    dataSource={fonteDeDados}
                    renderSeparator={(rowId) =>
                        <View key={rowId} style={{
                            flex: 1,
                            marginHorizontal: 16,
                            height: StyleSheet.hairlineWidth,
                            backgroundColor: colors.border_color,
                        }}
                        />}
                    renderRow={item => <ItemCarteira item={item} />} />
            )
        } else {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ margin: 20, textAlign: 'center' }}>Nenhum voucher disponível. Pontue nos estabelecimentos que você segue e resgate promoções</Text>
                </View>
            )
        }
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                {this.renderCarteira()}
            </View>
        )
    }
}

mapStateToProps = state => (
    {
        userId: state.AuthReducer.id,
        dashboard: state.UsuarioReducer.dashboard,
        listaVouchers: state.UsuarioReducer.listaVouchers
    }
)

export default connect(mapStateToProps, { recuperarListaVouchers })(MyWallet);