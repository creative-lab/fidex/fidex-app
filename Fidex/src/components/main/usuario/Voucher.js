import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import { connect } from 'react-redux';

import { colors } from '../../../../res/colors';
import CustomButtonAux from '../../custom/CustomButtonAux';
import { recuperarDetalhesVoucher } from '../../../actions/UsuarioActions'

class Voucher extends Component {

    componentDidMount() {
        console.log(this.props.voucher)
        this.props.recuperarDetalhesVoucher(this.props.userId, this.props.voucher.voucher_id)
    }

    renderVoucher() {
        if (this.props.detalheVoucher) {
            return (
                <View style={{ alignItems: 'center', flex: 1, paddingHorizontal: 30 }}>
                    <Text style={{ color: colors.primary_color, fontSize: 20, fontWeight: 'bold' }}>{this.props.detalheVoucher.promocao.promocao}</Text>
                    <Text style={{ fontWeight: 'bold', fontSize: 18 }}>{this.props.voucher.nome}</Text>
                    <Text style={{ marginBottom: 30, textAlign: 'center' }}>{`Voucher válido de ${this.props.detalheVoucher.voucher.validade_inicial} até ${this.props.detalheVoucher.voucher.validade_final}`}</Text>
                    <Text style={{ marginBottom: 6 }}>Código do voucher</Text>
                    <CustomButtonAux title={this.props.detalheVoucher.voucher.codigo} width={260} />
                    <View style={{ height: 30 }} />
                    <Text style={{ marginBottom: 30, textAlign: 'center' }}>Apresente este voucher no estabelecimento para confirmar o resgate da promoção</Text>
                    <Text style={{ fontWeight: 'bold', fontSize: 18, textAlign: 'center' }}>Regulamento</Text>
                    <Text >{this.props.detalheVoucher.promocao.descricao}</Text>
                </View>
            )
        }
    }
    render() {
        return (
            <ScrollView style={{ padding: 16, backgroundColor: colors.bg }}>
                {this.renderVoucher()}
            </ScrollView>
        )
    }

}

mapStateToProps = state => (
    {
        userId: state.AuthReducer.id,
        detalheVoucher: state.UsuarioReducer.detalheVoucher
    }
)

export default connect(mapStateToProps, { recuperarDetalhesVoucher })(Voucher);