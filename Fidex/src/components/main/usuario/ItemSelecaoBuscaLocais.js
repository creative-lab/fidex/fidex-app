import React from 'react';
import { View, Text, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';

import { colors } from '../../../../res/colors';

import { changeTypeOfSearchPlaces } from '../../../actions/EstabelecimentoActions'

const ItemSelecaoBuscaLocais = props => (

    <TouchableHighlight style={{ padding: 16 }}
        underlayColor={colors.bg_color_button_pressed}
        activeOpacity={0.7}

        onPress={() => props.changeTypeOfSearchPlaces(props.tipo.nome)}>
        <View>
            <Text style={{ color: props.tipoBuscaPorLocais === props.tipo.nome ? colors.primary_color : colors.border_color }}>{props.tipo.nome}</Text>
        </View>
    </TouchableHighlight>

)

mapStateToProps = state => (
    {
        statusBuscaPorLocais: state.EstabelecimentoReducer.statusBuscaPorLocais,
        tipoBuscaPorLocais: state.EstabelecimentoReducer.tipoBuscaPorLocais,
    }
)

export default connect(mapStateToProps, { changeTypeOfSearchPlaces })(ItemSelecaoBuscaLocais);