import React, { Component } from 'react';
import { View, Text, Image, ScrollView, Alert, TouchableHighlight, ListView, Platform } from 'react-native';
import { connect } from 'react-redux';
import Communications from 'react-native-communications';
import openMap from 'react-native-open-maps';

import { colors } from '../../../../res/colors';
import CustomButtonAux2 from '../../custom/CustomButtonAux2';
import { strings } from '../../../../res/strings';
import { recuperarPaginaEstabelecimentos, seguirEstabelecimento } from '../../../actions/EstabelecimentoActions';
import ItemPromocaoPagina from './ItemPromocaoPagina';

import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconMoomConfig from '../../../selection.json';
const Icon = createIconSetFromIcoMoon(iconMoomConfig);
import { formatUrl } from '../../../util/Utils' 

class PaginaEstabelecimento extends Component {

    componentDidMount() {
        this.props.recuperarPaginaEstabelecimentos(this.props.user_id, this.props.estabelecimento.id)
    }

    renderFacebookButton() {
        if (this.props.facebook) {
            let link = formatUrl(this.props.facebook);
            return (
                <TouchableHighlight
                    style={{ marginRight: 8 }}
                    underlayColor={colors.bg_color_button_pressed}
                    activeOpacity={0.7}
                    onPress={() =>
                        link ?
                            Communications.web(link) :
                            Alert.alert("Facebook", strings.link_not_found)
                    } >
                    <Icon name='facebook_ico' size={40} color={colors.primary_color} />
                </TouchableHighlight>
            )
        }
    }

    renderInstagramButton() {
        if (this.props.instagram) {
            let link = formatUrl(this.props.instagram);
            return (
                <TouchableHighlight
                    style={{ marginRight: 8 }}
                    underlayColor={colors.bg_color_button_pressed}
                    activeOpacity={0.7}
                    onPress={() =>
                        link ?
                            Communications.web(link) :
                            Alert.alert("Instagram", strings.link_not_found)
                    } >
                    <Icon name='instagram_ico' size={40} color={colors.primary_color} />
                </TouchableHighlight>
            )
        }
    }

    renderTwitterButton() {
        if (this.props.twitter) {
            let link = formatUrl(this.props.twitter);

            return (
                <TouchableHighlight
                    style={{ marginRight: 8 }}
                    underlayColor={colors.bg_color_button_pressed}
                    activeOpacity={0.7}
                    onPress={() =>
                        link ?
                            Communications.web(link) :
                            Alert.alert("Twitter", strings.link_not_found)
                    }>
                    <Icon name='twitter_ico' size={40} color={colors.primary_color} />
                </TouchableHighlight>
            )
        }
    }

    renderFollowButton() {
        if (this.props.segue === 'nao') {
            return (
                <CustomButtonAux2
                    underlayColor={colors.bg_color_button_pressed}
                    activeOpacity={0.7}
                    title="Seguir"
                    onPress={() => this.seguirEstabelecimento()} />
            )
        }
    }

    renderUnfollowButton() {
        if (this.props.segue !== 'nao') {
            return (
                <CustomButtonAux2
                    underlayColor={colors.bg_color_button_pressed}
                    activeOpacity={0.7}
                    width={130}
                    title="Deixar de seguir"
                    onPress={() => this.confirmUnfollow()} />
            )
        }
    }

    renderPromocoes() {
        if (this.props.promocoes != null) {

            const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
            let fonteDeTipos = ds.cloneWithRows(this.props.promocoes)

            return (
                <View>
                    <View style={{ width: '100%', height: 1, backgroundColor: colors.border_color, marginVertical: 8 }} />

                    <Text style={{ color: colors.border_color, fontSize: 18, }}>Promoções</Text>

                    <ListView
                        enableEmptySections
                        dataSource={fonteDeTipos}
                        renderRow={promocao => <ItemPromocaoPagina promocao={promocao} pontosUsuario={this.props.pontosUsuario} estabelecimentoId={this.props.estabelecimentoId} />} />
                </View>
            )

        }

    }

    renderSocialButtons() {
        if (this.props.facebook || this.props.instagram || this.twitter) {
            return (
                <View style={{ flexDirection: 'row', marginVertical: 20 }}>
                    {this.renderFacebookButton()}
                    {this.renderInstagramButton()}
                    {this.renderTwitterButton()}
                </View>
            )
        }
    }

    renderDescricao() {

        if (this.props.description) {
            return (
                <View>
                    <View style={{ width: '100%', height: 1, backgroundColor: colors.border_color, marginVertical: 8 }} />
                    <Text style={{ color: colors.border_color }}>{this.props.description}</Text>
                </View>
            )
        }
    }

    render() {

        return (
            <ScrollView style={{ flex: 1, backgroundColor: colors.bg, }}>
                <View style={{ padding: 16 }}>

                    <View style={{ flexDirection: 'row', marginVertical: 10 }}>
                        <View style={{ height: 80, width: 80, borderRadius: 40, backgroundColor: colors.border_color }}>
                            <Image
                                source={{ uri: this.props.avatar }}
                                style={{ height: 80, width: 80, borderRadius: 40 }} />
                        </View>

                        <View style={{ marginLeft: 16, justifyContent: 'center' }}>
                            <Text style={{ color: colors.primary_color, fontSize: 20, fontWeight: 'bold' }}>{this.props.nome}</Text>
                            <Text style={{ color: colors.border_color, fontSize: 20 }}>{this.props.category}</Text>
                            <Text style={{ color: colors.border_color, marginRight: 16 }} ellipsizeMode='tail' numberOfLines={2}>{this.props.endereco}</Text>
                        </View>
                    </View>

                    <View style={{ width: '100%', height: 1, backgroundColor: colors.border_color, marginVertical: 8 }} />

                    <View style={{ flexDirection: 'row', marginVertical: 10, justifyContent: 'space-between' }}>

                        <View style={{ alignItems: 'center', width: 130 }}>
                            <Text style={{ color: colors.border_color, fontSize: 12 }}>Pontos disponíveis</Text>
                            <Text style={{ color: colors.primary_color, fontSize: 20, fontWeight: 'bold' }}>{this.props.pontosUsuario}</Text>
                        </View>

                        <View style={{ alignItems: 'center', width: 130 }}>
                            <Text style={{ color: colors.border_color, fontSize: 12 }}>Promoções resgatadas</Text>
                            <Text style={{ color: colors.primary_color, fontSize: 20, fontWeight: 'bold' }}>{this.props.promocoesResgatadas}</Text>
                        </View>

                        <View style={{ alignItems: 'center', width: 100, justifyContent: 'center' }}>
                            {this.renderFollowButton()}

                        </View>

                    </View>
                    {this.renderDescricao()}

                    {this.renderSocialButtons()}

                    {this.renderPromocoes()}

                    <View style={{ width: '100%', height: 1, backgroundColor: colors.border_color, marginVertical: 8 }} />
                    <Text style={{ color: colors.border_color, fontSize: 18, marginBottom: 10 }}>Informações de contato</Text>

                    <View style={{ marginBottom: 8 }}>
                        <Text style={{ color: colors.primary_color, fontWeight: 'bold' }}>E-mail</Text>
                        <Text style={{ color: colors.border_color }}>{this.props.email}</Text>
                    </View>

                    <View style={{ marginBottom: 8 }}>
                        <Text style={{ color: colors.primary_color, fontWeight: 'bold' }}>Telefone</Text>
                        <Text style={{ color: colors.border_color }}>{this.props.phone}</Text>
                    </View>

                    <View style={{ marginBottom: 8 }}>
                        <Text style={{ color: colors.primary_color, fontWeight: 'bold' }}>WhatsApp</Text>
                        <Text style={{ color: colors.border_color }}>{this.props.whatsapp}</Text>
                    </View>

                    <View style={{ width: '100%', height: 1, backgroundColor: colors.border_color, marginVertical: 20 }} />

                    <View style={{ marginBottom: 8 }}>
                        <Text style={{ color: colors.primary_color, fontWeight: 'bold' }}>Categoria</Text>
                        <Text style={{ color: colors.border_color }}>{this.props.category}</Text>
                    </View>

                    <View style={{ marginBottom: 8 }}>
                        <Text style={{ color: colors.primary_color, fontWeight: 'bold' }}>Palavras-chave</Text>
                        <Text style={{ color: colors.border_color }}>{this.props.keywords}</Text>
                    </View>

                    <View style={{ alignItems: 'center', justifyContent: 'center', paddingVertical: 20 }}>
                        <CustomButtonAux2
                            underlayColor={colors.bg_color_button_pressed}
                            activeOpacity={0.7}
                            width={130}
                            title="Como chegar"
                            onPress={() => this.comoChegarAoEstabelecimento()} />

                        <View style={{ height: 20 }} />

                        {this.renderUnfollowButton()}

                    </View>
                </View>
            </ScrollView>
        )
    }

    seguirEstabelecimento() {
        this.props.seguirEstabelecimento(this.props.user_id, this.props.estabelecimento.id)
    }

    deixarDeSeguirEstabelecimento() {
        Alert.alert(strings.default_title_alert, strings.in_development);
    }

    comoChegarAoEstabelecimento() {
        if (this.props.latitude && this.props.longitude) {
            openMap({ latitude: this.props.latitude, longitude: this.props.longitude, query: (Platform.OS === 'ios' ? this.props.nome : null) });
        } else {
            Alert.alert(strings.default_title_alert, "A localização deste estabelecimento não foi definida");
        }
    }

    confirmUnfollow() {
        Alert.alert(
            strings.default_title_alert,
            strings.msg_unfollow,
            [
                { text: strings.action_yes, onPress: () => this.deixarDeSeguirEstabelecimento(), style: 'destructive' },
                { text: strings.action_no, onPress: () => false, style: 'cancel' }
            ],
            { cancelable: true }
        )
    }
}

mapStateToProps = state => (
    {
        user_id: state.AuthReducer.id,
        estabelecimentoId: state.EstabelecimentoReducer.id,
        nome: state.EstabelecimentoReducer.name,
        email: state.EstabelecimentoReducer.email,
        phone: state.EstabelecimentoReducer.phone,
        whatsapp: state.EstabelecimentoReducer.whatsapp,
        description: state.EstabelecimentoReducer.description,
        keywords: state.EstabelecimentoReducer.keywords,
        category: state.EstabelecimentoReducer.category,
        facebook: state.EstabelecimentoReducer.facebook,
        instagram: state.EstabelecimentoReducer.instagram,
        twitter: state.EstabelecimentoReducer.twitter,
        avatar: state.EstabelecimentoReducer.avatar,
        endereco: state.EstabelecimentoReducer.endereco,
        segue: state.EstabelecimentoReducer.segue,
        promocoes: state.EstabelecimentoReducer.promocoes,
        pontosUsuario: state.EstabelecimentoReducer.pontosUsuario,
        latitude: state.EstabelecimentoReducer.latitude,
        longitude: state.EstabelecimentoReducer.longitude,
        promocoesResgatadas: state.EstabelecimentoReducer.promocoesResgatadas
    }
)

export default connect(mapStateToProps, { recuperarPaginaEstabelecimentos, seguirEstabelecimento })(PaginaEstabelecimento);