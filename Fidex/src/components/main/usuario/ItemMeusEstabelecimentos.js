import React from 'react'
import { View, Text, Image, TouchableHighlight } from 'react-native';
import { Actions } from 'react-native-router-flux';

import { colors } from '../../../../res/colors';

const ItemMeusEstabelecimentos = props => (

    <TouchableHighlight
        underlayColor={colors.bg_color_button_pressed}
        activeOpacity={0.7}
        onPress={() => Actions.paginaEstabelecimento({ estabelecimento: props.estabelecimento })} >
        <View style={{ flexDirection: 'row', padding: 16, alignItems: "center" }}>
            <View style={{ height: 80, width: 80, borderRadius: 40, backgroundColor: colors.border_color }}>
                <Image source={{ uri: null }}
                    style={{ height: 80, width: 80, borderRadius: 40 }}
                    source={{ uri: props.estabelecimento.avatar }} />
            </View>

            <View style={{ marginLeft: 16, justifyContent: 'center' }}>
                <Text style={{ color: colors.border_color, fontSize: 20 }}>{props.estabelecimento.nome}</Text>
                <Text style={{ color: colors.border_color, fontSize: 18 }}>{props.estabelecimento.categoria}</Text>
                <Text style={{ color: colors.border_color }}>{`${props.estabelecimento.pontos} pontos`}</Text>
            </View>
        </View>
    </TouchableHighlight>
)
export default ItemMeusEstabelecimentos;