import React, { Component } from 'react'
import { View, Text, TouchableHighlight } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import iconMoomConfig from '../../../selection.json';
const Icon = createIconSetFromIcoMoon(iconMoomConfig);

import { colors } from '../../../../res/colors';

class ItemCarteira extends Component {

    recuperarDetalhesDoVoucher() {
        Actions.voucher({ voucher: this.props.item });
    }

    render() {
        return (
            <TouchableHighlight
                underlayColor={colors.bg_color_button_pressed}
                activeOpacity={0.7}
                onPress={() => this.recuperarDetalhesDoVoucher()} >
                <View style={{ flexDirection: 'row', padding: 16, alignItems: "center" }}>
                    <Icon name='voucher' size={80} color={colors.primary_color} />

                    <View style={{ marginLeft: 16, justifyContent: 'center' }}>
                        <Text style={{ color: colors.border_color, fontSize: 20, color: colors.primary_color }}>{this.props.item.promocao}</Text>
                        <Text style={{ color: colors.border_color, fontSize: 18 }}>{this.props.item.nome}</Text>
                        <Text style={{ color: colors.border_color }}>{`${this.props.item.pontos} pontos utilizados`}</Text>
                    </View>
                </View>
            </TouchableHighlight>
        )
    }
}


mapStateToProps = state => (
    {
        userId: state.AuthReducer.id
    }
)

export default connect(mapStateToProps, null)(ItemCarteira);