import React, { Component } from 'react';
import { View, Text, TouchableHighlight } from 'react-native';
import * as Progress from 'react-native-progress';
import { connect } from 'react-redux';

import { colors } from '../../../../res/colors';
import { resgatarPromocao } from '../../../actions/UsuarioActions';

class ItemPromocaoPagina extends Component {

    renderBotaoResgatarPromocao() {
        if (this.props.pontosUsuario / this.props.promocao.pontos >= 1) {
            return (

                <TouchableHighlight
                    style={{ marginRight: 8 }}
                    underlayColor={colors.bg_color_button_pressed}
                    activeOpacity={0.7}
                    onPress={() => this.resgate()} >
                    <Text style={{ color: colors.primary_color, fontWeight: 'bold' }}>Resgatar Promoção</Text>
                </TouchableHighlight>

            )
        }
    }

    resgate() {
        console.log(this.props.promocao.id, this.props.user_id, this.props.estabelecimentoId)
        this.props.resgatarPromocao(this.props.user_id, this.props.promocao.id, this.props.estabelecimentoId)
    }

    render() {
        return (
            <View style={{ marginVertical: 10 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={{ color: colors.primary_color, fontWeight: 'bold' }}>{this.props.promocao.promocao}</Text>
                    {this.renderBotaoResgatarPromocao()}
                </View>
                <Progress.Bar progress={this.props.pontosUsuario / this.props.promocao.pontos} width={null} color={colors.primary_color} unfilledColor='#DDDDDD' borderRadius={0} borderWidth={0} />
                <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                    <Text style={{ color: colors.primary_color, fontWeight: 'bold' }}>{this.props.promocao.pontos}</Text>
                    <Text style={{ color: colors.primary_color, paddingLeft: 5 }}>pts</Text>
                </View>
            </View>
        )
    }
}

mapStateToProps = state => (
    {
        user_id: state.AuthReducer.id,
    }
)

export default connect(mapStateToProps, { resgatarPromocao })(ItemPromocaoPagina);
