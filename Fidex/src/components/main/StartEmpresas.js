import React, { Component } from 'react';
import { View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';

import { colors } from '../../../res/colors';
import { listarEstabelecimentos } from '../../actions/EstabelecimentoActions'

class StartEmpresas extends Component {

    componentDidMount() {
        if (this.props.userId) {
            this.props.listarEstabelecimentos(this.props.userId)
        }
    }

    renderNext() {
        if (this.props.estabelecimentos && this.props.estabelecimentos.length > 0) {
            Actions.mainEstabelecimento();
        } else {
            Actions.homeNoneEstabelecimentos();
        }
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.bg, alignItems: 'center' }}>
            </View>
        )
    }
}

mapStateToProps = state => (
    {
        userId: state.AuthReducer.id,
        estabelecimentos: state.EstabelecimentoReducer.estabelecimentos
    }
)

export default connect(mapStateToProps, { listarEstabelecimentos })(StartEmpresas);