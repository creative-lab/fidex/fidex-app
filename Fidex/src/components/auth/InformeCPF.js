import React, { Component } from 'react';
import { View, Text, Image, ScrollView, Alert, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';
import CPF from 'gerador-validador-cpf';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialIcons'

import CustomButton from '../custom/CustomButton';
import CustomTextInput from '../custom/CustomTextInput';

import { colors } from '../../../res/colors';
import { strings } from '../../../res/strings';

import { changeCPF } from '../../actions/AuthActions';

class InformeCPF extends Component {

    checkData() {
        let message = '';
        if (!this.props.cpf) {
            message += strings.field_cpf;
        }
        return message;
    }

    attemptFinishCreateAccount() {
        let message = this.checkData();
        if (message) {
            Alert.alert(strings.title_error, `${strings.error_requireds_fiels} ${message}`);
        } else if (CPF.validate(this.props.cpf)) {
            Actions.main();
        } else {
            Alert.alert(strings.title_error, strings.invalid_cpf);
        }
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.bg }}>
                <ScrollView style={{ flex: 1 }}>
                    <View style={{ alignItems: 'center', flex: 1, paddingBottom: 16, paddingTop: 32 }}>

                        <Text style={{ fontSize: 20, marginHorizontal: 40, textAlign: 'center', color: colors.primary_color }}>Para continuar precisamos que informe seu número de CPF</Text>

                        <View style={{ height: 20 }} />

                        <CustomTextInput
                            placeholder={strings.placeholder_login}
                            width={260}
                            onChangeText={text => this.props.changeCPF(text)}
                            value={this.props.cpf}
                            maxLength={14}
                            blurOnSubmit={false}
                            keyboardType='numeric' />
                        <View style={{ height: 15 }} />

                        <View style={{ height: 20 }} />

                        <CustomButton
                            title="Finalizar cadastro"
                            width={260}
                            onPress={() => this.attemptFinishCreateAccount()} />
                        <View style={{ height: 20 }} />

                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', width: 260 }}>
                            <Text style={{ textAlign: 'center', color: 'black' }}>Ao se cadastrar você concorda com nossos    
                            <Text style={{ fontWeight: "bold", textAlign: 'center', color: 'black' }}> Termos e condições de uso</Text>
                            </Text>
                        </View>

                        <View style={{ height: 30 }} />

                    </View>
                </ScrollView>
                <TouchableHighlight
                    style={{ margin: 16, position: "absolute", top: 0, left: 0 }}
                    underlayColor={colors.bg_color_button_pressed}
                    activeOpacity={0.7}
                    onPress={() => Actions.pop()} >
                    <Icon name='keyboard-arrow-left' size={32} color={colors.primary_color} />
                </TouchableHighlight>
            </View>
        )
    }
}

mapStateToProps = state => (
    {
        cpf: state.AuthReducer.cpf
    }
)

export default connect(mapStateToProps, { changeCPF })(InformeCPF);