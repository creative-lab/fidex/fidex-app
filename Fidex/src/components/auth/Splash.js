import React from 'react';
import {View, Image} from 'react-native'

const logo= require('../../../res/imgs/logo.png');
import {colors} from '../../../res/colors';

const Splash = props => (
    <View
        style={{flex:1, backgroundColor:colors.bg, justifyContent: 'center', alignItems:'center'}}>
        <Image style={{height:172, width:128}} source={logo}/>
    </View>
)
export default Splash;