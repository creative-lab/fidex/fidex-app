/**
 * @author Silio Silvestre siliosffreitas@gmail.com
 */

import React, { Component } from 'react';
import { View, Text, Image, ScrollView, Alert, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialIcons'

import CustomButton from '../custom/CustomButton';
import CustomTextInput from '../custom/CustomTextInput';
import Validator from '../../util/Validator';

import { colors } from '../../../res/colors';
import { strings } from '../../../res/strings';

const logo = require('../../../res/imgs/logo.png');
import { changeEmail, changePass, loginEstabelecimento } from '../../actions/AuthActions';

class LoginEmpresas extends Component {

    constructor(props) {
        super(props);

        this.focusNextField = this.focusNextField.bind(this);
        this.inputs = {};
    }

    focusNextField(id) {
        this.inputs[id].focus();
    }

    checkData() {
        let message = '';
        if (!this.props.email) {
            message += strings.field_email;
        }
        if (!this.props.pass) {
            if (message) {
                message += ', ';
            }
            message += strings.field_pass;
        }
        return message;
    }

    attemptLogin() {
        let message = this.checkData();
        if (message) {
            Alert.alert(strings.title_error, `${strings.error_requireds_fiels} ${message}`);
        } else if (Validator.isEmailValid(this.props.email)) {
            this.props.loginEstabelecimento(this.props.email, this.props.pass)
        } else {
            Alert.alert(strings.title_error, strings.invalid_email);
        }
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.bg }}>
                <ScrollView style={{ flex: 1 }}>
                    <View style={{ alignItems: 'center', flex: 1, paddingBottom: 16, paddingTop: 32 }}>

                        <Image source={logo} />

                        <View style={{ height: 20 }} />

                        <CustomTextInput
                            placeholder={strings.placeholder_email}
                            width={260}
                            onChangeText={text => this.props.changeEmail(text)}
                            value={this.props.email}
                            blurOnSubmit={false}
                            keyboardType='email-address'
                            returnKeyType={"next"}
                            onSubmitEditing={() => this.customInput.refs.inputPass.focus()} />
                        <View style={{ height: 15 }} />

                        <CustomTextInput
                            placeholder={strings.placeholder_pass}
                            width={260}
                            returnKeyType={"done"}
                            blurOnSubmit={false}
                            onChangeText={text => this.props.changePass(text)}
                            secureTextEntry
                            maxLength={10}
                            value={this.props.pass}
                            refInner="inputPass"
                            onSubmitEditing={() => this.attemptLogin()}
                            ref={ref => this.customInput = ref} />
                        <View style={{ height: 20 }} />

                        <CustomButton
                            title={strings.action_enter}
                            width={260}

                            onPress={() => this.attemptLogin()} />

                        <TouchableHighlight onPress={() => false} underlayColor={colors.bg_color_button_pressed}
                            style={{ marginTop: 30 }}
                            activeOpacity={0.7}>
                            <Text style={{ color: colors.primary_color }}>Esqueci minha senha</Text >
                        </TouchableHighlight>


                        <TouchableHighlight onPress={() => Actions.createAccountEmrpesaPasso1()} underlayColor={colors.bg_color_button_pressed}
                            style={{ marginTop: 10 }}
                            activeOpacity={0.7}>
                            <Text style={{ color: colors.primary_color }}>{strings.action_create_account}</Text >
                        </TouchableHighlight>

                    </View>

                </ScrollView>

                <TouchableHighlight
                    style={{ margin: 16, position: "absolute", top: 0, left: 0 }}
                    underlayColor={colors.bg_color_button_pressed}
                    activeOpacity={0.7}
                    onPress={() => Actions.pop()} >
                    <Icon name='keyboard-arrow-left' size={32} color={colors.primary_color} />
                </TouchableHighlight>
            </View>
        );
    }
}


mapStateToProps = state => (
    {
        email: state.AuthReducer.email,
        pass: state.AuthReducer.pass
    }
)

export default connect(mapStateToProps, { changeEmail, changePass, loginEstabelecimento })(LoginEmpresas);