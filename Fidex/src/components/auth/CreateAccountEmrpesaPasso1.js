import React, { Component } from 'react';
import { View, Text, Image, ScrollView, Alert, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';
import CPF from 'gerador-validador-cpf';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialIcons'

import CustomButton from '../custom/CustomButton';
import CustomTextInput from '../custom/CustomTextInput';
import Validator from '../../util/Validator';

import { colors } from '../../../res/colors';
import { strings } from '../../../res/strings';

import { changeName, changeCPF, changePass, changeEmail, changePhone, changeConfirmPass, criarConta } from '../../actions/AuthActions';

class CreateAccountEmrpesaPasso1 extends Component {

    checkData() {
        let message = '';
        if (!this.props.name) {
            message += strings.placeholder_name_create;
        }

        if (!this.props.cpf) {
            if (message) {
                message += ', ';
            }
            message += strings.field_cpf;
        }

        if (!this.props.email) {
            if (message) {
                message += ', ';
            }
            message += strings.field_email;
        }

        if (!this.props.phone) {
            if (message) {
                message += ', ';
            }
            message += strings.field_phone;
        }

        if (!this.props.pass) {
            if (message) {
                message += ', ';
            }
            message += strings.field_pass;
        }
        if (!this.props.confirmPass) {
            if (message) {
                message += ', ';
            }
            message += strings.placeholder_confirm_pass_create;
        }
        return message;
    }

    _criarConta() {
        this.props.criarConta(this.props.name, this.props.email, this.props.cpf, this.props.phone, this.props.pass, 2)
    }

    attemptCreateAccount() {
        let message = this.checkData();
        if (message) {
            Alert.alert(strings.title_error, `${strings.error_requireds_fiels} ${message}`);
        } else if (!CPF.validate(this.props.cpf)) {
            Alert.alert(strings.title_error, strings.invalid_cpf);
        } else if (this.props.pass !== this.props.confirmPass) {
            Alert.alert(strings.title_error, "Senha e confirmação não são iguais");
        } else if (Validator.isEmailValid(this.props.email)) {
            this._criarConta();
        } else {
            Alert.alert(strings.title_error, strings.invalid_email);
        }
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.bg }}>
                <ScrollView style={{ flex: 1 }}>
                    <View style={{ alignItems: 'center', flex: 1, paddingBottom: 16, paddingTop: 32 }}>

                        <Text style={{ fontSize: 20, textAlign: 'center', color: colors.primary_color }}>Preencha os campos abaixo para se cadastrar</Text>

                        <View style={{ height: 20 }} />

                        <CustomTextInput
                            placeholder={strings.placeholder_name_create}
                            width={260}
                            onChangeText={text => this.props.changeName(text)}
                            value={this.props.name}
                            maxLength={14}
                            returnKeyType={"next"}
                            onSubmitEditing={() => this.customInput.refs.inputCPF.focus()} />
                        <View style={{ height: 15 }} />

                        <CustomTextInput
                            placeholder={strings.placeholder_login_create}
                            width={260}
                            onChangeText={text => this.props.changeCPF(text)}
                            value={this.props.cpf}
                            maxLength={14}
                            blurOnSubmit={false}
                            returnKeyType={"done"}
                            refInner="inputCPF"
                            ref={ref => this.customInput = ref}
                            onSubmitEditing={() => this.customEmail.refs.inputEmail.focus()}
                            keyboardType='numeric' />
                        <View style={{ height: 15 }} />

                        <CustomTextInput
                            placeholder={strings.placeholder_email}
                            width={260}
                            onChangeText={text => this.props.changeEmail(text)}
                            value={this.props.email}
                            returnKeyType={"next"}
                            blurOnSubmit={false}
                            keyboardType='email-address'
                            onSubmitEditing={() => this.customPhone.refs.inputPhone.focus()}
                            refInner="inputEmail"
                            ref={ref => this.customEmail = ref}
                            blurOnSubmit={false}
                        />
                        <View style={{ height: 15 }} />

                        <CustomTextInput
                            placeholder={strings.placeholder_phone}
                            width={260}
                            onChangeText={text => this.props.changePhone(text)}
                            value={this.props.phone}
                            returnKeyType={"done"}
                            blurOnSubmit={false}

                            refInner="inputPhone"
                            ref={ref => this.customPhone = ref}
                            blurOnSubmit={false}
                            onSubmitEditing={() => this.customPass.refs.inputPass.focus()}
                            keyboardType='phone-pad' />
                        <View style={{ height: 15 }} />

                        <CustomTextInput
                            placeholder={strings.placeholder_pass_create}
                            width={260}
                            blurOnSubmit={false}
                            onChangeText={text => this.props.changePass(text)}
                            secureTextEntry
                            maxLength={10}
                            returnKeyType={"next"}
                            refInner="inputPass"
                            ref={ref => this.customPass = ref}
                            onSubmitEditing={() => this.customConfirmPass.refs.inputConfirmePass.focus()}
                            blurOnSubmit={false}
                            value={this.props.pass} />
                        <View style={{ height: 15 }} />

                        <CustomTextInput
                            placeholder={strings.placeholder_confirm_pass_create}
                            width={260}
                            onChangeText={text => this.props.changeConfirmPass(text)}
                            secureTextEntry
                            maxLength={10}
                            returnKeyType={"done"}
                            refInner="inputConfirmePass"
                            ref={ref => this.customConfirmPass = ref}
                            onSubmitEditing={() => this.attemptCreateAccount()}
                            value={this.props.confirmPass} />

                        <View style={{ height: 20 }} />

                        <CustomButton
                            title={strings.action_create_account_button}
                            width={260}
                            onPress={() => this.attemptCreateAccount()} />
                        <View style={{ height: 20 }} />

                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', width: 260 }}>
                            <Text style={{ textAlign: 'center', color: 'black' }}>Ao se cadastrar você concorda com nossos
                            <Text style={{ fontWeight: "bold", textAlign: 'center', color: 'black' }}> Termos e condições de uso</Text>
                            </Text>
                        </View>

                        <View style={{ height: 20 }} />


                    </View>
                </ScrollView>

                <TouchableHighlight
                    style={{ margin: 16, position: "absolute", top: 0, left: 0 }}
                    underlayColor={colors.bg_color_button_pressed}
                    activeOpacity={0.7}
                    onPress={() => Actions.pop()} >
                    <Icon name='keyboard-arrow-left' size={32} color={colors.primary_color} />
                </TouchableHighlight>
            </View>
        )
    }
}

mapStateToProps = state => (
    {
        name: state.AuthReducer.name,
        cpf: state.AuthReducer.cpf,
        email: state.AuthReducer.email,
        phone: state.AuthReducer.phone,
        pass: state.AuthReducer.pass,
        confirmPass: state.AuthReducer.confirmPass
    }
)

export default connect(mapStateToProps, { changeName, changeCPF, changeEmail, changePhone, changePass, changeConfirmPass, criarConta })(CreateAccountEmrpesaPasso1);