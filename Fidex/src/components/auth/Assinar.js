import React, { Component } from 'react';
import { View, TouchableHighlight, Text, ListView, StyleSheet, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons'

import CustomButton from '../custom/CustomButton';
import { colors } from '../../../res/colors';
import { Actions } from 'react-native-router-flux';

class Assinar extends Component {
    constructor(props) {
        super(props)
        this.state = {
            recursos: [
                { recurso: "Crie promoções exclusivas" },
                { recurso: "Conceda pontos para fidelizar seu cliente" },
                { recurso: "Conheça melhor seus clientes" },
                { recurso: "Gerencia as movimentações de pontos dos seus clientes" },
                { recurso: "Apareça para milhares de usuários" },
                { recurso: "Acompanhe o desempenho do seu negócio" },
                { recurso: "Três meses grátis para testar" }
            ]
        }

        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })

        this.fonteDeDados = ds.cloneWithRows(this.state.recursos)
    }

    renderRow(contato) {
        return (
            <View style={{ flexDirection: 'row', alignItems: 'center', flex: 1, paddingVertical: 16, paddingLeft:16, paddingRight:32 }}>
                <Icon name='done' size={20} color={colors.primary_color} />
                <Text style={{ paddingLeft:16, color: colors.primary_color, fontWeight: 'bold', flexWrap: 'wrap' }}>{contato.recurso}</Text>
            </View>
        )
    }

    render() {
        return (
            <ScrollView style={{ flex: 1, backgroundColor: colors.bg }}>
                <View style={{ alignItems: 'center', flex: 1 }}>

                    <Text style={{ color: colors.border_color, textAlign: 'center', margin: 16 }}>Assine agora e começe a desfrutar de todos os recursos e oportunidades que só o Fidex pode oferecer para seu negócio</Text>

                    <ListView
                        enableEmptySections
                        style={{paddingHorizontal:16}}
                        renderSeparator={(rowId) =>
                            <View key={rowId} style={{
                                flex: 1,
                                marginRight:16,
                                height: StyleSheet.hairlineWidth,
                                backgroundColor: colors.border_color,
                            }}
                            />}
                        dataSource={this.fonteDeDados}
                        renderRow={this.renderRow} />

                    <View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
                        <Text style={{ paddingBottom: 2, fontWeight: 'bold', fontSize: 30, color: colors.primary_color, }}>R$</Text>
                        <Text style={{ fontWeight: 'bold', fontSize: 36, color: colors.primary_color }}>24,90</Text>
                        <Text style={{ paddingBottom: 5, color: colors.border_color }}>/mês</Text>
                    </View>

                    <Text style={{ margin: 10, color: colors.border_color }}>Você será cobrado após os 3 meses</Text>

                    <CustomButton
                        title="Começar os 90 dias de teste"
                        width={260}
                        onPress={() => Actions.criarEstabelecimentoPasso1()} />

                    <TouchableHighlight onPress={() => false} underlayColor={colors.bg_color_button_pressed}
                        style={{ margin: 16 }}
                        onPress={() => Actions.pop()}
                        activeOpacity={0.7}>
                        <Text style={{ color: colors.primary_color }}>Quero desistir</Text >
                    </TouchableHighlight>

                </View>
            </ScrollView>

        )
    }
}

export default Assinar