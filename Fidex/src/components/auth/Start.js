/**
 * @author Silio Silvestre siliosffreitas@gmail.com
 */

import React from 'react';
import { View, Text, Image, ImageBackground , TouchableHighlight} from 'react-native';
import { Actions } from 'react-native-router-flux';

import CustomButton from '../custom/CustomButton';
import { colors } from '../../../res/colors';
import { strings } from '../../../res/strings';

const logo = require('../../../res/imgs/logo.png');
const start = require('../../../res/imgs/bg_start.png');

const Start = props => (
    <ImageBackground source={start} style={{ flex: 1 }} >
        <View style={{ flex: 1, paddingBottom: 16, paddingTop: 32 }}>

            <View style={{ alignItems: 'center', flex: 1, justifyContent: 'space-between' }}>

                <Image source={logo} />

                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <CustomButton
                        title={strings.i_am_an_usar}
                        width={260}
                        onPress={() => Actions.login()} />
                    <View style={{ height: 15 }} />
                    <CustomButton
                        title={strings.i_am_a_corporation}
                        width={260}
                        onPress={() => Actions.loginEmpresas()} />
                </View>
            </View>


            <View style={{ justifyContent: 'flex-end', alignItems: 'center', flex: 1 }}>
                <TouchableHighlight
                    style={{padding: 10}}
                    underlayColor={colors.bg_color_button_pressed}
                    activeOpacity={0.7}
                    onPress={() => Actions.termos()} >
                    <Text style={{ color: colors.primary_color }}>{strings.terms_and_privacity}</Text >
                </TouchableHighlight>
            </View>
        </View>

    </ImageBackground>


);

export default Start;