/**
 * @author Silio Silvestre siliosffreitas@gmail.com
 */

import React, { Component } from 'react';
import { View, Text, Image, ScrollView, Alert, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';
import CPF from 'gerador-validador-cpf';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialIcons'

import CustomButton from '../custom/CustomButton';
import CustomTextInput from '../custom/CustomTextInput';

import { colors } from '../../../res/colors';
import { strings } from '../../../res/strings';

const logo = require('../../../res/imgs/logo.png');
import { changeCPF, changePass, loginUsuario } from '../../actions/AuthActions';

class Login extends Component {

    constructor(props) {
        super(props);

        this.focusNextField = this.focusNextField.bind(this);
        this.inputs = {};
    }

    focusNextField(id) {
        this.inputs[id].focus();
    }

    checkData() {
        let message = '';
        if (!this.props.cpf) {
            message += strings.field_cpf;
        }
        if (!this.props.pass) {
            if (message) {
                message += ', ';
            }
            message += strings.field_pass;
        }
        return message;
    }

    attemptLogin() {
        let message = this.checkData();
        if (message) {
            Alert.alert(strings.title_error, `${strings.error_requireds_fiels} ${message}`);
        } else if (CPF.validate(this.props.cpf)) {
            this.props.loginUsuario(this.props.cpf, this.props.pass)
        } else {
            Alert.alert(strings.title_error, strings.invalid_cpf);
        }
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.bg }}>
                <ScrollView style={{ flex: 1 }}>
                    <View style={{ alignItems: 'center', flex: 1, paddingBottom: 16, paddingTop: 32 }}>

                        <Image source={logo} />

                        <View style={{ height: 20 }} />

                        <CustomTextInput
                            placeholder={strings.placeholder_login}
                            width={260}
                            onChangeText={text => this.props.changeCPF(text)}
                            value={this.props.cpf}
                            maxLength={14}
                            returnKeyType={"done"}
                            blurOnSubmit={false}
                            onSubmitEditing={() => this.customInput.refs.inputPass.focus()}
                            keyboardType='numeric'
                        />
                        <View style={{ height: 15 }} />

                        <CustomTextInput
                            placeholder={strings.placeholder_pass}
                            width={260}
                            returnKeyType={"done"}
                            blurOnSubmit={false}
                            onChangeText={text => this.props.changePass(text)}
                            secureTextEntry
                            maxLength={10}
                            value={this.props.pass}
                            refInner="inputPass"
                            onSubmitEditing={() => this.attemptLogin()}
                            ref={ref => this.customInput = ref}
                        />
                        <View style={{ height: 20 }} />

                        <CustomButton
                            title={strings.action_enter}
                            width={260}
                            onPress={() => this.attemptLogin()} />

                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: 260 }}>
                            <View style={{ flex: 1, height: 1, backgroundColor: colors.border_color }} />
                            <Text style={{ padding: 16, color: colors.border_color }}>OU</Text>
                            <View style={{ flex: 1, height: 1, backgroundColor: colors.border_color }} />
                        </View>

                        {/* <CustomButtonAux title="Entrar com Facebook" width={260} source={facebook}
                            onPress={() => Actions.informeCPF()} />
                        <View style={{ height: 20 }} />
                        <CustomButtonAux title="Entrar com Google" width={260} source={google}
                            onPress={() => Actions.informeCPF()} /> */}

                        <View style={{ height: 30 }} />

                        <TouchableHighlight onPress={() => Actions.createAccount()} underlayColor={colors.bg_color_button_pressed}
                            activeOpacity={0.7}>
                            <Text style={{ color: colors.primary_color }}>{strings.action_create_account}</Text >
                        </TouchableHighlight>
                    </View>

                </ScrollView>

                <TouchableHighlight
                    style={{ margin: 16, position: "absolute", top: 0, left: 0 }}
                    underlayColor={colors.bg_color_button_pressed}
                    activeOpacity={0.7}
                    onPress={() => Actions.pop()} >
                    <Icon name='keyboard-arrow-left' size={32} color={colors.primary_color} />
                </TouchableHighlight>
            </View>
        );
    }
}


mapStateToProps = state => (
    {
        cpf: state.AuthReducer.cpf,
        pass: state.AuthReducer.pass
    }
)

export default connect(mapStateToProps, { changeCPF, changePass, loginUsuario })(Login);