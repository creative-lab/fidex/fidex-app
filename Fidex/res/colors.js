/**
 * @author Silio Silvestre siliosffreitas@gmail.com
 */

export const colors = {
    primary_color: "#4909AF",
    bg: "white",
    bg_button: "#4b16ac",
    text_button : "white",
    border_color: "#d4d4d4",
    bg_color_button_pressed: "#00FF0000"
}