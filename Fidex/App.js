/**
 * @author Silio Silvestre siliosffreitas@gmail.com
 */

import React, { Component } from 'react';
import { YellowBox } from 'react-native';
import {
  Platform,
  StyleSheet,
  Text,
  View, StatusBar
} from 'react-native';

import { Provider } from 'react-redux';
import { createStore } from 'redux';

import Routes from './src/components/Routes';
import reducers from './src/reducers';
import { colors } from './res/colors';
import configureStore from './src/util/configureStore';

type Props = {};
const store = configureStore();

import Loader from './src/components/custom/Loader';
export default class App extends Component<Props> {
  render() {

    YellowBox.ignoreWarnings([
      'Warning: isMounted(...) is deprecated', 
      'Module RCTImageLoader',
      'source.uri should not be an empty string'
    ]);

    return (
      <Provider store={store}>
        <View style={{ flex: 1 }}>
          <StatusBar backgroundColor={colors.primary_color} />
          <Routes />
          <Loader />
        </View>
      </Provider >
    );
  }
}
